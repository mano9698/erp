<?php

namespace App\Imports;

use App\Models\UI\BankStatement;
use Maatwebsite\Excel\Concerns\ToModel;
// use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Session;
use Illuminate\Support\Facades\Auth;
class BankStatementImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
            return new BankStatement([
                'user_id' => $UserId,
                'value_date' => $row[0],
                'description' => $row[1],
                'amount' => $row[2],
            ]);
        }else{
            $UserId = Session::get('UserId');
            return new BankStatement([
                'user_id' => $UserId,
                'value_date' => $row[0],
                'description' => $row[1],
                'amount' => $row[2],
            ]);
        }


    }
}
