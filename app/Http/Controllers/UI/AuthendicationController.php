<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Users;
use App\Models\UI\AssignUsers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class AuthendicationController extends Controller
{
    public function login(){
        return view('UI.login');
    }

    public function register(){
        return view('UI.register');
    }

    public function admin_login(Request $request){
        $Email = $request->email;
        $Password = $request->password;

        $CheckEmail = Users::where('email', $Email)->first();

        if($CheckEmail == null){

            return redirect()->back()->with('message','Please check your credentials');

        }else{

            if($CheckEmail->status == 0){

                return redirect()->back()->with('message','Your account is not activated. Please contact your administrator...');

            }elseif($CheckEmail->user_type == 1){
                if (Auth::guard('super_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('AdminName', Auth::guard('super_admin')->user()->name);
                    $request->session()->put('AdminEmail', Auth::guard('super_admin')->user()->email);
                    $request->session()->put('AdminId', Auth::guard('super_admin')->user()->id);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');

                }
            }elseif($CheckEmail->user_type == 2){
                if (Auth::guard('company')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('Companyname', Auth::guard('company')->user()->name);
                    $request->session()->put('CompanyEmail', Auth::guard('company')->user()->email);
                    $request->session()->put('CompanyId', Auth::guard('company')->user()->id);
                    $request->session()->put('UserId', Auth::guard('company')->user()->id);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');

                }
            }elseif($CheckEmail->user_type == 3){
                return redirect()->back()->with('message','Your Role is not changed. Please contact your administrator');
            }elseif($CheckEmail->user_type == 4){
                if (Auth::guard('manager')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('Managername', Auth::guard('manager')->user()->name);
                    $request->session()->put('ManagerEmail', Auth::guard('manager')->user()->email);
                    $request->session()->put('ManagerId', Auth::guard('manager')->user()->id);
                    $request->session()->put('UserId', Auth::guard('manager')->user()->id);
                    $AssignUsers = AssignUsers::where('user_id', Auth::guard('manager')->user()->id)->first();

                    $request->session()->put('UserId', $AssignUsers->company_id);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');

                }
            }elseif($CheckEmail->user_type == 5){
                if (Auth::guard('accountant')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('Accountantname', Auth::guard('accountant')->user()->name);
                    $request->session()->put('AccountantEmail', Auth::guard('accountant')->user()->email);
                    $request->session()->put('AccountantId', Auth::guard('accountant')->user()->id);

                    $request->session()->put('UserId', Auth::guard('accountant')->user()->id);

                    $AssignUsers = AssignUsers::where('user_id', Auth::guard('accountant')->user()->id)->first();

                    $request->session()->put('UserId', $AssignUsers->company_id);

                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');

                }
            }elseif($CheckEmail->user_type == 6){
                if (Auth::guard('hr')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('Hrname', Auth::guard('hr')->user()->name);
                    $request->session()->put('HrEmail', Auth::guard('hr')->user()->email);
                    $request->session()->put('HrId', Auth::guard('hr')->user()->id);

                    $request->session()->put('UserId', Auth::guard('hr')->user()->id);

                    $AssignUsers = AssignUsers::where('user_id', Auth::guard('hr')->user()->id)->first();

                    $request->session()->put('UserId', $AssignUsers->company_id);

                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');

                }
            }
        }

    }

    public function add_companies(Request $request){
        $Users = new Users();

        $Users->name = $request->name;
        $Users->email = $request->email;
        $Users->mobile = $request->mobile;
        $Users->password = Hash::make($request->password);
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $Users->address = $request->address;
        $Users->pan = $request->pan;
        $Users->gst = $request->gst;
        $Users->status = 0;
        $Users->user_type = 2;

        $AddUsers = $Users->save();

        return redirect()->back()->with('message','Company Registered Successfully');
    }




    public function update_users(Request $request){
        $id = $request->id;
        $Users = Users::where('id', $id)->first();

        $Users->name = $request->name;
        $Users->email = $request->email;
        $Users->mobile = $request->mobile;
        // $Users->password = Hash::make($request->password);
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        // $Users->status = 1;
        // $Users->user_type = 2;

        $UpdateUsers = $Users->save();

        return redirect()->back()->with('message','User Updated Successfully');
    }

    public function add_users(Request $request){
        $Users = new Users();

        $Users->name = $request->name;
        $Users->email = $request->email;
        $Users->mobile = $request->mobile;
        $Users->password = Hash::make("Erp@123");
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;

        $Users->status = 0;
        $Users->user_type = 3;

        $AddUsers = $Users->save();

        return redirect()->back()->with('message','User Registered Successfully');
    }


    public function admin_logout()
    {
        Auth::guard('super_admin')->logout();
        return redirect('/');
    }


    public function company_logout()
    {
        Auth::guard('company')->logout();
        return redirect('/');
    }

    public function manager_logout()
    {
        Auth::guard('manager')->logout();
        return redirect('/');
    }

    public function accountant_logout()
    {
        Auth::guard('accountant')->logout();
        return redirect('/');
    }

    public function hr_logout()
    {
        Auth::guard('hr')->logout();
        return redirect('/');
    }
}
