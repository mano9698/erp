<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Users;
use App\Models\UI\Bills;

use App\Models\UI\AssignUsers;
use App\Models\UI\Documents;
use App\Models\UI\CostCenter;
use App\Models\UI\Employees;
use App\Models\UI\Ledger;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;
use Charts;
use Session;

class HomeController extends Controller
{
    public function home(){
        $title = "Dashboard";
        $data['year_list'] = $this->fetch_year();
        if(Auth::guard('super_admin')->check()){
            $Employees = Employees::count();
        }else{
            $UserId = Session::get('UserId');
            $Employees = Employees::where('user_id', $UserId)->count();
        }
        return view('UI.layouts.dashboard', compact('title', 'Employees'))->with($data);
    }

    public function bar_chats()
    {
        $title = "Dashboard";
        $users = Ledger::select(DB::raw("SUM(total_amount) as total_amount"))
                ->whereYear('created_at', date('Y'))
                ->groupBy(DB::raw("Month(created_at)"))
                ->pluck('total_amount');

        // echo json_encode($users);
        // exit;

        $months = Ledger::select(DB::raw("Month(created_at) as month"))
                ->whereYear('created_at', date('Y'))
                ->groupBy(DB::raw("Month(created_at)"))
                ->pluck('month');
        $datas = array(0,0,0,0,0,0,0,0,0,0,0,0);
        foreach($months as $index => $month){
            $datas[$month] = $users[$index];
        }
        return view('UI.bar_chart', compact('datas'));
    }

    public function chats()
    {
        $data['year_list'] = $this->fetch_year();
        return view('UI.charts')->with($data);
    }

    public function fetch_year() {
        $data = DB::table('users')->select(DB::raw('YEAR(created_at) year'))->distinct()->groupBy('created_at')->orderBy('created_at', 'DESC')->get();
        return $data;
    }

    public function fetch_data(Request $request, $id) {
        if($request->input('year'))
        {

            $chart_data = $this->fetch_chart_data($request->input('year'), $id);

            foreach($chart_data->toArray() as $row)
            {
             $output[] = array(
              'month'  => date('M', strtotime($row->year)),
              'profit' => $row->total_amount
             );
            }

            echo json_encode($output);
        }

    }

    function fetch_chart_data($year, $id)
    {
    //  $data =  DB::table('ledger')->select('ledger.created_at', DB::raw('sum(ledger.total_amount) as total_amount'))->distinct()->join('sub_group', 'sub_group.id', '=', 'ledger.group_id')->join('primary_group', 'primary_group.id', '=', 'sub_group.primary_group_id')->orderBy('ledger.created_at', 'ASC')->where('primary_group.id', $id)->whereYear('ledger.created_at', $year)->get();

    $data =  DB::table('ledger')->select(DB::raw('MONTH(ledger.created_at) as month,YEAR(ledger.created_at) as year'), DB::raw('sum(ledger.total_amount) as total_amount'))->join('sub_group', 'sub_group.id', '=', 'ledger.group_id')->join('primary_group', 'primary_group.id', '=', 'sub_group.primary_group_id')->orderBy('ledger.created_at', 'ASC')->where('primary_group.id', $id)->whereYear('ledger.created_at', $year)->groupby('month','year','total_amount')->get();
     return $data;
    }




    public function companies_list(){
        $title = "Companies List";
        $Users = Users::where('user_type', 2)->orderBy('created_at', 'DESC')->get();
        return view('UI.companies.list', compact('Users', 'title'));

    }


    public function users_list(){
        $title = "Users List";
        $Users = Users::where('user_type', 3)->Orwhere('user_type', 4)->Orwhere('user_type', 5)->Orwhere('user_type', 6)->orderBy('created_at', 'DESC')->get();
        return view('UI.users.users_list', compact('Users', 'title'));

    }

    public function change_status(Request $request)
    {
    	// \Log::info($request->all());
        $user = Users::find($request->id);
        $user->status = $request->status;
        $user->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function edit_user($id){
        $title = "Edit User";
        $Users = Users::where('id', $id)->first();
        return view('UI.users.edit_user', compact('Users', 'title'));

    }

    public function add_user(){
        $title = "New User";
        // $Users = Users::where('id', $id)->first();
        return view('UI.users.new_user', compact('title'));

    }

    public function assign_user($id){
        $title = "Assign User";
        $UsersList = Users::where('user_type', 3)->orderBy('created_at', 'DESC')->get();

        $Users = AssignUsers::where('assign_users.company_id', $id)
                ->select('users.*', 'assign_users.id as AssignId')
                ->join('users', 'assign_users.user_id', '=', 'users.id')
                ->orderBy('created_at', 'DESC')->get();
        return view('UI.users.assign_users', compact('title', 'Users', 'UsersList'));

    }

    public function add_assign_users(Request $request){
        $AssignUsers = new AssignUsers();

        $AssignUsers->user_id = $request->user_id;
        $AssignUsers->company_id = $request->company_id;

        $AddUsers = $AssignUsers->save();

        return redirect()->back()->with('message','User Added Successfully');
    }

    public function update_assign_users(Request $request){
        $id = $request->id;

        $AssignUsers = AssignUsers::where('id', $id)->first();

        $AssignUsers->user_id = $request->user_id;
        $AssignUsers->company_id = $request->company_id;

        $AddUsers = $AssignUsers->save();

        return redirect()->back()->with('message','User Updated Successfully');
    }

    public function delete_assign_users($id)
    {
    	// \Log::info($request->all());
        $AssignUsers = AssignUsers::where('id', $id)->delete();

        return redirect()->back()->with('message','User Deleted Successfully');
    }


    public function change_user_roles(){
        $title = "Change User Roles";
        $UserId = Session::get('CompanyId');
        $UsersList = Users::where('user_type', 3)->orderBy('created_at', 'DESC')->get();

        $Users = AssignUsers::select('users.*', 'assign_users.id as AssignId')
                ->join('users', 'assign_users.user_id', '=', 'users.id')
                ->where('assign_users.company_id', $UserId)
                ->orderBy('created_at', 'DESC')->get();
        return view('UI.companies.change_role_users', compact('title', 'Users', 'UsersList'));
    }

    public function change_password(){
        $title = "Change Password";

        return view('UI.users.change_password', compact('title'));
    }

    public function change_roles(Request $request)
    {
    	// \Log::info($request->all());
        $user = Users::find($request->id);
        $user->user_type = $request->user_id;
        $user->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function update_password(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');
        }elseif(Auth::guard('accountant')->check()){
            $UserId = Session::get('AccountantId');
        }elseif(Auth::guard('hr')->check()){
            $UserId = Session::get('HrId');
        }

        $Users = Users::where('id', $UserId)->first();

        $Users->password = Hash::make($request->password);

        $UpdateUsers = $Users->save();

        return redirect()->back()->with('message','Password Updated Successfully');
    }

    public function add_bills(){
        $title = "Add Bills";

        return view('UI.bills.new_bills', compact('title'));
    }

    public function view_bills(){
        $title = "View Bills";
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
            $Bills = Bills::get();
        }elseif(Auth::guard('company')->check()){
            $UserId = Session::get('CompanyId');
            $Bills = Bills::where('user_id', $UserId)->get();
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');
            $Bills = Bills::where('user_id', $UserId)->get();
        }elseif(Auth::guard('accountant')->check()){
            $UserId = Session::get('AccountantId');
            $Bills = Bills::where('user_id', $UserId)->get();
        }elseif(Auth::guard('hr')->check()){
            $UserId = Session::get('HrId');
            $Bills = Bills::where('user_id', $UserId)->get();
        }


        return view('UI.bills.view_bills', compact('title', 'Bills'));
    }

    public function store_bills(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');
        }elseif(Auth::guard('accountant')->check()){
            $UserId = Session::get('AccountantId');
        }elseif(Auth::guard('hr')->check()){
            $UserId = Session::get('HrId');
        }

        $Bills = new Bills();

        $Bills->user_id = $UserId;
        $Bills->bill_type = $request->bill_type;
        $Bills->bill_date = $request->bill_date;
        $Bills->bill_no = $request->bill_no;
        $Bills->party_name = $request->party_name;
        $Bills->amount = $request->amount;
        $Bills->short_description = $request->short_description;

        $AddBills = $Bills->save();

        return redirect()->back()->with('message','Bills Added Successfully');
    }


    public function add_upload_document(){
        $title = "Upload Document";

        return view('UI.document.new_document', compact('title'));
    }

    public function store_documents(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');
        }elseif(Auth::guard('accountant')->check()){
            $UserId = Session::get('AccountantId');
        }elseif(Auth::guard('hr')->check()){
            $UserId = Session::get('HrId');
        }

        $Documents = new Documents();

        $Documents->user_id = $UserId;
        $Documents->document_type = $request->document_type;

        if($request->hasfile('document')){
            $extension = $request->file('document')->getClientOriginalExtension();
            $dir = 'UI/documents/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('document')->move($dir, $filename);

            $Documents->document = $filename;
        }

        $Documents->upload_date = $request->upload_date;
        $Documents->document_name = $request->document_name;
        $Documents->short_description = $request->short_description;

        $AddDocuments = $Documents->save();

        return redirect()->back()->with('message','Documents Added Successfully');
    }

    public function view_documents(){
        $title = "View Documents";
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
            $Documents = Documents::get();
        }elseif(Auth::guard('company')->check()){
            $UserId = Session::get('CompanyId');
            $Documents = Documents::where('user_id', $UserId)->get();
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');
            $Documents = Documents::where('user_id', $UserId)->get();
        }elseif(Auth::guard('accountant')->check()){
            $UserId = Session::get('AccountantId');
            $Documents = Documents::where('user_id', $UserId)->get();
        }elseif(Auth::guard('hr')->check()){
            $UserId = Session::get('HrId');
            $Documents = Documents::where('user_id', $UserId)->get();
        }


        return view('UI.document.documents_list', compact('title', 'Documents'));
    }



    // Cost Center
    public function cost_center_lists(){
        $title = "Cost Center Lists";
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
            $CostCenter = CostCenter::orderBy('created_at', 'DESC')->get();
            $Documents = Documents::get();
        }elseif(Auth::guard('company')->check()){
            $UserId = Session::get('CompanyId');
            $CostCenter = CostCenter::where('user_id', $UserId)->orderBy('created_at', 'DESC')->get();
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');
            $CostCenter = CostCenter::where('user_id', $UserId)->orderBy('created_at', 'DESC')->get();
        }elseif(Auth::guard('accountant')->check()){
            $UserId = Session::get('AccountantId');
            $CostCenter = CostCenter::where('user_id', $UserId)->orderBy('created_at', 'DESC')->get();
        }elseif(Auth::guard('hr')->check()){
            $UserId = Session::get('HrId');
            $CostCenter = CostCenter::where('user_id', $UserId)->orderBy('created_at', 'DESC')->get();
        }


        return view('UI.cost_center.lists', compact('CostCenter', 'title'));

    }

    public function add_cost_center(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
            $Documents = Documents::get();
        }elseif(Auth::guard('company')->check()){
            $UserId = Session::get('CompanyId');
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');
        }elseif(Auth::guard('accountant')->check()){
            $UserId = Session::get('AccountantId');
        }elseif(Auth::guard('hr')->check()){
            $UserId = Session::get('HrId');
        }

        $CostCenter = new CostCenter();

        $CostCenter->user_id = $UserId;
        $CostCenter->name = $request->name;
        $CostCenter->status = 1;

        $AddCostCenter = $CostCenter->save();

        return redirect()->back()->with('message','CostCenter Added Successfully');
    }

    public function update_cost_center(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
            $Documents = Documents::get();
        }elseif(Auth::guard('company')->check()){
            $UserId = Session::get('CompanyId');
        }elseif(Auth::guard('manager')->check()){
            $UserId = Session::get('ManagerId');
        }elseif(Auth::guard('accountant')->check()){
            $UserId = Session::get('AccountantId');
        }elseif(Auth::guard('hr')->check()){
            $UserId = Session::get('HrId');
        }

        $id = $request->id;

        $CostCenter = CostCenter::where('id', $id)->first();
        $CostCenter->user_id = $UserId;
        $CostCenter->name = $request->name;

        $AddCostCenter = $CostCenter->save();

        return redirect()->back()->with('message','CostCenter Updated Successfully');
    }

    public function group_cost_center(Request $request)
    {
    	// \Log::info($request->all());
        $CostCenter = CostCenter::find($request->id);
        $CostCenter->status = $request->status;
        $CostCenter->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_cost_center(Request $request)
    {
    	// \Log::info($request->all());
        $CostCenter = CostCenter::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }

    public function invoice_design(){
        $title = "Dashboard";

        return view('UI.invoice.invoice_design', compact('title'));
    }
}
