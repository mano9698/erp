<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\PrimaryGroup;
use App\Models\UI\SubGroup;

use App\Models\UI\Customers;
use App\Models\UI\Ledger;
use App\Models\UI\CostCenter;
use App\Models\UI\CustomerInvoice;
use App\Models\UI\CustomerLedgerAmounts;

use Illuminate\Support\Facades\Auth;

use Session;

class CustomersController extends Controller
{
    public function customers_list(){
        $title = "Customers List";
        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::join('primary_group', 'primary_group.id', 'customers.group_type_id')->select('customers.*', 'primary_group.group_name')->get();
        }else{
            $UserId = Session::get('UserId');
            $Customers = Customers::join('primary_group', 'primary_group.id', 'customers.group_type_id')->select('customers.*', 'primary_group.group_name')->where('customers.user_id', $UserId)->get();
        }

        // $Customers = Customers::select('customers.*', 'primary_group.group_name')->join('primary_group', 'primary_group.id', '=', 'customers.group_type_id')->where('customers.user_id', $UserId)->get();


        return view("UI.customers.customers_list", compact('Customers', 'title'));
    }

    public function add_customer(){
        $title = "Add Customers";
        // $Customers = Customers::get();
        $PrimaryGroup = PrimaryGroup::get();
        if(Auth::guard('super_admin')->check()){
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

            $SubGroup = PrimaryGroup::where('type_of_group', 2)->get();
        }else{
            $UserId = Session::get('UserId');
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

        $SubGroup = PrimaryGroup::where('type_of_group', 2)->where('user_id', $UserId)->get();
            // $SubGroup = SubGroup::where('user_id', 1)->orwhere('user_id', $UserId)->get();
        }
        return view("UI.customers.new_customer", compact('title', 'PrimaryGroup', 'SubGroup'));
    }

    public function edit_customer($id){
        $title = "Edit Customers";
        $Customers = Customers::where('id', $id)->first();
        if(Auth::guard('super_admin')->check()){
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

            $SubGroup = PrimaryGroup::where('type_of_group', 2)->get();
        }else{
            $UserId = Session::get('UserId');
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

        $SubGroup = PrimaryGroup::where('type_of_group', 2)->where('user_id', $UserId)->get();
        }
        return view("UI.customers.edit_customer", compact('title', 'Customers', 'PrimaryGroup', 'SubGroup'));
    }

    public function store_customer(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Customers = new Customers();

        $Customers->user_id = $UserId;
        $Customers->name = $request->name;
        $Customers->group_type_id = $request->group_type_id;
        $Customers->customer_type = $request->customer_type;
        $Customers->pan_no = $request->pan_no;
        $Customers->gst_no = $request->gst_no;
        $Customers->bussiness_category = $request->bussiness_category;
        $Customers->country = $request->country;
        $Customers->state = $request->state;
        $Customers->city = $request->city;
        $Customers->pin_code = $request->pin_code;
        $Customers->address_1 = $request->address_1;
        $Customers->address_2 = $request->address_2;
        $Customers->contac_person_name = $request->contac_person_name;
        $Customers->contact_person_designation = $request->contact_person_designation;
        $Customers->phone = $request->phone;
        $Customers->email = $request->email;
        $Customers->status = 1;

        if($request->hasfile('pan_copy')){
            $extension = $request->file('pan_copy')->getClientOriginalExtension();
            $dir = 'UI/customers/pan/';
            $filename1 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pan_copy')->move($dir, $filename1);

            $Customers->pan_copy = $filename1;
        }

        if($request->hasfile('gst_copy')){
            $extension = $request->file('gst_copy')->getClientOriginalExtension();
            $dir = 'UI/customers/gst/';
            $filename2 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('gst_copy')->move($dir, $filename2);

            $Customers->gst_copy = $filename2;
        }

        if($request->hasfile('company_incorporation')){
            $extension = $request->file('company_incorporation')->getClientOriginalExtension();
            $dir = 'UI/customers/company_incorporation/';
            $filename3 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('company_incorporation')->move($dir, $filename3);

            $Customers->company_incorporation = $filename3;
        }

        if($request->hasfile('aggreement_contract_copy')){
            $extension = $request->file('aggreement_contract_copy')->getClientOriginalExtension();
            $dir = 'UI/customers/agreement _contract/';
            $filename4 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('aggreement_contract_copy')->move($dir, $filename4);

            $Customers->aggreement_contract_copy = $filename4;
        }

        $AddCustomers = $Customers->save();

        return redirect()->back()->with('message','Customer Added Successfully');
    }



    public function update_customer(Request $request){
        $id = $request->id;
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Customers = Customers::where('id', $id)->first();

        $Customers->user_id = $UserId;
        $Customers->name = $request->name;
        $Customers->group_type_id = $request->group_type_id;
        $Customers->customer_type = $request->customer_type;
        $Customers->pan_no = $request->pan_no;
        $Customers->gst_no = $request->gst_no;
        $Customers->bussiness_category = $request->bussiness_category;
        $Customers->country = $request->country;
        $Customers->state = $request->state;
        $Customers->city = $request->city;
        $Customers->pin_code = $request->pin_code;
        $Customers->address_1 = $request->address_1;
        $Customers->address_2 = $request->address_2;
        $Customers->contac_person_name = $request->contac_person_name;
        $Customers->contact_person_designation = $request->contact_person_designation;
        $Customers->phone = $request->phone;
        $Customers->email = $request->email;
        // $Customers->status = 1;

        if($request->hasfile('pan_copy')){
            $extension = $request->file('pan_copy')->getClientOriginalExtension();
            $dir = 'UI/customers/pan/';
            $filename1 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pan_copy')->move($dir, $filename1);

            $Customers->pan_copy = $filename1;
        }else{
            $Customers->pan_copy = $Customers->pan_copy;
        }

        if($request->hasfile('gst_copy')){
            $extension = $request->file('gst_copy')->getClientOriginalExtension();
            $dir = 'UI/customers/gst/';
            $filename2 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('gst_copy')->move($dir, $filename2);

            $Customers->gst_copy = $filename2;
        }else{
            $Customers->gst_copy = $Customers->gst_copy;
        }

        if($request->hasfile('company_incorporation')){
            $extension = $request->file('company_incorporation')->getClientOriginalExtension();
            $dir = 'UI/customers/company_incorporation/';
            $filename3 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('company_incorporation')->move($dir, $filename3);

            $Customers->company_incorporation = $filename3;
        }else{
            $Customers->company_incorporation = $Customers->company_incorporation;
        }

        if($request->hasfile('aggreement_contract_copy')){
            $extension = $request->file('aggreement_contract_copy')->getClientOriginalExtension();
            $dir = 'UI/customers/agreement _contract/';
            $filename4 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('aggreement_contract_copy')->move($dir, $filename4);

            $Customers->aggreement_contract_copy = $filename4;
        }else{
            $Customers->aggreement_contract_copy = $Customers->aggreement_contract_copy;
        }

        $AddCustomers = $Customers->save();

        return redirect()->back()->with('message','Customer Updated Successfully');
    }


    public function customer_status(Request $request)
    {
    	// \Log::info($request->all());
        $Customers = Customers::find($request->id);
        $Customers->status = $request->status;
        $Customers->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_customers(Request $request)
    {
    	// \Log::info($request->all());
        $Customers = Customers::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }






    // Customer Invoice

    public function customers_invoice_list(){
        $title = "Customers Invoice List";
        if(Auth::guard('super_admin')->check()){
            $CustomerInvoice = CustomerInvoice::select('customer_invoice.*', 'customers.id as CustomerId', 'customers.name')->join('customers', 'customers.id', 'customer_invoice.customer_id')->get();
        }else{
            $UserId = Session::get('UserId');
            $CustomerInvoice = CustomerInvoice::select('customer_invoice.*', 'customers.id as CustomerId', 'customers.name')->join('customers', 'customers.id', 'customer_invoice.customer_id')->where('customer_invoice.user_id', $UserId)->get();
        }



        return view("UI.customers.invoice_list", compact('CustomerInvoice', 'title'));
    }

    public function add_customer_invoice(){
        $title = "Add Customers Invoice";
        // // $Customers = Customers::get();
        // $PrimaryGroup = PrimaryGroup::get();
        // $SubGroup = SubGroup::get();
        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Ledger = Ledger::get();
            $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Customers = Customers::where('user_id', $UserId)->get();
            $Ledger = Ledger::where('user_id', $UserId)->get();
            $CostCenter = CostCenter::where('user_id', $UserId)->get();
        }

        return view("UI.customers.new_invoice", compact('title', 'Customers' ,'Ledger', 'CostCenter'));
    }

    public function edit_invoice($id){
        $title = "Edit Invoice";
        // $CustomerInvoice = CustomerInvoice::with('CustomerLedgerAmounts')->where('id', $id)->first();
        $CustomerInvoice = CustomerInvoice::where('id', $id)->first();

        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Ledger = Ledger::get();
            $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Customers = Customers::where('user_id', $UserId)->get();
            $Ledger = Ledger::where('user_id', $UserId)->get();
            $CostCenter = CostCenter::where('user_id', $UserId)->get();
        }

        return view("UI.customers.edit_invoice", compact('title', 'Customers', 'Ledger', 'CustomerInvoice', 'CostCenter'));
    }


    public function store_customer_invoice(Request $request){


            if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

            $Customers = new CustomerInvoice();

            $Customers->user_id = $UserId;
            $Customers->customer_id = $request->customer_id;
            $Customers->invoice_number = $request->invoice_number;
            $Customers->invoice_date = $request->invoice_date;

            $Customers->total_amount = $request->total_amount;
            $Customers->status = 3;

            $AddCustomers = $Customers->save();

            $request->session()->put('CustomerId', $Customers->id);

            $LedgerArr = $_POST['ledger'];
            $CostArr = $_POST['cost'];
            $AmountArr = $_POST['amount'];

            if(!empty($AmountArr)){

                for($i = 0; $i < count($AmountArr); $i++){
                    if(!empty($AmountArr[$i])){

                        $CustomerLedgerAmounts = new CustomerLedgerAmounts();
                        $CustomerLedgerAmounts->customer_invoice_id = $Customers->id;
                        $CustomerLedgerAmounts->ledger_id = $LedgerArr[$i];
                        $CustomerLedgerAmounts->cost_id = $CostArr[$i];
                        $CustomerLedgerAmounts->amount = $AmountArr[$i];

                        $CustomerLedgerAmounts->save();

                        $Ledger = Ledger::where('id', $LedgerArr[$i])->first();

                        $Ledger->total_amount = $AmountArr[$i];

                        $Ledger->save();

                        // echo json_encode($AmountArr[$i]);
                        // Database insert query goes here
                    }
                }
            }

            // exit;

            return redirect()->back()->with('message','Customer Invoice Updated Successfully');


    }


    public function update_customer_invoice(Request $request){

            // if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

            $id = $request->id;

            $Customers = CustomerInvoice::where('id', $id)->first();

            $Customers->customer_id = $request->customer_id;
            $Customers->invoice_number = $request->invoice_number;
            $Customers->invoice_date = $request->invoice_date;

            $Customers->total_amount = $request->total_amount;

            $AddCustomers = $Customers->save();


            $LedgerArr = $_POST['ledger'];
            $CostArr = $_POST['cost'];
            $AmountArr = $_POST['amount'];

            // CustomerLedgerAmounts::where('customer_invoice_id', $Customers->id)->delete();

            if(!empty($AmountArr)){

                for($i = 0; $i < count($AmountArr); $i++){
                    if(!empty($AmountArr[$i])){

                        $CustomerLedgerAmounts = new CustomerLedgerAmounts();
                        $CustomerLedgerAmounts->customer_invoice_id = $Customers->id;
                        $CustomerLedgerAmounts->ledger_id = $LedgerArr[$i];
                        $CustomerLedgerAmounts->cost_id = $CostArr[$i];
                        $CustomerLedgerAmounts->amount = $AmountArr[$i];

                        $CustomerLedgerAmounts->save();

                        $Ledger = Ledger::where('id', $LedgerArr[$i])->first();

                        $Ledger->total_amount = $AmountArr[$i];

                        $Ledger->save();
                        // Database insert query goes here
                    }
                }
            }

            return redirect()->back()->with('message','Customer Invoice Updated Successfully');

    }


    public function customer_invoice_status(Request $request)
    {
    	// \Log::info($request->all());
        $CustomerInvoice = CustomerInvoice::find($request->id);
        $CustomerInvoice->status = $request->status;
        $CustomerInvoice->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_customer_invoice(Request $request)
    {
    	// \Log::info($request->all());
        $CustomerInvoice = CustomerInvoice::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }

    public function delete_customers_ledger_amounts(Request $request)
    {
    	// \Log::info($request->all());
        $Customers = CustomerLedgerAmounts::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }

}
