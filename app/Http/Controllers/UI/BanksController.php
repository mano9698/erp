<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Session;
use Illuminate\Support\Facades\Auth;

use App\Models\UI\Banks;
use App\Models\UI\Customers;
use App\Models\UI\Vendors;
use App\Models\UI\Employees;
use App\Models\UI\CostCenter;
use App\Models\UI\BankStatement;
use Excel;
use App\Imports\BankStatementImport;
class BanksController extends Controller
{
    public function banks_list(){
        $title = "Banks List";
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Banks = Banks::where('user_id', $UserId)->get();

        return view("UI.banks.bank_lists", compact('Banks', 'title'));
    }

    public function add_bank(){
        $title = "Add Bank";
        // $Customers = Customers::get();
        // $PrimaryGroup = PrimaryGroup::get();
        // $SubGroup = SubGroup::get();
        return view("UI.banks.new_bank", compact('title'));
    }

    public function edit_bank($id){
        $title = "Edit Banks";
        $Banks = Banks::where('id', $id)->first();
        return view("UI.banks.edit_bank", compact('title', 'Banks'));
    }

    public function store_banks(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Banks = new Banks();

        $Banks->user_id = $UserId;
        $Banks->bank_name = $request->bank_name;
        $Banks->account_number = $request->account_number;
        $Banks->ifsc_code = $request->ifsc_code;
        $Banks->bank_branch = $request->bank_branch;
        $Banks->status = 1;


        $AddBanks = $Banks->save();

        return redirect()->back()->with('message','Bank Added Successfully');
    }



    public function update_bank(Request $request){
        $id = $request->id;
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Banks = Banks::where('id', $id)->first();

        $Banks->user_id = $UserId;
        $Banks->bank_name = $request->bank_name;
        $Banks->account_number = $request->account_number;
        $Banks->ifsc_code = $request->ifsc_code;
        $Banks->bank_branch = $request->bank_branch;


        $AddBanks = $Banks->save();

        return redirect()->back()->with('message','Bank Updated Successfully');
    }


    public function bank_status(Request $request)
    {
    	// \Log::info($request->all());
        $Banks = Banks::find($request->id);
        $Banks->status = $request->status;
        $Banks->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_bank(Request $request)
    {
    	// \Log::info($request->all());
        $Banks = Banks::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }



    // Bank Statement
    public function banks_statement_list(){
        $title = "Banks Statement List";
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('UserId');
        }


        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
            $Banks = Banks::get();
            $CostCenter = CostCenter::get();
            $BankStatement = BankStatement::where('status', 0)->get();
        }else{
            $UserId = Session::get('UserId');
            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
            $BankStatement = BankStatement::where('user_id', $UserId)->where('status', 0)->get();
        }

        return view("UI.bank_statements.bank_statement_list", compact('BankStatement', 'title', 'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function confirm_banks_statement_list(){
        $title = "Confirm Banks Statement List";
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('UserId');
        }


        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
            $Banks = Banks::get();
            $CostCenter = CostCenter::get();
            $BankStatement = BankStatement::where('status', 1)->get();
        }else{
            $UserId = Session::get('UserId');
            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
            $BankStatement = BankStatement::where('user_id', $UserId)->where('status', 1)->get();
        }

        return view("UI.bank_statements.confirm_bank_statement", compact('BankStatement', 'title', 'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function importExcel(Request $request)
    {
        Excel::import(new BankStatementImport, $request->file);

    return redirect()->back()->with('message','Imported Successfully');
    }

    public function update_bank_statement(Request $request){
        $id = $request->id;

        $BankStatement = BankStatement::where('id', $id)->first();

        $BankStatement->bank_id = $request->bank_id;
        $BankStatement->ledger_id = $request->customer_id;
        $BankStatement->payment_mode = $request->payment_mode_id;
        $BankStatement->payment_reference_number = $request->payment_reference_number;
        $BankStatement->status = 1;
        $BankStatement->save();

        return redirect()->back()->with('message','Bank Statement Update Successfully');

    }


    public function update_confirm_bank_statement(Request $request){
        $id = $request->id;

        $BankStatement = BankStatement::where('id', $id)->first();

        $BankStatement->bank_id = $request->bank_id;
        $BankStatement->ledger_id = $request->customer_id;
        $BankStatement->payment_mode = $request->payment_mode_id;
        $BankStatement->payment_reference_number = $request->payment_reference_number;
        $BankStatement->save();

        return redirect()->back()->with('message','Bank Statement Update Successfully');

    }

    public function delete_bank_statement($id)
    {
    	// \Log::info($request->all());
        $BankStatement = BankStatement::where('id', $id)->delete();

        return redirect()->back()->with('message','Bank Statement Deleted Successfully');
    }

    public function delete_all_bank_statement(Request $request)
    {
        $ids = $request->ids;
        BankStatement::whereIn('id',explode(",",$ids))->delete();
        return response()->json(['success'=>"Bank Statement Deleted successfully."]);
    }
}
