<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Ledger;
use App\Models\UI\Banks;
use App\Models\UI\CashPaymentEntry;
use App\Models\UI\CashReceivedEntry;
use App\Models\UI\CashDepositedBank;
use App\Models\UI\CostCenter;
use App\Models\UI\CostPaymentLedgerAmounts;
use App\Models\UI\Customers;
use App\Models\UI\Vendors;
use App\Models\UI\Employees;

use Illuminate\Support\Facades\Auth;

use Session;


class CashController extends Controller
{
    public function cash_payment_entry_list(){
        $title = "Cash Payment Entry List";
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $CashPaymentEntry = CashPaymentEntry::select('cash_payment_entry.*', 'ledger.ledger_name')->join('ledger', 'ledger.id', '=', 'cash_payment_entry.cash_in_hand_group')->where('cash_payment_entry.user_id', $UserId)->get();

        // $OnlinePaymentEntry = OnlinePaymentEntry::get();

        $CostCenter = CostCenter::get();
        return view("UI.cash_payment.cash_payment_entry_list", compact('CashPaymentEntry', 'title'));
    }

    public function add_cash_entry(){
        $title = "New Cash Payment Entry";
        // $Customers = Customers::get();
        // $Banks = Banks::get();
        if(Auth::guard('super_admin')->check()){
            $Ledger = Ledger::get();
        $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Ledger = Ledger::where('user_id', $UserId)->get();
            $CostCenter = CostCenter::where('user_id', $UserId)->get();
        }

        return view("UI.cash_payment.new_cash_payment_entry", compact('title', 'Ledger', 'CostCenter'));
    }

    public function edit_cash_payment_entry($id){
        $title = "Edit Cash Payment Entry";
        $CashPaymentEntry = CashPaymentEntry::where('id', $id)->first();
        // $Banks = Banks::get();
        if(Auth::guard('super_admin')->check()){
            $Ledger = Ledger::get();
        $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Ledger = Ledger::where('user_id', $UserId)->get();
            $CostCenter = CostCenter::where('user_id', $UserId)->get();
        }
        return view("UI.cash_payment.edit_cash_payment_entry", compact('title', 'CashPaymentEntry', 'Ledger', 'CostCenter'));
    }

    public function store_cash_payment_entry(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $CashPaymentEntry = new CashPaymentEntry();

        $CashPaymentEntry->user_id = $UserId;
        $CashPaymentEntry->cash_in_hand_group = $request->cash_in_hand_group;
        $CashPaymentEntry->payment_date = $request->payment_date;
        $CashPaymentEntry->remarks = $request->remarks;
        $CashPaymentEntry->status = 1;

        $AddCashPaymentEntry = $CashPaymentEntry->save();

        $request->session()->put('CustomerId', $CashPaymentEntry->id);

        $LedgerArr = $_POST['ledger'];
        $CostArr = $_POST['cost'];
        $AmountArr = $_POST['amount'];

        if(!empty($AmountArr)){

            for($i = 0; $i < count($AmountArr); $i++){
                if(!empty($AmountArr[$i])){

                    $CostPaymentLedgerAmounts = new CostPaymentLedgerAmounts();
                    $CostPaymentLedgerAmounts->cost_payment_id = $CashPaymentEntry->id;
                    $CostPaymentLedgerAmounts->ledger_id = $LedgerArr[$i];
                    $CostPaymentLedgerAmounts->cost_id = $CostArr[$i];
                    $CostPaymentLedgerAmounts->amount = $AmountArr[$i];

                    $CostPaymentLedgerAmounts->save();

                    $Ledger = Ledger::where('id', $LedgerArr[$i])->first();

                    $Ledger->total_amount = $AmountArr[$i];

                    $Ledger->save();

                    // echo json_encode($AmountArr[$i]);
                    // Database insert query goes here
                }
            }
        }

        return redirect()->back()->with('message','Cash Payment Entry Added Successfully');
    }

    public function update_cash_payment_entry(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $CashPaymentEntry = CashPaymentEntry::where('id', $id)->first();;

        $CashPaymentEntry->user_id = $UserId;
        $CashPaymentEntry->cash_in_hand_group = $request->cash_in_hand_group;
        $CashPaymentEntry->payment_date = $request->payment_date;
        $CashPaymentEntry->remarks = $request->remarks;

        $AddCashPaymentEntry = $CashPaymentEntry->save();

        $request->session()->put('CustomerId', $CashPaymentEntry->id);

        $LedgerArr = $_POST['ledger'];
        $CostArr = $_POST['cost'];
        $AmountArr = $_POST['amount'];

        if(!empty($AmountArr)){

            for($i = 0; $i < count($AmountArr); $i++){
                if(!empty($AmountArr[$i])){

                    $CostPaymentLedgerAmounts = new CostPaymentLedgerAmounts();
                    $CostPaymentLedgerAmounts->cost_payment_id = $CashPaymentEntry->id;
                    $CostPaymentLedgerAmounts->ledger_id = $LedgerArr[$i];
                    $CostPaymentLedgerAmounts->cost_id = $CostArr[$i];
                    $CostPaymentLedgerAmounts->amount = $AmountArr[$i];

                    $CostPaymentLedgerAmounts->save();

                    $Ledger = Ledger::where('id', $LedgerArr[$i])->first();

                    $Ledger->total_amount = $AmountArr[$i];

                    $Ledger->save();

                    // echo json_encode($AmountArr[$i]);
                    // Database insert query goes here
                }
            }
        }

        return redirect()->back()->with('message','Cash Payment Entry Updated Successfully');
    }


    // public function cheque_entry_status(Request $request)
    // {
    // 	// \Log::info($request->all());
    //     $ChequePaymentEntry = ChequePaymentEntry::find($request->id);
    //     $ChequePaymentEntry->status = $request->status;
    //     $ChequePaymentEntry->save();

    //     return response()->json(['success'=>'Status changed successfully.']);
    // }

    public function delete_cash_ledger_amount(Request $request)
    {
    	// \Log::info($request->all());
        $CostPaymentLedgerAmounts = CostPaymentLedgerAmounts::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }

    public function delete_cash_payment_entry(Request $request)
    {
    	// \Log::info($request->all());
        $CashPaymentEntry = CashPaymentEntry::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }



    // Cash Payment Received
    public function cash_received_entry_list(){
        $title = "Cash Received Entry List";
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $CashReceivedEntry = CashReceivedEntry::select('cash_received_entry.*', 'ledger.ledger_name')->join('ledger', 'ledger.id', '=', 'cash_received_entry.ledger_id')->where('cash_received_entry.user_id', $UserId)->get();

        // $OnlinePaymentEntry = OnlinePaymentEntry::get();

        return view("UI.cash_received.cash_received_entry_list", compact('CashReceivedEntry', 'title'));
    }

    public function add_cash_received_entry(){
        $title = "New Cash Received Entry";
        // $Customers = Customers::get();
        // $Banks = Banks::get();
        if(Auth::guard('super_admin')->check()){
            $Ledger = Ledger::get();
        $CostCenter = CostCenter::get();
        $Customers = Customers::get();
        $Vendors = Vendors::get();
        $Employees = Employees::get();
        $Banks = Banks::get();
        }else{
            $UserId = Session::get('UserId');
            $Ledger = Ledger::where('user_id', $UserId)->get();
            $CostCenter = CostCenter::where('user_id', $UserId)->get();
            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }
        return view("UI.cash_received.new_cash_received", compact('title', 'Ledger', 'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function edit_cash_received_entry($id){
        $title = "Edit Cash Payment Entry";
        $CashReceivedEntry = CashReceivedEntry::where('id', $id)->first();
        // $Banks = Banks::get();
        if(Auth::guard('super_admin')->check()){
            $Ledger = Ledger::get();
        $CostCenter = CostCenter::get();
        $Customers = Customers::get();
        $Vendors = Vendors::get();
        $Employees = Employees::get();
        $Banks = Banks::get();
        }else{
            $UserId = Session::get('UserId');
            $Ledger = Ledger::where('user_id', $UserId)->get();
            $CostCenter = CostCenter::where('user_id', $UserId)->get();
            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }
        return view("UI.cash_received.edit_cash_received", compact('title', 'CashReceivedEntry', 'Ledger', 'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function store_cash_received_entry(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $CashReceivedEntry = new CashReceivedEntry();

        $CashReceivedEntry->user_id = $UserId;
        $CashReceivedEntry->cash_in_hand_group = $request->cash_in_hand_group;
        $CashReceivedEntry->ledger_id = $request->ledger_id;
        $CashReceivedEntry->cash_received_date = $request->cash_received_date;
        $CashReceivedEntry->cash_amount_received = $request->cash_amount_received;
        $CashReceivedEntry->received_for = $request->received_for;
        $CashReceivedEntry->status = 1;

        $AddCashReceivedEntry = $CashReceivedEntry->save();

        return redirect()->back()->with('message','Cash Received Entry Added Successfully');
    }

    public function update_cash_received_entry(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $CashReceivedEntry = CashReceivedEntry::where('id', $id)->first();;

        $CashReceivedEntry->user_id = $UserId;
        $CashReceivedEntry->cash_in_hand_group = $request->cash_in_hand_group;
        $CashReceivedEntry->ledger_id = $request->ledger_id;
        $CashReceivedEntry->cash_received_date = $request->cash_received_date;
        $CashReceivedEntry->cash_amount_received = $request->cash_amount_received;
        $CashReceivedEntry->received_for = $request->received_for;

        $AddCashReceivedEntry = $CashReceivedEntry->save();

        return redirect()->back()->with('message','Cash Received Entry Updated Successfully');
    }


    // public function cheque_entry_status(Request $request)
    // {
    // 	// \Log::info($request->all());
    //     $ChequePaymentEntry = ChequePaymentEntry::find($request->id);
    //     $ChequePaymentEntry->status = $request->status;
    //     $ChequePaymentEntry->save();

    //     return response()->json(['success'=>'Status changed successfully.']);
    // }

    public function delete_cash_received_entry(Request $request)
    {
    	// \Log::info($request->all());
        $CashReceivedEntry = CashReceivedEntry::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }
    // End





    // Cash Deposited Bank
    public function cash_deposited_entry_list(){
        $title = "Cash Deposited Entry List";
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $CashDepositedBank = CashDepositedBank::select('cash_deposited_bank.*', 'banks.bank_name')->join('banks', 'banks.id', '=', 'cash_deposited_bank.bank_id')->where('cash_deposited_bank.user_id', $UserId)->get();

        // $OnlinePaymentEntry = OnlinePaymentEntry::get();

        return view("UI.cash_deposited.deposited_list", compact('CashDepositedBank', 'title'));
    }

    public function add_cash_deposited_entry(){
        $title = "New Cash Deposited Entry";
        // $Customers = Customers::get();
        $Banks = Banks::get();
        $Ledger = Ledger::get();
        return view("UI.cash_deposited.new_cash_deposited", compact('title', 'Ledger', 'Banks'));
    }

    public function edit_cash_deposited_entry($id){
        $title = "Edit Cash Deposited Entry";
        $CashDepositedBank = CashDepositedBank::where('id', $id)->first();
        $Banks = Banks::get();
        $Ledger = Ledger::get();
        return view("UI.cash_deposited.edit_deposited", compact('title', 'CashDepositedBank', 'Ledger', 'Banks'));
    }

    public function store_cash_deposited_entry(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $CashDepositedBank = new CashDepositedBank();

        $CashDepositedBank->user_id = $UserId;
        $CashDepositedBank->cash_in_hand_group = $request->cash_in_hand_group;
        $CashDepositedBank->bank_id = $request->bank_id;
        $CashDepositedBank->cash_deposited_date = $request->cash_deposited_date;
        $CashDepositedBank->cash_deposited_amount = $request->cash_deposited_amount;
        $CashDepositedBank->cash_deposited_by = $request->cash_deposited_by;
        $CashDepositedBank->status = 1;

        $AddCashDepositedBank = $CashDepositedBank->save();

        return redirect()->back()->with('message','Cash Deposited Entry Added Successfully');
    }

    public function update_cash_deposited_entry(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $CashDepositedBank = CashDepositedBank::where('id', $id)->first();;

        $CashDepositedBank->user_id = $UserId;
        $CashDepositedBank->cash_in_hand_group = $request->cash_in_hand_group;
        $CashDepositedBank->bank_id = $request->bank_id;
        $CashDepositedBank->cash_deposited_date = $request->cash_deposited_date;
        $CashDepositedBank->cash_deposited_amount = $request->cash_deposited_amount;
        $CashDepositedBank->cash_deposited_by = $request->cash_deposited_by;

        $AddCashDepositedBank = $CashDepositedBank->save();

        return redirect()->back()->with('message','Cash Deposited Entry Updated Successfully');
    }


    // public function cheque_entry_status(Request $request)
    // {
    // 	// \Log::info($request->all());
    //     $ChequePaymentEntry = ChequePaymentEntry::find($request->id);
    //     $ChequePaymentEntry->status = $request->status;
    //     $ChequePaymentEntry->save();

    //     return response()->json(['success'=>'Status changed successfully.']);
    // }

    public function delete_cash_deposited_entry(Request $request)
    {
    	// \Log::info($request->all());
        $CashDepositedBank = CashDepositedBank::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }
    // End
}
