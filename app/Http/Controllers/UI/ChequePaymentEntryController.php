<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\UI\Ledger;
use App\Models\UI\Banks;
use App\Models\UI\ChequePaymentEntry;
use App\Models\UI\ChequePaymentReceived;
use App\Models\UI\Customers;
use App\Models\UI\Vendors;
use App\Models\UI\Employees;
use App\Models\UI\ChequeDepositeReceived;

use Session;

class ChequePaymentEntryController extends Controller
{
    public function cheque_entry_list(){
        $title = "Cheque Payment Entry List";
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $ChequePaymentEntry = ChequePaymentEntry::select('cheque_payment_entry.*', 'banks.bank_name', 'ledger.ledger_name')->join('banks', 'banks.id', '=', 'cheque_payment_entry.bank_id')->join('ledger', 'ledger.id', '=', 'cheque_payment_entry.ledger_id')->where('cheque_payment_entry.user_id', $UserId)->get();

        // $OnlinePaymentEntry = OnlinePaymentEntry::get();

        return view("UI.cheque_payment.cheque_list", compact('ChequePaymentEntry', 'title'));
    }

    public function add_cheque_entry(){
        $title = "New Cheque Payment Entry";
        // $Customers = Customers::get();
        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
            $Banks = Banks::get();
        }else{
            $UserId = Session::get('UserId');

            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }
        return view("UI.cheque_payment.new_cheque_payment", compact('title', 'Banks', 'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function edit_cheque_entry($id){
        $title = "Edit Cheque Payment Entry";
        $ChequePaymentEntry = ChequePaymentEntry::where('id', $id)->first();
        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
            $Banks = Banks::get();
        }else{
            $UserId = Session::get('UserId');

            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }
        return view("UI.cheque_payment.edit_cheque_payment", compact('title', 'ChequePaymentEntry', 'Banks', 'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function store_cheque_entry(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $ChequePaymentEntry = new ChequePaymentEntry();

        $ChequePaymentEntry->user_id = $UserId;
        $ChequePaymentEntry->bank_id = $request->bank_id;
        $ChequePaymentEntry->ledger_id = $request->ledger_id;
        $ChequePaymentEntry->cheque_number = $request->cheque_number;
        $ChequePaymentEntry->cheque_date = $request->cheque_date;
        $ChequePaymentEntry->cheque_amount = $request->cheque_amount;
        $ChequePaymentEntry->remarks = $request->remarks;
        $ChequePaymentEntry->status = 1;

        $AddChequePaymentEntry = $ChequePaymentEntry->save();

        return redirect()->back()->with('message','Cheque Payment Entry Added Successfully');
    }

    public function update_cheque_entry(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $ChequePaymentEntry = ChequePaymentEntry::where('id', $id)->first();;

        $ChequePaymentEntry->user_id = $UserId;
        $ChequePaymentEntry->bank_id = $request->bank_id;
        $ChequePaymentEntry->ledger_id = $request->ledger_id;
        $ChequePaymentEntry->cheque_number = $request->cheque_number;
        $ChequePaymentEntry->cheque_date = $request->cheque_date;
        $ChequePaymentEntry->cheque_amount = $request->cheque_amount;
        $ChequePaymentEntry->remarks = $request->remarks;
        $ChequePaymentEntry->status = 1;

        $AddChequePaymentEntry = $ChequePaymentEntry->save();

        return redirect()->back()->with('message','Cheque Payment Entry Updated Successfully');
    }


    public function cheque_entry_status(Request $request)
    {
    	// \Log::info($request->all());
        $ChequePaymentEntry = ChequePaymentEntry::find($request->id);
        $ChequePaymentEntry->status = $request->status;
        $ChequePaymentEntry->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_cheque_entry(Request $request)
    {
    	// \Log::info($request->all());
        $ChequePaymentEntry = ChequePaymentEntry::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }



    // Cheque Payment Received
    public function cheque_received_list(){
        $title = "Cheque Payment Received List";
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $ChequePaymentReceived = ChequePaymentReceived::select('cheque_payment_received.*', 'ledger.ledger_name')->join('ledger', 'ledger.id', '=', 'cheque_payment_received.ledger_id')->where('cheque_payment_received.user_id', $UserId)->get();

        // $OnlinePaymentEntry = OnlinePaymentEntry::get();

        return view("UI.cheque_received.cheque_received_list", compact('ChequePaymentReceived', 'title'));
    }

    public function add_cheque_received(){
        $title = "New Cheque Payment Received";
        // $Customers = Customers::get();
        // $Banks = Banks::get();
        if(Auth::guard('super_admin')->check()){
            $Banks = Banks::get();
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
        }else{
            $UserId = Session::get('UserId');

            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }
        return view("UI.cheque_received.new_cheque_received", compact('title',  'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function edit_cheque_received($id){
        $title = "Edit Cheque Payment Received";
        $ChequePaymentReceived = ChequePaymentReceived::where('id', $id)->first();
        // $Banks = Banks::get();
        if(Auth::guard('super_admin')->check()){
            $Banks = Banks::get();
        $Ledger = Ledger::get();
        }else{
            $UserId = Session::get('UserId');

            $Banks = Banks::where('user_id', $UserId)->get();
            $Ledger = Ledger::where('user_id', $UserId)->get();
        }
        return view("UI.cheque_received.edit_cheque_received", compact('title', 'ChequePaymentReceived',  'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function store_cheque_received(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $ChequePaymentReceived = new ChequePaymentReceived();

        $ChequePaymentReceived->user_id = $UserId;
        $ChequePaymentReceived->ledger_id = $request->ledger_id;
        $ChequePaymentReceived->cheque_number = $request->cheque_number;
        $ChequePaymentReceived->cheque_date = $request->cheque_date;
        $ChequePaymentReceived->cheque_amount = $request->cheque_amount;
        $ChequePaymentReceived->received_for = $request->received_for;
        $ChequePaymentReceived->status = 1;

        $AddChequePaymentReceived = $ChequePaymentReceived->save();

        return redirect()->back()->with('message','Cheque Payment Received Added Successfully');
    }

    public function update_cheque_received(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $ChequePaymentReceived = ChequePaymentReceived::where('id', $id)->first();;

        $ChequePaymentReceived->user_id = $UserId;
        $ChequePaymentReceived->ledger_id = $request->ledger_id;
        $ChequePaymentReceived->cheque_number = $request->cheque_number;
        $ChequePaymentReceived->cheque_date = $request->cheque_date;
        $ChequePaymentReceived->cheque_amount = $request->cheque_amount;
        $ChequePaymentReceived->received_for = $request->received_for;
        $ChequePaymentReceived->status = 1;

        $AddChequePaymentReceived = $ChequePaymentReceived->save();

        return redirect()->back()->with('message','Cheque Payment Received Updated Successfully');
    }


    public function cheque_received_status(Request $request)
    {
    	// \Log::info($request->all());
        $ChequePaymentReceived = ChequePaymentReceived::find($request->id);
        $ChequePaymentReceived->status = $request->status;
        $ChequePaymentReceived->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_cheque_received(Request $request)
    {
    	// \Log::info($request->all());
        $ChequePaymentReceived = ChequePaymentReceived::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }
    // End





    // Cheque Desposite Received
    public function cheque_deposite_list(){
        $title = "Cheque Payment Deposited Received List";
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $ChequePaymentReceived = ChequePaymentReceived::get();

        $ChequeDepositeReceived = ChequeDepositeReceived::select('cheque_deposite_received.*', 'banks.bank_name')->join('banks', 'banks.id', '=', 'cheque_deposite_received.bank_id')->where('cheque_deposite_received.user_id', $UserId)->get();


        // $OnlinePaymentEntry = OnlinePaymentEntry::get();

        return view("UI.cheque_deposite.desposite_list", compact('ChequeDepositeReceived', 'title', 'ChequePaymentReceived'));
    }

    public function add_cheque_deposite(){
        $title = "New Cheque Payment Deposited Received";
        // $Customers = Customers::get();
        $Banks = Banks::get();
        $ChequePaymentReceived = ChequePaymentReceived::get();
        return view("UI.cheque_deposite.new_deposite_received", compact('title', 'Banks', 'ChequePaymentReceived'));
    }

    public function edit_cheque_deposite($id){
        $title = "Edit Cheque Payment Deposited Received";
        $ChequeDepositeReceived = ChequeDepositeReceived::where('id', $id)->first();
        $Banks = Banks::get();
        // $Ledger = Ledger::get();
        $ChequePaymentReceived = ChequePaymentReceived::get();

        return view("UI.cheque_deposite.edit_deposite_received", compact('title', 'ChequeDepositeReceived', 'Banks', 'ChequePaymentReceived'));
    }

    public function store_cheque_deposite(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $ChequeDepositeReceived = new ChequeDepositeReceived();

        $ChequeDepositeReceived->user_id = $UserId;
        $ChequeDepositeReceived->cheque_id = $request->cheque_id;
        $ChequeDepositeReceived->bank_id = $request->bank_id;
        $ChequeDepositeReceived->cheque_date = $request->cheque_date;
        $ChequeDepositeReceived->status = 1;

        $AddChequeDepositeReceived = $ChequeDepositeReceived->save();

        return redirect()->back()->with('message','Cheque Payment Deposite Received Added Successfully');
    }

    public function update_cheque_deposite(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $ChequeDepositeReceived = ChequeDepositeReceived::where('id', $id)->first();;

        $ChequeDepositeReceived->user_id = $UserId;
        $ChequeDepositeReceived->cheque_id = $request->cheque_id;
        $ChequeDepositeReceived->bank_id = $request->bank_id;
        $ChequeDepositeReceived->cheque_date = $request->cheque_date;
        $ChequeDepositeReceived->status = 1;

        $AddChequeDepositeReceived = $ChequeDepositeReceived->save();

        return redirect()->back()->with('message','Cheque Payment Deposite Received Updated Successfully');
    }


    // public function cheque_received_status(Request $request)
    // {
    // 	// \Log::info($request->all());
    //     $ChequePaymentReceived = ChequePaymentReceived::find($request->id);
    //     $ChequePaymentReceived->status = $request->status;
    //     $ChequePaymentReceived->save();

    //     return response()->json(['success'=>'Status changed successfully.']);
    // }

    public function delete_cheque_deposite(Request $request)
    {
    	// \Log::info($request->all());
        $ChequeDepositeReceived = ChequeDepositeReceived::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }
    // End
}
