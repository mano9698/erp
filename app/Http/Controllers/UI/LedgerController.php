<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Ledger;
use App\Models\UI\SubGroup;
use App\Models\UI\LedgerAdjustmentEntry;
use App\Models\UI\OpeningBalance;
use App\Models\UI\CustomerInvoice;
use App\Models\UI\CostCenter;
use App\Models\UI\CustomerLedgerAmounts;

use App\Models\UI\VendorInvoice;
use App\Models\UI\VendorLedgerAmounts;
use App\Models\UI\PrimaryGroup;
use App\Models\UI\Customers;
use App\Models\UI\Vendors;
use App\Models\UI\Employees;
use App\Models\UI\Banks;
use Illuminate\Support\Facades\Auth;

use Session;

class LedgerController extends Controller
{
    // Ledger Crud

    public function ledger_lists(){
        $title = "Ledger Lists";
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        if(Auth::guard('super_admin')->check()){
            $Ledger = Ledger::join('primary_group', 'primary_group.id', '=', 'ledger.group_id')->select('primary_group.group_name', 'ledger.ledger_name', 'ledger.status', 'ledger.id', 'ledger.created_at')->orderBy('created_at', 'DESC')->get();

            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

            $SubGroup = PrimaryGroup::where('type_of_group', 2)->get();
        }else{
            $UserId = Session::get('UserId');
            $Ledger = Ledger::join('primary_group', 'primary_group.id', '=', 'ledger.group_id')->select('primary_group.group_name', 'ledger.ledger_name', 'ledger.status', 'ledger.id', 'ledger.created_at')->where('ledger.user_id', $UserId)->orderBy('created_at', 'DESC')->get();

        $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

        $SubGroup = PrimaryGroup::where('type_of_group', 2)->where('user_id', $UserId)->get();
            // $SubGroup = SubGroup::where('user_id', $UserId)->get();
        }

        return view('UI.group_type.ledgers_list', compact( 'Ledger', 'title', 'PrimaryGroup', 'SubGroup'));

    }

    public function add_ledger(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Ledger = new Ledger();

        $Ledger->user_id = $UserId;
        $Ledger->group_id = $request->sub_group;
        $Ledger->ledger_name = $request->ledger;
        $Ledger->status = 0;

        $AddLedger = $Ledger->save();

        return redirect()->back()->with('message','Ledger Added Successfully');
    }

    public function update_ledger(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $Ledger = Ledger::where('id', $id)->first();

        $Ledger->user_id = $UserId;
        $Ledger->group_id = $request->sub_group;
        $Ledger->ledger_name = $request->ledger;

        $AddLedger = $Ledger->save();

        return redirect()->back()->with('message','Ledger Updated Successfully');
    }

    public function ledger_status(Request $request)
    {
    	// \Log::info($request->all());
        $Ledger = Ledger::find($request->id);
        $Ledger->status = $request->status;
        $Ledger->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_ledger(Request $request)
    {
    	// \Log::info($request->all());
        $Ledger = Ledger::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }

    // End


    // Ledger Adjustment Entry
    public function ledger_adjustment_entry_list(){
        $title = "Ledger Adjustment Entry List";
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('UserId');
        }

        $LedgerAdjustmentEntry = LedgerAdjustmentEntry::select('ledger_adjustment_entry.*', 'ledger.ledger_name')->join('ledger', 'ledger.id', '=', 'ledger_adjustment_entry.from_ledger_id')->where('ledger_adjustment_entry.user_id', $UserId)->get();

        // $OnlinePaymentEntry = OnlinePaymentEntry::get();

        return view("UI.ledger_adjustment.entry_list", compact('LedgerAdjustmentEntry', 'title'));
    }

    public function add_adjustment_entry(){
        $title = "New Ledger Adjustment Entry";
        // $Customers = Customers::get();
        // $Banks = Banks::get();
        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
            $Banks = Banks::get();
            $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }
        return view("UI.ledger_adjustment.new_ledger_adjustment", compact('title', 'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function edit_adjustment_entry($id){
        $title = "Edit Ledger Adjustment Entry";
        $LedgerAdjustmentEntry = LedgerAdjustmentEntry::where('id', $id)->first();
        // $Banks = Banks::get();
        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
            $Banks = Banks::get();
            $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }
        return view("UI.ledger_adjustment.edit_ledger_adjustment", compact('title', 'Customers', 'Vendors', 'Employees', 'Banks', 'LedgerAdjustmentEntry'));
    }

    public function store_adjustment_entry(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $FromLedger = $request->from_ledger_id;
        $ToLedger = $request->to_ledger_id;
        $Amount = $request->amount_to_transfer;

        $LedgerAdjustmentEntry = new LedgerAdjustmentEntry();

        $LedgerAdjustmentEntry->user_id = $UserId;
        $LedgerAdjustmentEntry->date_of_adjustment = $request->date_of_adjustment;
        $LedgerAdjustmentEntry->from_ledger_id = $FromLedger;
        $LedgerAdjustmentEntry->to_ledger_id = $ToLedger;
        $LedgerAdjustmentEntry->amount_to_transfer = $Amount;
        $LedgerAdjustmentEntry->remarks = $request->remarks;
        $LedgerAdjustmentEntry->status = 1;

        $AddLedgerAdjustmentEntry = $LedgerAdjustmentEntry->save();

        // Reduce Amount

        $Ledger = Ledger::where('id', $FromLedger)->first();

        $ReduceAmount = $Ledger->total_amount - $Amount;

        $Ledger->total_amount = $ReduceAmount;

        $Ledger->save();

        // End


        // Add Amount
        $AddLedger = Ledger::where('id', $ToLedger)->first();

        $AddAmount = $AddLedger->total_amount + $Amount;

        $AddLedger->total_amount = $AddAmount;

        $AddLedger->save();

        // End

        return redirect()->back()->with('message','Ledger Adjustment Entry Added Successfully');
    }

    public function update_adjustment_entry(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $FromLedger = $request->from_ledger_id;
        $ToLedger = $request->to_ledger_id;
        $Amount = $request->amount_to_transfer;

        $LedgerAdjustmentEntry = LedgerAdjustmentEntry::where('id', $id)->first();;

        $LedgerAdjustmentEntry->user_id = $UserId;
        $LedgerAdjustmentEntry->date_of_adjustment = $request->date_of_adjustment;
        $LedgerAdjustmentEntry->from_ledger_id = $FromLedger;
        $LedgerAdjustmentEntry->to_ledger_id = $ToLedger;
        $LedgerAdjustmentEntry->amount_to_transfer = $Amount;
        $LedgerAdjustmentEntry->remarks = $request->remarks;

        $AddLedgerAdjustmentEntry = $LedgerAdjustmentEntry->save();

        // Reduce Amount

        $Ledger = Ledger::where('id', $FromLedger)->first();

        $ReduceAmount = $Ledger->total_amount - $Amount;

        $Ledger->total_amount = $ReduceAmount;

        $Ledger->save();

        // End


        // Add Amount
        $AddLedger = Ledger::where('id', $ToLedger)->first();

        $AddAmount = $AddLedger->total_amount + $Amount;

        $AddLedger->total_amount = $AddAmount;

        $AddLedger->save();

        // End

        return redirect()->back()->with('message','Ledger Adjustment Entry Updated Successfully');
    }


    // public function cheque_entry_status(Request $request)
    // {
    // 	// \Log::info($request->all());
    //     $ChequePaymentEntry = ChequePaymentEntry::find($request->id);
    //     $ChequePaymentEntry->status = $request->status;
    //     $ChequePaymentEntry->save();

    //     return response()->json(['success'=>'Status changed successfully.']);
    // }

    public function delete_adjustment_entry(Request $request)
    {
    	// \Log::info($request->all());
        $LedgerAdjustmentEntry = LedgerAdjustmentEntry::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }
    // End




    // Opening Balance
    public function opening_balance_list(){
        $title = "Opening balance List";
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $OpeningBalance = OpeningBalance::select('opening_balance.*', 'ledger.ledger_name')->join('ledger', 'ledger.id', '=', 'opening_balance.ledger_id')->where('opening_balance.user_id', $UserId)->get();

        // $OnlinePaymentEntry = OnlinePaymentEntry::get();

        return view("UI.opening_balance.balance_list", compact('OpeningBalance', 'title'));
    }

    public function add_opening_balance(){
        $title = "New Opening balance";
        // $Customers = Customers::get();
        // $Banks = Banks::get();
        if(Auth::guard('super_admin')->check()){
            $Ledger = Ledger::get();
        $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Ledger = Ledger::where('user_id', $UserId)->get();
        }
        return view("UI.opening_balance.new_balance", compact('title', 'Ledger'));
    }

    public function edit_opening_balance($id){
        $title = "Edit Opening balance";
        $OpeningBalance = OpeningBalance::where('id', $id)->first();
        // $Banks = Banks::get();
        if(Auth::guard('super_admin')->check()){
            $Ledger = Ledger::get();
        $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Ledger = Ledger::where('user_id', $UserId)->get();
        }
        return view("UI.opening_balance.edit_balance", compact('title', 'OpeningBalance', 'Ledger'));
    }

    public function store_opening_balance(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $OpeningBalance = new OpeningBalance();

        $OpeningBalance->user_id = $UserId;
        $OpeningBalance->ledger_id = $request->ledger_id;
        $OpeningBalance->balance_date = $request->balance_date;
        $OpeningBalance->balance_amount = $request->balance_amount;
        $OpeningBalance->status = 1;

        $AddOpeningBalance = $OpeningBalance->save();

        // if($request->type == 1){
        //     // Add Amount
        //     $AddLedger = Ledger::where('id', $request->ledger_id)->first();

        //     $AddAmount = $AddLedger->total_amount + $request->balance_amount;

        //     $AddLedger->total_amount = $AddAmount;

        //     $AddLedger->save();
        // }elseif($request->type == 2){
        //     $Ledger = Ledger::where('id', $request->ledger_id)->first();

        //     $ReduceAmount = $Ledger->total_amount - $request->balance_amount;

        //     $Ledger->total_amount = $ReduceAmount;

        //     $Ledger->save();
        // }

        return redirect()->back()->with('message','Opening Balance Added Successfully');
    }

    public function update_opening_balance(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $OpeningBalance = OpeningBalance::where('id', $id)->first();;

        $OpeningBalance->user_id = $UserId;
        $OpeningBalance->ledger_id = $request->ledger_id;
        $OpeningBalance->balance_date = $request->balance_date;
        $OpeningBalance->balance_amount = $request->balance_amount;

        $AddOpeningBalance = $OpeningBalance->save();

        return redirect()->back()->with('message','Opening Balance Updated Successfully');
    }


    // public function cheque_entry_status(Request $request)
    // {
    // 	// \Log::info($request->all());
    //     $ChequePaymentEntry = ChequePaymentEntry::find($request->id);
    //     $ChequePaymentEntry->status = $request->status;
    //     $ChequePaymentEntry->save();

    //     return response()->json(['success'=>'Status changed successfully.']);
    // }

    public function delete_opening_balance(Request $request)
    {
    	// \Log::info($request->all());
        $OpeningBalance = OpeningBalance::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }
    // End
}
