<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\PrimaryGroup;
use App\Models\UI\SubGroup;
use App\Models\UI\Ledger;

use App\Models\UI\Vendors;

use App\Models\UI\VendorInvoice;
use App\Models\UI\CostCenter;
use App\Models\UI\VendorLedgerAmounts;

use Illuminate\Support\Facades\Auth;

use Session;

class VendorsController extends Controller
{
    public function vendors_list(){
        $title = "Vendors List";
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
            $Vendors = Vendors::join('primary_group', 'primary_group.id', 'vendors.group_type_id')->select('vendors.*', 'primary_group.group_name')->get();
         }else{
             $UserId = Session::get('UserId');
             $Vendors = Vendors::join('primary_group', 'primary_group.id', 'vendors.group_type_id')->select('vendors.*', 'primary_group.group_name')->where('vendors.user_id', $UserId)->get();
        }

        // $Vendors = Vendors::select('vendors.*', 'primary_group.group_name')->join('primary_group', 'primary_group.id', '=', 'vendors.group_type_id')->where('vendors.user_id', $UserId)->get();



        return view("UI.vendors.vendors_list", compact('Vendors', 'title'));
    }

    public function add_vendor(){
        $title = "Add Vendors";
        // $Customers = Customers::get();
        $PrimaryGroup = PrimaryGroup::get();
        if(Auth::guard('super_admin')->check()){
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

            $SubGroup = PrimaryGroup::where('type_of_group', 2)->get();
        }else{
            $UserId = Session::get('UserId');
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

        $SubGroup = PrimaryGroup::where('type_of_group', 2)->where('user_id', $UserId)->get();
        }
        return view("UI.vendors.new_vendor", compact('title', 'PrimaryGroup', 'SubGroup'));
    }

    public function edit_vendor($id){
        $title = "Edit Vendors";
        $Vendors = Vendors::where('id', $id)->first();
        if(Auth::guard('super_admin')->check()){
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

            $SubGroup = PrimaryGroup::where('type_of_group', 2)->get();
        }else{
            $UserId = Session::get('UserId');
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

        $SubGroup = PrimaryGroup::where('type_of_group', 2)->where('user_id', $UserId)->get();
        }
        return view("UI.vendors.edit_vendor", compact('title', 'Vendors', 'PrimaryGroup', 'SubGroup'));
    }

    public function store_vendors(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Vendors = new Vendors();

        $Vendors->user_id = $UserId;
        $Vendors->name = $request->name;
        $Vendors->group_type_id = $request->group_type_id;
        $Vendors->vendor_type = $request->vendor_type;
        $Vendors->pan_no = $request->pan_no;
        $Vendors->gst_no = $request->gst_no;
        $Vendors->bussiness_category = $request->bussiness_category;
        $Vendors->country = $request->country;
        $Vendors->state = $request->state;
        $Vendors->city = $request->city;
        $Vendors->pin_code = $request->pin_code;
        $Vendors->address_1 = $request->address_1;
        $Vendors->address_2 = $request->address_2;
        $Vendors->contac_person_name = $request->contact_person_name;
        $Vendors->contact_person_designation = $request->contact_person_designation;
        $Vendors->phone = $request->phone;
        $Vendors->email = $request->email;
        $Vendors->status = 1;

        if($request->hasfile('pan_copy')){
            $extension = $request->file('pan_copy')->getClientOriginalExtension();
            $dir = 'UI/vendors/pan/';
            $filename1 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pan_copy')->move($dir, $filename1);

            $Vendors->pan_copy = $filename1;
        }

        if($request->hasfile('gst_copy')){
            $extension = $request->file('gst_copy')->getClientOriginalExtension();
            $dir = 'UI/vendors/gst/';
            $filename2 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('gst_copy')->move($dir, $filename2);

            $Vendors->gst_copy = $filename2;
        }

        if($request->hasfile('company_incorporation')){
            $extension = $request->file('company_incorporation')->getClientOriginalExtension();
            $dir = 'UI/vendors/company_incorporation/';
            $filename3 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('company_incorporation')->move($dir, $filename3);

            $Vendors->company_incorporation = $filename3;
        }

        if($request->hasfile('aggreement_contract_copy')){
            $extension = $request->file('aggreement_contract_copy')->getClientOriginalExtension();
            $dir = 'UI/vendors/agreement _contract/';
            $filename4 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('aggreement_contract_copy')->move($dir, $filename4);

            $Vendors->aggreement_contract_copy = $filename4;
        }

        $AddVendors = $Vendors->save();

        return redirect()->back()->with('message','Vendor Added Successfully');
    }



    public function update_vendor(Request $request){
        $id = $request->id;
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Vendors = Vendors::where('id', $id)->first();

        $Vendors->user_id = $UserId;
        $Vendors->name = $request->name;
        $Vendors->group_type_id = $request->group_type_id;
        $Vendors->vendor_type = $request->vendor_type;
        $Vendors->pan_no = $request->pan_no;
        $Vendors->gst_no = $request->gst_no;
        $Vendors->bussiness_category = $request->bussiness_category;
        $Vendors->country = $request->country;
        $Vendors->state = $request->state;
        $Vendors->city = $request->city;
        $Vendors->pin_code = $request->pin_code;
        $Vendors->address_1 = $request->address_1;
        $Vendors->address_2 = $request->address_2;
        $Vendors->contac_person_name = $request->contac_person_name;
        $Vendors->contact_person_designation = $request->contact_person_designation;
        $Vendors->phone = $request->phone;
        $Vendors->email = $request->email;
        // $Vendors->status = 1;

        if($request->hasfile('pan_copy')){
            $extension = $request->file('pan_copy')->getClientOriginalExtension();
            $dir = 'UI/vendors/pan/';
            $filename1 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pan_copy')->move($dir, $filename1);

            $Vendors->pan_copy = $filename1;
        }else{
            $Vendors->pan_copy = $Vendors->pan_copy;
        }

        if($request->hasfile('gst_copy')){
            $extension = $request->file('gst_copy')->getClientOriginalExtension();
            $dir = 'UI/vendors/gst/';
            $filename2 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('gst_copy')->move($dir, $filename2);

            $Vendors->gst_copy = $filename2;
        }else{
            $Vendors->gst_copy = $Vendors->gst_copy;
        }

        if($request->hasfile('company_incorporation')){
            $extension = $request->file('company_incorporation')->getClientOriginalExtension();
            $dir = 'UI/vendors/company_incorporation/';
            $filename3 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('company_incorporation')->move($dir, $filename3);

            $Vendors->company_incorporation = $filename3;
        }else{
            $Vendors->company_incorporation = $Vendors->company_incorporation;
        }

        if($request->hasfile('aggreement_contract_copy')){
            $extension = $request->file('aggreement_contract_copy')->getClientOriginalExtension();
            $dir = 'UI/vendors/agreement _contract/';
            $filename4 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('aggreement_contract_copy')->move($dir, $filename4);

            $Vendors->aggreement_contract_copy = $filename4;
        }else{
            $Vendors->aggreement_contract_copy = $Vendors->aggreement_contract_copy;
        }

        $AddVendors = $Vendors->save();

        return redirect()->back()->with('message','Vendor Updated Successfully');
    }


    public function vendor_status(Request $request)
    {
    	// \Log::info($request->all());
        $Vendors = Vendors::find($request->id);
        $Vendors->status = $request->status;
        $Vendors->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_vendors(Request $request)
    {
    	// \Log::info($request->all());
        $Vendors = Vendors::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }




    // Vendor Invoice

    public function vendors_invoice_list(){
        $title = "Vendor Invoice List";
        if(Auth::guard('super_admin')->check()){
            $VendorInvoice = VendorInvoice::select('vendor_invoice.*', 'vendors.id as VendorId', 'vendors.name')->join('vendors', 'vendors.id', 'vendor_invoice.vendor_id')->get();
        }else{
            $UserId = Session::get('UserId');
            $VendorInvoice = VendorInvoice::select('vendor_invoice.*', 'vendors.id as VendorId', 'vendors.name')->join('vendors', 'vendors.id', 'vendor_invoice.vendor_id')->where('vendor_invoice.user_id', $UserId)->get();
        }

        return view("UI.vendors.invoice_list", compact('VendorInvoice', 'title'));
    }

    public function add_vendor_invoice(){
        $title = "Add Vendor Invoice";
        // // $Customers = Customers::get();
        // $PrimaryGroup = PrimaryGroup::get();
        // $SubGroup = SubGroup::get();
        if(Auth::guard('super_admin')->check()){
            $Vendors = Vendors::get();
            $Ledger = Ledger::get();
            $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Ledger = Ledger::where('user_id', $UserId)->get();
            $CostCenter = CostCenter::where('user_id', $UserId)->get();
        }


        return view("UI.vendors.new_vendor_invoice", compact('title', 'Vendors' ,'Ledger', 'CostCenter'));
    }


    public function edit_invoice($id){
        $title = "Edit Invoice";
        // $CustomerInvoice = CustomerInvoice::with('CustomerLedgerAmounts')->where('id', $id)->first();
        $VendorInvoice = VendorInvoice::where('id', $id)->first();

        if(Auth::guard('super_admin')->check()){
            $Vendors = Vendors::get();
            $Ledger = Ledger::get();
            $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Ledger = Ledger::where('user_id', $UserId)->get();
            $CostCenter = CostCenter::where('user_id', $UserId)->get();
        }
        return view("UI.vendors.edit_vendor_invoice", compact('title', 'Vendors', 'Ledger', 'VendorInvoice', 'CostCenter'));
    }


    public function store_vendor_invoice(Request $request){

            if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

            $VendorInvoice = new VendorInvoice();

            $VendorInvoice->user_id = $UserId;
            $VendorInvoice->vendor_id = $request->vendor_id;
            $VendorInvoice->invoice_number = $request->invoice_number;
            $VendorInvoice->invoice_date = $request->invoice_date;

            $VendorInvoice->total_amount = $request->total_amount;
            // $VendorInvoice->status = 3;

            $AddVendorInvoice = $VendorInvoice->save();

            $request->session()->put('CustomerId', $VendorInvoice->id);

            $LedgerArr = $_POST['ledger'];
            $CostArr = $_POST['cost'];
            $AmountArr = $_POST['amount'];

            if(!empty($AmountArr)){

                for($i = 0; $i < count($AmountArr); $i++){
                    if(!empty($AmountArr[$i])){

                        $CustomerLedgerAmounts = new VendorLedgerAmounts();
                        $CustomerLedgerAmounts->vendor_invoice_id = $VendorInvoice->id;
                        $CustomerLedgerAmounts->ledger_id = $LedgerArr[$i];
                        $CustomerLedgerAmounts->cost_id = $CostArr[$i];
                        $CustomerLedgerAmounts->amount = $AmountArr[$i];

                        $CustomerLedgerAmounts->save();
                        // Database insert query goes here
                    }
                }
            }

            return redirect()->back()->with('message','Vendor Invoice Updated Successfully');

    }



    public function update_vendor_invoice(Request $request){

            $id = $request->id;

            if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

            $VendorInvoice = VendorInvoice::where('id', $id)->first();

            $VendorInvoice->user_id = $UserId;
            $VendorInvoice->vendor_id = $request->vendor_id;
            $VendorInvoice->invoice_number = $request->invoice_number;
            $VendorInvoice->invoice_date = $request->invoice_date;

            $VendorInvoice->total_amount = $request->total_amount;

            $AddVendorInvoice = $VendorInvoice->save();


            $LedgerArr = $_POST['ledger'];
            $CostArr = $_POST['cost'];
            $AmountArr = $_POST['amount'];

            if(!empty($AmountArr)){

                for($i = 0; $i < count($AmountArr); $i++){
                    if(!empty($AmountArr[$i])){

                        $CustomerLedgerAmounts = new VendorLedgerAmounts();
                        $CustomerLedgerAmounts->vendor_invoice_id = $VendorInvoice->id;
                        $CustomerLedgerAmounts->ledger_id = $LedgerArr[$i];
                        $CustomerLedgerAmounts->cost_id = $CostArr[$i];
                        $CustomerLedgerAmounts->amount = $AmountArr[$i];

                        $CustomerLedgerAmounts->save();
                        // Database insert query goes here
                    }
                }
            }

            return redirect()->back()->with('message','Vendor Invoice Updated Successfully');

    }


    public function vendor_invoice_status(Request $request)
    {
    	// \Log::info($request->all());
        $VendorInvoice = VendorInvoice::find($request->id);
        $VendorInvoice->status = $request->status;
        $VendorInvoice->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_vendor_invoice(Request $request)
    {
    	// \Log::info($request->all());
        $VendorInvoice = VendorInvoice::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }


    public function delete_vendors_ledger_amounts(Request $request)
    {
    	// \Log::info($request->all());
        $VendorLedgerAmounts = VendorLedgerAmounts::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }
}
