<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Vendors;
use App\Models\UI\Ledger;

use App\Models\UI\CustomerInvoice;
use App\Models\UI\CustomerLedgerAmounts;

use App\Models\UI\Users;
use App\Models\UI\MISReport;

use DB;
use Session;
class ReportsController extends Controller
{
    public function ledger_reports_list(){
        $title = "Reports List";
        $UserId = Session::get('UserId');

        // $Vendors = Vendors::select('vendors.*', 'primary_group.group_name')->join('primary_group', 'primary_group.id', '=', 'vendors.group_type_id')->where('vendors.user_id', $UserId)->get();

        $Vendors = Vendors::where('vendors.user_id', $UserId)->get();
        $Ledger = Ledger::get();
        return view("UI.reports.ledger_reports", compact('Vendors', 'title', 'Ledger'));
    }

    public function get_reports_by_ledger(Request $request){
        $LedgerId = $request->ledger_name;

        // echo $LedgerName;

        $GetReports = DB::table('customers')
                    ->select('customers.name', 'customer_invoice.invoice_date', 'customer_ledger_amounts.amount')
                    ->join('customer_invoice', 'customer_invoice.customer_id', '=', 'customers.id')
                    ->join('customer_ledger_amounts', 'customer_ledger_amounts.customer_invoice_id', '=', 'customer_invoice.id')
                    ->where('customer_ledger_amounts.ledger_id', $LedgerId)
                    ->get();

                    echo json_encode($GetReports);
                    exit;
    }

    public function reports_list(){
        $title = "Reports List";

        return view('UI.reports.reports', compact('title'));
    }

    public function mis_reports_list(){
        $title = "MIS Reports List";
        $MISReport = MISReport::select('users.name', 'mis_report.*')->join('users', 'users.id', '=', 'mis_report.company_id')->get();
        return view('UI.mis_report.list', compact('title', 'MISReport'));
    }

    public function add_mis_reports(){
        $title = "Add MIS Reports";
        $Users = Users::where('user_type', 2)->get();
        return view('UI.mis_report.new_mis_report', compact('title', 'Users'));
    }

    public function edit_mis_reports($id){
        $title = "Edit MIS Reports";
        $MISReport = MISReport::where('id', $id)->first();
        $Users = Users::where('user_type', 2)->get();
        return view('UI.mis_report.edit_mis_report', compact('title', 'Users', 'MISReport'));
    }

    public function view_mis_reports($id){
        $title = "Edit MIS Reports";
        $MISReport = MISReport::select('users.name', 'mis_report.*')->join('users', 'users.id', '=', 'mis_report.company_id')->where('mis_report.id', $id)->first();
        $Users = Users::where('user_type', 2)->get();
        return view('UI.mis_report.view_mis_report', compact('title', 'Users', 'MISReport'));
    }

    public function store_msi_report(Request $request){
        $MISReport = new MISReport();

        $MISReport->company_id = $request->company_id;
        $MISReport->title = $request->title;
        // $MISReport->month = $request->month;
        // $MISReport->year = $request->year;
        $MISReport->description = $request->description;

        $AddUsers = $MISReport->save();

        return redirect()->back()->with('message','MIS Report Updated Successfully');
    }


    public function update_msi_report(Request $request){
        $id = $request->id;
        $MISReport =  MISReport::where('id', $id)->first();

        $MISReport->company_id = $request->company_id;
        $MISReport->title = $request->title;
        // $MISReport->month = $request->month;
        // $MISReport->year = $request->year;
        $MISReport->description = $request->description;

        $AddUsers = $MISReport->save();

        return redirect()->back()->with('message','MIS Report Updated Successfully');
    }

    public function delete_mis_report($id)
    {
    	// \Log::info($request->all());
        $MISReport = MISReport::where('id', $id)->delete();

        return redirect()->back()->with('message','MIS Report Deleted Successfully');
    }
}
