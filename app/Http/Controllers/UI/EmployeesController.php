<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Employees;
use App\Models\UI\PrimaryGroup;
use App\Models\UI\SubGroup;
use App\Models\UI\EmployeeSalary;
use App\Models\UI\EmployeeSalaryAmounts;
use App\Models\UI\Ledger;
use App\Models\UI\CostCenter;

use Illuminate\Support\Facades\Auth;

use Session;
class EmployeesController extends Controller
{
    public function employees_list(){
        $title = "Employees List";
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Employees = Employees::where('user_id', $UserId)->get();

        return view("UI.employees.employee_list", compact('Employees', 'title'));
    }

    public function add_employee(){
        $title = "Add Employee";
        $PrimaryGroup = PrimaryGroup::get();
        if(Auth::guard('super_admin')->check()){
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

            $SubGroup = PrimaryGroup::where('type_of_group', 2)->get();
        }else{
            $UserId = Session::get('UserId');
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

            $SubGroup = PrimaryGroup::where('type_of_group', 2)->where('user_id', $UserId)->get();
        }
        // $Customers = Customers::get();
        // $PrimaryGroup = PrimaryGroup::get();
        // $SubGroup = SubGroup::get();
        return view("UI.employees.new_employee", compact('title', 'PrimaryGroup', 'SubGroup'));
    }

    public function edit_employee($id){
        $title = "Edit Employee";

        $Employees = Employees::where('id', $id)->first();
        if(Auth::guard('super_admin')->check()){
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

            $SubGroup = PrimaryGroup::where('type_of_group', 2)->get();
        }else{
            $UserId = Session::get('UserId');
            $PrimaryGroup = PrimaryGroup::where('type_of_group', 1)->get();

        $SubGroup = PrimaryGroup::where('type_of_group', 2)->where('user_id', $UserId)->get();
        }
        return view("UI.employees.edit_employee", compact('title', 'Employees', 'PrimaryGroup', 'SubGroup'));
    }

    public function store_employee(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Employees = new Employees();

        $Employees->user_id = $UserId;
        $Employees->group_type_id = $request->group_type_id;
        $Employees->first_name = $request->first_name;
        $Employees->middle_name = $request->middle_name;
        $Employees->last_name = $request->last_name;
        $Employees->dob = $request->dob;
        $Employees->pan_no = $request->pan_no;
        $Employees->aadhar = $request->aadhar;
        $Employees->joining_date = $request->joining_date;
        $Employees->designation = $request->designation;
        $Employees->department = $request->department;
        $Employees->reporting_manager = $request->reporting_manager;
        $Employees->gross_salary = $request->gross_salary;
        $Employees->annual_ctc = $request->annual_ctc;
        $Employees->country = $request->country;
        $Employees->state = $request->state;
        $Employees->city = $request->city;
        $Employees->pin_code = $request->pin_code;
        $Employees->address_1 = $request->address_1;
        $Employees->address_2 = $request->address_2;
        $Employees->blood_group = $request->blood_group;
        $Employees->phone = $request->phone;
        $Employees->email = $request->email;
        $Employees->address_2 = $request->address_2;
        $Employees->address_2 = $request->address_2;
        $Employees->status = 1;

        if($request->hasfile('pan_copy')){
            $extension = $request->file('pan_copy')->getClientOriginalExtension();
            $dir = 'UI/employees/pan_copy/';
            $filename1 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pan_copy')->move($dir, $filename1);

            $Employees->pan_copy = $filename1;
        }

        if($request->hasfile('aadhar_copy')){
            $extension = $request->file('aadhar_copy')->getClientOriginalExtension();
            $dir = 'UI/employees/aadhar_copy/';
            $filename2 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('aadhar_copy')->move($dir, $filename2);

            $Employees->aadhar_copy = $filename2;
        }

        if($request->hasfile('passport_photo')){
            $extension = $request->file('passport_photo')->getClientOriginalExtension();
            $dir = 'UI/employees/passport_photo/';
            $filename3 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('passport_photo')->move($dir, $filename3);

            $Employees->passport_photo = $filename3;
        }

        if($request->hasfile('offer_letter')){
            $extension = $request->file('offer_letter')->getClientOriginalExtension();
            $dir = 'UI/employees/offer_letter/';
            $filename4 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('offer_letter')->move($dir, $filename4);

            $Employees->offer_letter = $filename4;
        }

        $AddEmployees = $Employees->save();

        return redirect()->back()->with('message','Employee Added Successfully');
    }



    public function update_employee(Request $request){
        $id = $request->id;
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $Employees = Employees::where('id', $id)->first();

        $Employees->user_id = $UserId;
        $Employees->group_type_id = $request->group_type_id;
        $Employees->first_name = $request->first_name;
        $Employees->middle_name = $request->middle_name;
        $Employees->last_name = $request->last_name;
        $Employees->dob = $request->dob;
        $Employees->pan_no = $request->pan_no;
        $Employees->aadhar = $request->aadhar;
        $Employees->joining_date = $request->joining_date;
        $Employees->designation = $request->designation;
        $Employees->department = $request->department;
        $Employees->reporting_manager = $request->reporting_manager;
        $Employees->gross_salary = $request->gross_salary;
        $Employees->annual_ctc = $request->annual_ctc;
        $Employees->country = $request->country;
        $Employees->state = $request->state;
        $Employees->city = $request->city;
        $Employees->pin_code = $request->pin_code;
        $Employees->address_1 = $request->address_1;
        $Employees->address_2 = $request->address_2;
        $Employees->blood_group = $request->blood_group;
        $Employees->phone = $request->phone;
        $Employees->email = $request->email;
        $Employees->address_2 = $request->address_2;
        $Employees->address_2 = $request->address_2;
        $Employees->status = 1;

        if($request->hasfile('pan_copy')){
            $extension = $request->file('pan_copy')->getClientOriginalExtension();
            $dir = 'UI/employees/pan_copy/';
            $filename1 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('pan_copy')->move($dir, $filename1);

            $Employees->pan_copy = $filename1;
        }else{
            $Employees->pan_copy = $Employees->pan_copy;
        }

        if($request->hasfile('aadhar_copy')){
            $extension = $request->file('aadhar_copy')->getClientOriginalExtension();
            $dir = 'UI/employees/aadhar_copy/';
            $filename2 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('aadhar_copy')->move($dir, $filename2);

            $Employees->aadhar_copy = $filename2;
        }else{
            $Employees->aadhar_copy = $Employees->aadhar_copy;
        }

        if($request->hasfile('passport_photo')){
            $extension = $request->file('passport_photo')->getClientOriginalExtension();
            $dir = 'UI/employees/passport_photo/';
            $filename3 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('passport_photo')->move($dir, $filename3);

            $Employees->passport_photo = $filename3;
        }else{
            $Employees->passport_photo = $Employees->passport_photo;
        }

        if($request->hasfile('offer_letter')){
            $extension = $request->file('offer_letter')->getClientOriginalExtension();
            $dir = 'UI/employees/offer_letter/';
            $filename4 = uniqid() . '_' . time() . '.' . $extension;
            $request->file('offer_letter')->move($dir, $filename4);

            $Employees->offer_letter = $filename4;
        }else{
            $Employees->offer_letter = $Employees->offer_letter;
        }

        $AddEmployees = $Employees->save();

        return redirect()->back()->with('message','Employee Updated Successfully');
    }


    public function employee_status(Request $request)
    {
    	// \Log::info($request->all());
        $Employees = Employees::find($request->id);
        $Employees->status = $request->status;
        $Employees->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_employee(Request $request)
    {
    	// \Log::info($request->all());
        $Employees = Employees::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }


    // Employee Salary

    public function emp_salary_list(){
        $title = "Employee Salary List";
        if(Auth::guard('super_admin')->check()){
            $EmployeeSalary = EmployeeSalary::select('employee_salary.*', 'employees.id as CustomerId', 'employees.first_name')->join('employees', 'employees.id', 'employee_salary.emp_id')->get();
        }else{
            $UserId = Session::get('UserId');
            $EmployeeSalary = EmployeeSalary::select('employee_salary.*', 'employees.id as CustomerId', 'employees.first_name')->join('employees', 'employees.id', 'employee_salary.emp_id')->where('employee_salary.user_id', $UserId)->get();
        }



        return view("UI.employees.salary_list", compact('EmployeeSalary', 'title'));
    }

    public function add_employee_salary(){
        $title = "Add Employee Salary";
        // // $Customers = Customers::get();
        // $PrimaryGroup = PrimaryGroup::get();
        // $SubGroup = SubGroup::get();
        if(Auth::guard('super_admin')->check()){
            $Employees = Employees::get();
            $Ledger = Ledger::get();
            $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Employees = Employees::where('user_id', $UserId)->get();
            $Ledger = Ledger::where('user_id', $UserId)->get();
            $CostCenter = CostCenter::where('user_id', $UserId)->get();
        }

        return view("UI.employees.new_salary", compact('title', 'Employees' ,'Ledger', 'CostCenter'));
    }

    public function edit_employee_salary($id){
        $title = "Edit Employee Salary";
        // $CustomerInvoice = CustomerInvoice::with('CustomerLedgerAmounts')->where('id', $id)->first();
        $EmployeeSalary = EmployeeSalary::where('id', $id)->first();

        if(Auth::guard('super_admin')->check()){
            $Employees = Employees::get();
            $Ledger = Ledger::get();
            $CostCenter = CostCenter::get();
        }else{
            $UserId = Session::get('UserId');
            $Employees = Employees::where('user_id', $UserId)->get();
            $Ledger = Ledger::where('user_id', $UserId)->get();
            $CostCenter = CostCenter::where('user_id', $UserId)->get();
        }
        return view("UI.employees.edit_salary", compact('title', 'Employees', 'Ledger', 'EmployeeSalary', 'CostCenter'));
    }


    public function store_emp_salary(Request $request){


            if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

            $Employees = new EmployeeSalary();

            $Employees->user_id = $UserId;
            $Employees->emp_id = $request->emp_id;
            $Employees->salary_month = $request->salary_month;
            $Employees->due_date_date = $request->due_date_date;

            $Employees->total_amount = $request->total_amount;
            $Employees->status = 3;

            $AddCustomers = $Employees->save();

            $request->session()->put('EmployeeId', $Employees->id);

            $LedgerArr = $_POST['ledger'];
            $CostArr = $_POST['cost'];
            $AmountArr = $_POST['amount'];

            if(!empty($AmountArr)){

                for($i = 0; $i < count($AmountArr); $i++){
                    if(!empty($AmountArr[$i])){

                        $EmployeeSalaryAmounts = new EmployeeSalaryAmounts();
                        $EmployeeSalaryAmounts->emp_salary_id = $Employees->id;
                        $EmployeeSalaryAmounts->ledger_id = $LedgerArr[$i];
                        $EmployeeSalaryAmounts->cost_id = $CostArr[$i];
                        $EmployeeSalaryAmounts->amount = $AmountArr[$i];

                        $EmployeeSalaryAmounts->save();

                        $Ledger = Ledger::where('id', $LedgerArr[$i])->first();

                        $Ledger->total_amount = $AmountArr[$i];

                        $Ledger->save();

                        // echo json_encode($AmountArr[$i]);
                        // Database insert query goes here
                    }
                }
            }

            // exit;

            return redirect()->back()->with('message','Employee Salary Updated Successfully');


    }


    public function update_emp_salary(Request $request){

            // if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

            $id = $request->id;

            $Employees = EmployeeSalary::where('id', $id)->first();

            $Employees->emp_id = $request->emp_id;
            $Employees->salary_month = $request->salary_month;
            $Employees->due_date_date = $request->due_date_date;

            $Employees->total_amount = $request->total_amount;

            $AddCustomers = $Employees->save();


            $LedgerArr = $_POST['ledger'];
            $CostArr = $_POST['cost'];
            $AmountArr = $_POST['amount'];

            // EmployeeSalaryAmounts::where('emp_salary_id', $Employees->id)->delete();

            if(!empty($AmountArr)){

                for($i = 0; $i < count($AmountArr); $i++){
                    if(!empty($AmountArr[$i])){

                        $EmployeeSalaryAmounts = new EmployeeSalaryAmounts();
                        $EmployeeSalaryAmounts->emp_salary_id = $Employees->id;
                        $EmployeeSalaryAmounts->ledger_id = $LedgerArr[$i];
                        $EmployeeSalaryAmounts->cost_id = $CostArr[$i];
                        $EmployeeSalaryAmounts->amount = $AmountArr[$i];

                        $EmployeeSalaryAmounts->save();

                        $Ledger = Ledger::where('id', $LedgerArr[$i])->first();

                        $Ledger->total_amount = $AmountArr[$i];

                        $Ledger->save();
                        // Database insert query goes here
                    }
                }
            }

            return redirect()->back()->with('message','Employee Salary Data Updated Successfully');

    }


    // public function customer_invoice_status(Request $request)
    // {
    // 	// \Log::info($request->all());
    //     $CustomerInvoice = CustomerInvoice::find($request->id);
    //     $CustomerInvoice->status = $request->status;
    //     $CustomerInvoice->save();

    //     return response()->json(['success'=>'Status changed successfully.']);
    // }

    public function delete_employee_salary(Request $request)
    {
    	// \Log::info($request->all());
        $EmployeeSalary = EmployeeSalary::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }

    public function delete_employee_salary_amount(Request $request)
    {
    	// \Log::info($request->all());
        $Customers = EmployeeSalaryAmounts::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }
}
