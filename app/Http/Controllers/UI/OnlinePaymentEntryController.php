<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Models\UI\Ledger;
use App\Models\UI\Banks;
use App\Models\UI\OnlinePaymentEntry;
use App\Models\UI\OnlinePaymentReceived;
use App\Models\UI\Customers;
use App\Models\UI\Vendors;
use App\Models\UI\Employees;
use Session;

use Illuminate\Support\Facades\Auth;

class OnlinePaymentEntryController extends Controller
{
    public function payment_entry_list(){
        $title = "Online Payment Entry List";
       if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $OnlinePaymentEntry = OnlinePaymentEntry::select('online_payment_entry.*', 'banks.bank_name', 'ledger.ledger_name')->join('banks', 'banks.id', '=', 'online_payment_entry.bank_id')->join('ledger', 'ledger.id', '=', 'online_payment_entry.ledger_id')->where('online_payment_entry.user_id', $UserId)->get();

        // $OnlinePaymentEntry = OnlinePaymentEntry::get();

        return view("UI.online_payment_entry.entry_list", compact('OnlinePaymentEntry', 'title'));
    }

    public function add_payment_entry(){
        $title = "New Online Payment Entry";
        // $Customers = Customers::get();
        if(Auth::guard('super_admin')->check()){
            // $Ledger = Ledger::get();
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
            $Banks = Banks::get();
        }else{
            $UserId = Session::get('UserId');

            $Banks = Banks::where('user_id', $UserId)->get();
            // $Ledger = Ledger::where('user_id', $UserId)->get();
            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }

        return view("UI.online_payment_entry.new_online_payment", compact('title', 'Banks', 'Customers', 'Vendors', 'Employees'));
    }

    public function edit_payment_entry($id){
        $title = "Edit Online Payment Entry";
        $OnlinePaymentEntry = OnlinePaymentEntry::where('id', $id)->first();

        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
            $Banks = Banks::get();
        }else{
            $UserId = Session::get('UserId');

            $Banks = Banks::where('user_id', $UserId)->get();
            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }

        return view("UI.online_payment_entry.edit_online_payment_entry", compact('title', 'OnlinePaymentEntry', 'Banks', 'Customers', 'Vendors', 'Employees'));
    }

    public function store_payment_entry(Request $request){
       if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $OnlinePaymentEntry = new OnlinePaymentEntry();

        $OnlinePaymentEntry->user_id = $UserId;
        $OnlinePaymentEntry->bank_id = $request->bank_id;
        $OnlinePaymentEntry->ledger_id = $request->ledger_id;
        $OnlinePaymentEntry->payment_mode = $request->payment_mode;
        $OnlinePaymentEntry->payment_date = $request->payment_date;
        $OnlinePaymentEntry->paid_amount = $request->paid_amount;
        $OnlinePaymentEntry->reference_number = $request->ref_number;
        $OnlinePaymentEntry->remarks = $request->remarks;
        $OnlinePaymentEntry->status = 1;

        $AddOnlinePaymentEntry = $OnlinePaymentEntry->save();

        return redirect()->back()->with('message','Online Payment Entry Added Successfully');
    }

    public function update_payment_entry(Request $request){
       if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $OnlinePaymentEntry = OnlinePaymentEntry::where('id', $id)->first();;

        $OnlinePaymentEntry->user_id = $UserId;
        $OnlinePaymentEntry->bank_id = $request->bank_id;
        $OnlinePaymentEntry->ledger_id = $request->ledger_id;
        $OnlinePaymentEntry->payment_mode = $request->payment_mode;
        $OnlinePaymentEntry->payment_date = $request->payment_date;
        $OnlinePaymentEntry->paid_amount = $request->paid_amount;
        $OnlinePaymentEntry->reference_number = $request->ref_number;
        $OnlinePaymentEntry->remarks = $request->remarks;

        $AddOnlinePaymentEntry = $OnlinePaymentEntry->save();

        return redirect()->back()->with('message','Online Payment Entry Updated Successfully');
    }


    public function payment_entry_status(Request $request)
    {
    	// \Log::info($request->all());
        $OnlinePaymentEntry = OnlinePaymentEntry::find($request->id);
        $OnlinePaymentEntry->status = $request->status;
        $OnlinePaymentEntry->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_payment_entry(Request $request)
    {
    	// \Log::info($request->all());
        $OnlinePaymentEntry = OnlinePaymentEntry::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }




    // Online Payment Received Entry
    public function payment_received_list(){
        $title = "Online Payment Received List";
       if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $OnlinePaymentReceived = OnlinePaymentReceived::select('online_payment_received.*', 'banks.bank_name', 'ledger.ledger_name')->join('banks', 'banks.id', '=', 'online_payment_received.bank_id')->join('ledger', 'ledger.id', '=', 'online_payment_received.ledger_id')->where('online_payment_received.user_id', $UserId)->get();

        // $OnlinePaymentEntry = OnlinePaymentEntry::get();

        return view("UI.online_payment_received.received_list", compact('OnlinePaymentReceived', 'title'));
    }

    public function add_payment_received(){
        $title = "New Online Payment Received";
        // $Customers = Customers::get();
        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
            $Banks = Banks::get();
        }else{
            $UserId = Session::get('UserId');

            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }
        return view("UI.online_payment_received.new_payment_received", compact('title',  'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function edit_payment_received($id){
        $title = "Edit Online Payment Entry";
        $OnlinePaymentReceived = OnlinePaymentReceived::where('id', $id)->first();
        if(Auth::guard('super_admin')->check()){
            $Customers = Customers::get();
            $Vendors = Vendors::get();
            $Employees = Employees::get();
            $Banks = Banks::get();
        }else{
            $UserId = Session::get('UserId');

            $Customers = Customers::where('user_id', $UserId)->get();
            $Vendors = Vendors::where('user_id', $UserId)->get();
            $Employees = Employees::where('user_id', $UserId)->get();
            $Banks = Banks::where('user_id', $UserId)->get();
        }
        return view("UI.online_payment_received.edit_payment_received", compact('title', 'OnlinePaymentReceived',  'Customers', 'Vendors', 'Employees', 'Banks'));
    }

    public function store_payment_received(Request $request){
       if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $OnlinePaymentReceived = new OnlinePaymentReceived();

        $OnlinePaymentReceived->user_id = $UserId;
        $OnlinePaymentReceived->bank_id = $request->bank_id;
        $OnlinePaymentReceived->ledger_id = $request->ledger_id;
        $OnlinePaymentReceived->credit_date = $request->credit_date;
        $OnlinePaymentReceived->credited_in_bank = $request->credited_in_bank;
        $OnlinePaymentReceived->ref_number = $request->ref_number;
        $OnlinePaymentReceived->received_for = $request->received_for;
        $OnlinePaymentReceived->status = 1;

        $AddOnlinePaymentReceived = $OnlinePaymentReceived->save();

        return redirect()->back()->with('message','Online Payment Received Added Successfully');
    }

    public function update_payment_received(Request $request){
       if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $id = $request->id;

        $OnlinePaymentReceived = OnlinePaymentReceived::where('id', $id)->first();;

        $OnlinePaymentReceived->user_id = $UserId;
        $OnlinePaymentReceived->bank_id = $request->bank_id;
        $OnlinePaymentReceived->ledger_id = $request->ledger_id;
        $OnlinePaymentReceived->credit_date = $request->credit_date;
        $OnlinePaymentReceived->credited_in_bank = $request->credited_in_bank;
        $OnlinePaymentReceived->ref_number = $request->ref_number;
        $OnlinePaymentReceived->received_for = $request->received_for;
        $OnlinePaymentReceived->status = 1;

        $AddOnlinePaymentReceived = $OnlinePaymentReceived->save();

        $AddOnlinePaymentReceived = $OnlinePaymentReceived->save();

        return redirect()->back()->with('message','Online Payment Received Updated Successfully');
    }


    public function payment_received_status(Request $request)
    {
    	// \Log::info($request->all());
        $OnlinePaymentReceived = OnlinePaymentReceived::find($request->id);
        $OnlinePaymentReceived->status = $request->status;
        $OnlinePaymentReceived->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_payment_received(Request $request)
    {
    	// \Log::info($request->all());
        $OnlinePaymentReceived = OnlinePaymentReceived::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }
    // End
}
