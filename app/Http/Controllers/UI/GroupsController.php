<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\GroupType;
use App\Models\UI\PrimaryGroup;
use App\Models\UI\SubGroup;
use Illuminate\Support\Facades\Auth;

use Session;
class GroupsController extends Controller
{
    // Group Type
    // public $UserId;

    // public function __construct() {
    // //   $this->middleware('auth');
    //     if(Auth::guard('super_admin')->check()){
    //         $UserId = Session::get('AdminId');
    //     }else{
    //         if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }
    //     }

    // }

    public function group_type_lists(){
        $title = "Group Type Lists";
        $GroupType = GroupType::orderBy('created_at', 'DESC')->get();
        return view('UI.group_type.lists', compact('GroupType', 'title'));

    }

    public function add_group_type(Request $request){
        $GroupType = new GroupType();

        $GroupType->name = $request->name;
        $GroupType->status = 1;

        $AddGroupType = $GroupType->save();

        return redirect()->back()->with('message','GroupType Added Successfully');
    }

    public function update_group_type(Request $request){
        $id = $request->id;

        $GroupType = GroupType::where('id', $id)->first();

        $GroupType->name = $request->name;

        $AddGroupType = $GroupType->save();

        return redirect()->back()->with('message','GroupType Updated Successfully');
    }

    public function group_type_status(Request $request)
    {
    	// \Log::info($request->all());
        $GroupType = GroupType::find($request->id);
        $GroupType->status = $request->status;
        $GroupType->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_group_type(Request $request)
    {
    	// \Log::info($request->all());
        $GroupType = GroupType::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }

    // End

    // Primary Group Type

    public function primary_group_lists(){
        $title = "Primary Group Lists";
        $GroupType = GroupType::get();
        $PrimaryGroup = PrimaryGroup::join('group_type', 'primary_group.group_type', '=', 'group_type.id')->select('group_type.name', 'primary_group.group_name', 'primary_group.status', 'primary_group.id', 'primary_group.created_at')->where('primary_group.type_of_group', 1)->orderBy('created_at', 'DESC')->get();
        return view('UI.group_type.primary_group_lists', compact('PrimaryGroup', 'GroupType', 'title'));

    }

    public function add_primary_group(Request $request){
        $PrimaryGroup = new PrimaryGroup();

        $PrimaryGroup->group_type = $request->group_type;
        $PrimaryGroup->group_name = $request->primary_group;
        $PrimaryGroup->type_of_group = 1;

        $AddGroupType = $PrimaryGroup->save();

        return redirect()->back()->with('message','Primary Group Added Successfully');
    }

    public function update_primary_group(Request $request){
        $id = $request->id;

        $PrimaryGroup = PrimaryGroup::where('id', $id)->first();

        $PrimaryGroup->group_type = $request->group_type;
        $PrimaryGroup->group_name = $request->primary_group;

        $AddGroupType = $PrimaryGroup->save();

        return redirect()->back()->with('message','Primary Group Updated Successfully');
    }

    public function primary_group_status(Request $request)
    {
    	// \Log::info($request->all());
        $PrimaryGroup = PrimaryGroup::find($request->id);
        $PrimaryGroup->status = $request->status;
        $PrimaryGroup->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_primary_group(Request $request)
    {
    	// \Log::info($request->all());
        $PrimaryGroup = PrimaryGroup::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }

    // End


    // Sub Group Type

    public function sub_group_lists(){
        $title = "Sub Group Lists";

        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('UserId');
        }

        $PrimaryGroup = PrimaryGroup::where('primary_group.type_of_group', 1)->get();
        $SubGroup = PrimaryGroup::select( 'primary_group.group_name', 'primary_group.status', 'primary_group.id', 'primary_group.created_at')->where('primary_group.type_of_group', 2)->orderBy('created_at', 'DESC')->get();

        return view('UI.group_type.subgroup_lists', compact('PrimaryGroup', 'SubGroup', 'title'));

    }

    public function add_sub_group(Request $request){
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }


        $SubGroup = new PrimaryGroup();

        $SubGroup->user_id = $UserId;
        $SubGroup->primary_group_id = $request->primary_group;
        $SubGroup->group_name = $request->sub_group;
        $SubGroup->type_of_group = 2;

        $AddGroupType = $SubGroup->save();

        return redirect()->back()->with('message','Sub Group Added Successfully');
    }

    public function update_sub_group(Request $request){
        $id = $request->id;
        if(Auth::guard('super_admin')->check()){             $UserId = Session::get('AdminId');         }else{             $UserId = Session::get('UserId');         }

        $SubGroup = PrimaryGroup::where('id', $id)->first();

        $SubGroup->user_id = $UserId;

        $SubGroup->primary_group_id = $request->primary_group;
        $SubGroup->group_name = $request->sub_group;

        $AddGroupType = $SubGroup->save();

        return redirect()->back()->with('message','Sub Group Updated Successfully');
    }

    public function sub_group_status(Request $request)
    {
    	// \Log::info($request->all());
        $SubGroup = SubGroup::find($request->id);
        $SubGroup->status = $request->status;
        $SubGroup->save();

        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function delete_sub_group(Request $request)
    {
    	// \Log::info($request->all());
        $SubGroup = SubGroup::where('id', $request->id)->delete();

        return response()->json(['success'=>'Deleted successfully.']);
    }

    // End
}
