<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnlinePaymentReceived extends Model
{
    use HasFactory;

    protected $table = 'online_payment_received';

    protected $fillable = ['user_id', 'bank_id', 'ledger_id', 'credit_date','credited_in_bank', 'ref_number', 'received_for', 'status'];
}
