<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
    use HasFactory;

    protected $table = 'vendors';

    protected $fillable = ['name', 'group_type_id', 'vendor_type', 'pan_no','gst_no', 'bussiness_category', 'country', 'state', 'city', 'pin_code', 'address_1', 'address_2', 'contac_person_name', 'contact_person_designation', 'phone', 'email', 'pan_copy', 'gst_copy', 'company_incorporation', 'aggreement_contract_copy', 'status'];
}
