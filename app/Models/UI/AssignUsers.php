<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssignUsers extends Model
{
    use HasFactory;

    protected $table = 'assign_users';

    protected $fillable = ['user_id', 'company_id'];
}
