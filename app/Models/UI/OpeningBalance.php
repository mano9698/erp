<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OpeningBalance extends Model
{
    use HasFactory;

    protected $table = 'opening_balance';

    protected $fillable = ['user_id', 'ledger_id', 'balance_date', 'balance_amount', 'status'];
}
