<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashReceivedEntry extends Model
{
    use HasFactory;

    protected $table = 'cash_received_entry';

    protected $fillable = ['user_id', 'cash_in_hand_group', 'ledger_id', 'cash_received_date', 'cash_amount_received', 'received_for', 'status'];
}
