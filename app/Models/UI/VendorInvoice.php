<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorInvoice extends Model
{
    use HasFactory;

    protected $table = 'vendor_invoice';

    protected $fillable = ['vendor_id', 'invoice_number', 'invoice_date', 'status'];
}
