<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banks extends Model
{
    use HasFactory;

    protected $table = 'banks';

    protected $fillable = ['user_id', 'bank_name', 'account_number', 'ifsc_code', 'bank_branch', 'status'];
}
