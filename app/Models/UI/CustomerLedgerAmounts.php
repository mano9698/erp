<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerLedgerAmounts extends Model
{
    use HasFactory;

    protected $table = 'customer_ledger_amounts';

    protected $fillable = ['customer_invoice_id', 'ledger_id', 'cost_id', 'amount'];
}
