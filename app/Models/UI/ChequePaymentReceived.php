<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChequePaymentReceived extends Model
{
    use HasFactory;

    protected $table = 'cheque_payment_received';

    protected $fillable = ['user_id', 'ledger_id', 'cheque_number','cheque_date', 'cheque_amount', 'received_for', 'status'];
}
