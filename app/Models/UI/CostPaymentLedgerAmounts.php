<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CostPaymentLedgerAmounts extends Model
{
    use HasFactory;

    protected $table = 'cost_payment_ledger_amount';

    protected $fillable = ['cost_payment_id', 'ledger_id', 'cost_id', 'amount'];
}
