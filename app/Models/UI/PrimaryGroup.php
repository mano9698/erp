<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrimaryGroup extends Model
{
    use HasFactory;

    protected $table = 'primary_group';

    protected $fillable = ['user_id','primary_group_id','group_type', 'group_name', 'group_category', 'status', 'type_of_group'];
}
