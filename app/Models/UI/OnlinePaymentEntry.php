<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnlinePaymentEntry extends Model
{
    use HasFactory;

    protected $table = 'online_payment_entry';

    protected $fillable = ['user_id', 'bank_id', 'ledger_id', 'payment_mode','payment_date', 'paid_amount', 'reference_number', 'remarks', 'status'];
}
