<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeSalaryAmounts extends Model
{
    use HasFactory;

    protected $table = 'employee_salary_amounts';

    protected $fillable = ['emp_salary_id', 'ledger_id', 'amount'];
}
