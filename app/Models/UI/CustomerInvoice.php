<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerInvoice extends Model
{
    use HasFactory;

    protected $table = 'customer_invoice';

    protected $fillable = ['customer_id', 'invoice_number', 'invoice_date', 'status'];



    public function CustomerLedgerAmounts(){
        return  $this->hasMany(\App\Models\UI\CustomerLedgerAmounts::class, 'customer_invoice_id','id')->select(['customer_invoice_id','ledger_id','amount']);
    }
}
