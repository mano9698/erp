<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubGroup extends Model
{
    use HasFactory;

    protected $table = 'sub_group';

    protected $fillable = ['user_id','primary_group_id', 'subgroup_name', 'status'];
}
