<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashDepositedBank extends Model
{
    use HasFactory;

    protected $table = 'cash_deposited_bank';

    protected $fillable = ['user_id', 'cash_in_hand_group', 'bank_id', 'cash_deposited_date','cash_deposited_amount', 'cash_deposited_by' ,'status'];
}
