<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    use HasFactory;

    protected $table = 'employees';

    protected $fillable = ['group_type_id','user_id', 'first_name', 'middle_name', 'last_name','dob', 'pan_no', 'aadhar', 'joining_date', 'designation', 'department', 'reporting_manager', 'gross_salary', 'annual_ctc', 'country', 'state', 'city', 'pin_code', 'address_1', 'address_2', 'blood_group', 'phone', 'email', 'pan_copy', 'aadhar_copy', 'passport_photo', 'offer_letter', 'status'];
}
