<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChequePaymentEntry extends Model
{
    use HasFactory;

    protected $table = 'cheque_payment_entry';

    protected $fillable = ['user_id', 'bank_id', 'ledger_id', 'cheque_number','cheque_date', 'cheque_amount', 'remarks', 'status'];
}
