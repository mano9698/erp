<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChequeDepositeReceived extends Model
{
    use HasFactory;

    protected $table = 'cheque_deposite_received';

    protected $fillable = ['user_id', 'cheque_id', 'bank_id','cheque_date', 'status'];
}
