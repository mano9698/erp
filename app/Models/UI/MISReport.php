<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MISReport extends Model
{
    use HasFactory;

    protected $table = 'mis_report';

    protected $fillable = ['company_id', 'title', 'month', 'year','description'];
}
