<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankStatement extends Model
{
    use HasFactory;

    protected $table = 'bank_statement';

    protected $fillable = ['user_id', 'bank_id', 'ledger_id', 'payment_mode', 'payment_reference_number', 'value_date', 'description', 'amount', 'status'];
}
