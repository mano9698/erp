<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LedgerAdjustmentEntry extends Model
{
    use HasFactory;

    protected $table = 'ledger_adjustment_entry';

    protected $fillable = ['user_id', 'date_of_adjustment', 'from_ledger_id', 'to_ledger_id', 'amount_to_transfer', 'remarks', 'status'];
}
