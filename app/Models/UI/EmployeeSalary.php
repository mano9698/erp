<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeSalary extends Model
{
    use HasFactory;

    protected $table = 'employee_salary';

    protected $fillable = ['user_id','emp_id', 'salary_month', 'due_date_date','total_amount', 'status'];



    public function EmployeeSalaryAmounts(){
        return  $this->hasMany(\App\Models\UI\EmployeeSalaryAmounts::class, 'emp_salary_id','id')->select(['emp_salary_id','ledger_id','amount']);
    }
}
