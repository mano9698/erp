<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorLedgerAmounts extends Model
{
    use HasFactory;

    protected $table = 'vendor_ledger_amounts';

    protected $fillable = ['vendor_invoice_id', 'ledger_id','cost_id', 'amount'];
}
