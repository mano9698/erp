<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CashPaymentEntry extends Model
{
    use HasFactory;

    protected $table = 'cash_payment_entry';

    protected $fillable = ['user_id', 'cash_in_hand_group', 'ledger_id', 'payment_date','amount', 'remarks' ,'status'];
}
