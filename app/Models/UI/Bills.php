<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bills extends Model
{
    use HasFactory;

    protected $table = 'bills';

    protected $fillable = ['user_id', 'bill_type', 'bill_date','cheque_date', 'bill_no', 'party_name', 'amount', 'short_description'];

}
