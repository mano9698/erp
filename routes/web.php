<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('charts', 'UI\HomeController@chats');

Route::post('chart/fetch_data/{id}', 'UI\HomeController@fetch_data');

Route::get('chart/bar_chats', 'UI\HomeController@bar_chats');

Route::get('/', 'UI\AuthendicationController@login');

Route::get('/register', 'UI\AuthendicationController@register');

Route::post('/add_users', 'UI\AuthendicationController@add_users');

Route::post('/add_companies', 'UI\AuthendicationController@add_companies');

Route::post('/login', 'UI\AuthendicationController@admin_login');

Route::group(['prefix' => '/admin'], function () {

    Route::get('/dashboard', 'UI\HomeController@home');

    Route::get('/reports_list', 'UI\ReportsController@reports_list');

    Route::get('/admin_logout', 'UI\AuthendicationController@admin_logout');


});


Route::group(['prefix' => '/admin/companies'], function () {


    Route::get('/list', 'UI\HomeController@companies_list');

    Route::post('/change_status', 'UI\HomeController@change_status');

    Route::get('/edit_user/{id}', 'UI\HomeController@edit_user');

    Route::post('/update_users', 'UI\AuthendicationController@update_users');

    Route::get('/company_logout', 'UI\AuthendicationController@company_logout');

    Route::get('/manager_logout', 'UI\AuthendicationController@manager_logout');

    Route::get('/accountant_logout', 'UI\AuthendicationController@accountant_logout');

    Route::get('/hr_logout', 'UI\AuthendicationController@hr_logout');
});

Route::group(['prefix' => '/admin/bills'], function () {
    Route::get('/add_bills', 'UI\HomeController@add_bills');

    Route::get('/view_bills', 'UI\HomeController@view_bills');

    Route::post('/store_bills', 'UI\HomeController@store_bills');
});

Route::group(['prefix' => '/admin/cost_center'], function () {
    Route::get('/list', 'UI\HomeController@cost_center_lists');

    Route::post('/add_cost_center', 'UI\HomeController@add_cost_center');

    Route::post('/update_cost_center', 'UI\HomeController@update_cost_center');

    Route::post('/group_cost_center', 'UI\HomeController@group_cost_center');

    Route::post('/delete_cost_center', 'UI\HomeController@delete_cost_center');
});

Route::group(['prefix' => '/admin/documents'], function () {
    Route::get('/add_upload_document', 'UI\HomeController@add_upload_document');

    Route::get('/view_documents', 'UI\HomeController@view_documents');

    Route::post('/store_documents', 'UI\HomeController@store_documents');
});


Route::group(['prefix' => '/admin/users'], function () {


    Route::get('/list', 'UI\HomeController@users_list');

    Route::post('/change_status', 'UI\HomeController@change_status');

    Route::get('/add_user', 'UI\HomeController@add_user');

    Route::post('/add_assign_users', 'UI\HomeController@add_assign_users');

    Route::get('/assign_user/{id}', 'UI\HomeController@assign_user');

    Route::get('/edit_user/{id}', 'UI\HomeController@edit_user');

    Route::get('/change_password', 'UI\HomeController@change_password');

    Route::post('/update_users', 'UI\AuthendicationController@update_users');

    Route::get('/change_user_roles', 'UI\HomeController@change_user_roles');

    Route::get('/logout', 'UI\AuthendicationController@user_logout');

    Route::post('/update_assign_users', 'UI\HomeController@update_assign_users');

    Route::post('/change_roles', 'UI\HomeController@change_roles');

    Route::post('/update_password', 'UI\HomeController@update_password');

    Route::get('/delete_assign_users/{id}', 'UI\HomeController@delete_assign_users');
});


Route::group(['prefix' => '/admin/group_type'], function () {

    Route::get('/list', 'UI\GroupsController@group_type_lists');

    Route::post('/add_group_type', 'UI\GroupsController@add_group_type');

    Route::post('/update_group_type', 'UI\GroupsController@update_group_type');

    Route::post('/group_type_status', 'UI\GroupsController@group_type_status');

    Route::post('/delete_group_type', 'UI\GroupsController@delete_group_type');

});

Route::group(['prefix' => '/admin/primary_group'], function () {

    Route::get('/list', 'UI\GroupsController@primary_group_lists');

    Route::post('/add_primary_group', 'UI\GroupsController@add_primary_group');

    Route::post('/update_primary_group', 'UI\GroupsController@update_primary_group');

    Route::post('/primary_group_status', 'UI\GroupsController@primary_group_status');

    Route::post('/delete_primary_group', 'UI\GroupsController@delete_primary_group');

});

Route::group(['prefix' => '/admin/sub_group'], function () {

    Route::get('/list', 'UI\GroupsController@sub_group_lists');

    Route::post('/add_sub_group', 'UI\GroupsController@add_sub_group');

    Route::post('/update_sub_group', 'UI\GroupsController@update_sub_group');

    Route::post('/sub_group_status', 'UI\GroupsController@sub_group_status');

    Route::post('/delete_sub_group', 'UI\GroupsController@delete_sub_group');

});


Route::group(['prefix' => '/admin/ledger'], function () {

    Route::get('/list', 'UI\LedgerController@ledger_lists');

    Route::post('/add_ledger', 'UI\LedgerController@add_ledger');

    Route::post('/update_ledger', 'UI\LedgerController@update_ledger');

    Route::post('/ledger_status', 'UI\LedgerController@ledger_status');

    Route::post('/delete_ledger', 'UI\LedgerController@delete_ledger');

});

Route::group(['prefix' => '/admin/customers'], function () {

    Route::get('/list', 'UI\CustomersController@customers_list');

    Route::get('/add_customer', 'UI\CustomersController@add_customer');

    Route::post('/store_customer', 'UI\CustomersController@store_customer');

    Route::get('/edit_customer/{id}', 'UI\CustomersController@edit_customer');

    Route::post('/update_customer', 'UI\CustomersController@update_customer');

    Route::post('/customer_status', 'UI\CustomersController@customer_status');

    Route::post('/delete_customers', 'UI\CustomersController@delete_customers');

});

// Customer Invoice
Route::group(['prefix' => '/admin/customer_invoice'], function () {

    Route::get('/list', 'UI\CustomersController@customers_invoice_list');

    Route::get('/add_customer_invoice', 'UI\CustomersController@add_customer_invoice');

    Route::post('/store_customer_invoice', 'UI\CustomersController@store_customer_invoice');


    Route::get('/edit_invoice/{id}', 'UI\CustomersController@edit_invoice');

    Route::post('/update_customer_invoice', 'UI\CustomersController@update_customer_invoice');

    Route::post('/customer_invoice_status', 'UI\CustomersController@customer_invoice_status');

    Route::post('/delete_customer_invoice', 'UI\CustomersController@delete_customer_invoice');

    Route::post('/delete_customers_ledger_amounts', 'UI\CustomersController@delete_customers_ledger_amounts');

});



Route::group(['prefix' => '/admin/vendors'], function () {

    Route::get('/list', 'UI\VendorsController@vendors_list');

    Route::get('/add_vendor', 'UI\VendorsController@add_vendor');

    Route::post('/store_vendors', 'UI\VendorsController@store_vendors');

    Route::get('/edit_vendor/{id}', 'UI\VendorsController@edit_vendor');

    Route::post('/update_vendor', 'UI\VendorsController@update_vendor');

    Route::post('/vendor_status', 'UI\VendorsController@vendor_status');

    Route::post('/delete_vendors', 'UI\VendorsController@delete_vendors');

});


//Vendor Invoice
Route::group(['prefix' => '/admin/vendor_invoice'], function () {

    Route::get('/list', 'UI\VendorsController@vendors_invoice_list');

    Route::get('/add_vendor_invoice', 'UI\VendorsController@add_vendor_invoice');

    Route::post('/store_vendor_invoice', 'UI\VendorsController@store_vendor_invoice');


    Route::get('/edit_invoice/{id}', 'UI\VendorsController@edit_invoice');

    Route::post('/update_vendor_invoice', 'UI\VendorsController@update_vendor_invoice');

    Route::post('/vendor_invoice_status', 'UI\VendorsController@vendor_invoice_status');

    Route::post('/delete_vendor_invoice', 'UI\VendorsController@delete_vendor_invoice');

    Route::post('/delete_vendors_ledger_amounts', 'UI\VendorsController@delete_vendors_ledger_amounts');

});




Route::group(['prefix' => '/admin/employees'], function () {

    Route::get('/list', 'UI\EmployeesController@employees_list');

    Route::get('/add_employee', 'UI\EmployeesController@add_employee');

    Route::post('/store_employee', 'UI\EmployeesController@store_employee');

    Route::get('/edit_employee/{id}', 'UI\EmployeesController@edit_employee');

    Route::post('/update_employee', 'UI\EmployeesController@update_employee');

    Route::post('/employee_status', 'UI\EmployeesController@employee_status');

    Route::post('/delete_employee', 'UI\EmployeesController@delete_employee');

});

// Employee Salary
Route::group(['prefix' => '/admin/emp_salary'], function () {

    Route::get('/list', 'UI\EmployeesController@emp_salary_list');

    Route::get('/add_employee_salary', 'UI\EmployeesController@add_employee_salary');

    Route::post('/store_emp_salary', 'UI\EmployeesController@store_emp_salary');


    Route::get('/edit_employee_salary/{id}', 'UI\EmployeesController@edit_employee_salary');

    Route::post('/update_emp_salary', 'UI\EmployeesController@update_emp_salary');

    // Route::post('/customer_invoice_status', 'UI\EmployeesController@customer_invoice_status');

    Route::post('/delete_employee_salary', 'UI\EmployeesController@delete_employee_salary');

    Route::post('/delete_employee_salary_amount', 'UI\EmployeesController@delete_employee_salary_amount');

});


Route::group(['prefix' => '/admin/banks'], function () {

    Route::get('/list', 'UI\BanksController@banks_list');

    Route::get('/add_bank', 'UI\BanksController@add_bank');

    Route::post('/store_banks', 'UI\BanksController@store_banks');

    Route::get('/edit_bank/{id}', 'UI\BanksController@edit_bank');

    Route::post('/update_bank', 'UI\BanksController@update_bank');

    Route::post('/bank_status', 'UI\BanksController@bank_status');

    Route::post('/delete_bank', 'UI\BanksController@delete_bank');

});


Route::group(['prefix' => '/admin/banks_statement'], function () {

    Route::get('list', 'UI\BanksController@banks_statement_list');

    Route::get('confirm_banks_statement_list', 'UI\BanksController@confirm_banks_statement_list');

    Route::post('importExcel', 'UI\BanksController@importExcel');

    Route::post('update_bank_statement', 'UI\BanksController@update_bank_statement');

    Route::post('update_confirm_bank_statement', 'UI\BanksController@update_confirm_bank_statement');

    Route::get('delete_bank_statement/{id}', 'UI\BanksController@delete_bank_statement');

    Route::delete('delete_all_bank_statement', 'UI\BanksController@delete_all_bank_statement');
});








//Bank Online Payment Entry
Route::group(['prefix' => '/admin/online_payment_entry'], function () {

    Route::get('/list', 'UI\OnlinePaymentEntryController@payment_entry_list');

    Route::get('/add_payment_entry', 'UI\OnlinePaymentEntryController@add_payment_entry');

    Route::post('/store_payment_entry', 'UI\OnlinePaymentEntryController@store_payment_entry');


    Route::get('/edit_payment_entry/{id}', 'UI\OnlinePaymentEntryController@edit_payment_entry');

    Route::post('/update_payment_entry', 'UI\OnlinePaymentEntryController@update_payment_entry');

    Route::post('/payment_entry_status', 'UI\OnlinePaymentEntryController@payment_entry_status');

    Route::post('/delete_payment_entry', 'UI\OnlinePaymentEntryController@delete_payment_entry');


});
// End

//Bank Online Payment Received
Route::group(['prefix' => '/admin/online_payment_received'], function () {

    Route::get('/list', 'UI\OnlinePaymentEntryController@payment_received_list');

    Route::get('/add_payment_received', 'UI\OnlinePaymentEntryController@add_payment_received');

    Route::post('/store_payment_received', 'UI\OnlinePaymentEntryController@store_payment_received');

    Route::get('/edit_payment_received/{id}', 'UI\OnlinePaymentEntryController@edit_payment_received');

    Route::post('/update_payment_received', 'UI\OnlinePaymentEntryController@update_payment_received');

    Route::post('/payment_received_status', 'UI\OnlinePaymentEntryController@payment_received_status');

    Route::post('/delete_payment_received', 'UI\OnlinePaymentEntryController@delete_payment_received');


});
// End


//Cheque Online Payment Entry
Route::group(['prefix' => '/admin/cheque_payment_entry'], function () {

    Route::get('/list', 'UI\ChequePaymentEntryController@cheque_entry_list');

    Route::get('/add_cheque_entry', 'UI\ChequePaymentEntryController@add_cheque_entry');

    Route::post('/store_cheque_entry', 'UI\ChequePaymentEntryController@store_cheque_entry');


    Route::get('/edit_cheque_entry/{id}', 'UI\ChequePaymentEntryController@edit_cheque_entry');

    Route::post('/update_cheque_entry', 'UI\ChequePaymentEntryController@update_cheque_entry');

    Route::post('/cheque_entry_status', 'UI\ChequePaymentEntryController@cheque_entry_status');

    Route::post('/delete_cheque_entry', 'UI\ChequePaymentEntryController@delete_cheque_entry');


});

// End

//Cheque Online Payment Received
Route::group(['prefix' => '/admin/cheque_payment_received'], function () {

    Route::get('/list', 'UI\ChequePaymentEntryController@cheque_received_list');

    Route::get('/add_cheque_received', 'UI\ChequePaymentEntryController@add_cheque_received');

    Route::post('/store_cheque_received', 'UI\ChequePaymentEntryController@store_cheque_received');


    Route::get('/edit_cheque_received/{id}', 'UI\ChequePaymentEntryController@edit_cheque_received');

    Route::post('/update_cheque_received', 'UI\ChequePaymentEntryController@update_cheque_received');

    Route::post('/cheque_received_status', 'UI\ChequePaymentEntryController@cheque_received_status');

    Route::post('/delete_cheque_received', 'UI\ChequePaymentEntryController@delete_cheque_received');


});
// End

//Cheque Payment Deposite Received
Route::group(['prefix' => '/admin/cheque_payment__deposite_received'], function () {

    Route::get('/list', 'UI\ChequePaymentEntryController@cheque_deposite_list');

    Route::get('/add_cheque_deposite', 'UI\ChequePaymentEntryController@add_cheque_deposite');

    Route::post('/store_cheque_deposite', 'UI\ChequePaymentEntryController@store_cheque_deposite');


    Route::get('/edit_cheque_deposite/{id}', 'UI\ChequePaymentEntryController@edit_cheque_deposite');

    Route::post('/update_cheque_deposite', 'UI\ChequePaymentEntryController@update_cheque_deposite');

    // Route::post('/cheque_received_status', 'UI\ChequePaymentEntryController@cheque_received_status');

    Route::post('/delete_cheque_deposite', 'UI\ChequePaymentEntryController@delete_cheque_deposite');


});
// End




//Cash Payment Entry
Route::group(['prefix' => '/admin/cash_payment'], function () {

    Route::get('/list', 'UI\CashController@cash_payment_entry_list');

    Route::get('/add_cash_entry', 'UI\CashController@add_cash_entry');

    Route::post('/store_cash_payment_entry', 'UI\CashController@store_cash_payment_entry');


    Route::get('/edit_cash_payment_entry/{id}', 'UI\CashController@edit_cash_payment_entry');

    Route::post('/update_cash_payment_entry', 'UI\CashController@update_cash_payment_entry');

    // Route::post('/cheque_received_status', 'UI\ChequePaymentEntryController@cheque_received_status');

    Route::post('/delete_cash_payment_entry', 'UI\CashController@delete_cash_payment_entry');

    Route::post('/delete_cash_ledger_amount', 'UI\CashController@delete_cash_ledger_amount');


});
// End



//Cash Received Entry
Route::group(['prefix' => '/admin/cash_received'], function () {

    Route::get('/list', 'UI\CashController@cash_received_entry_list');

    Route::get('/add_cash_received_entry', 'UI\CashController@add_cash_received_entry');

    Route::post('/store_cash_received_entry', 'UI\CashController@store_cash_received_entry');


    Route::get('/edit_cash_received_entry/{id}', 'UI\CashController@edit_cash_received_entry');

    Route::post('/update_cash_received_entry', 'UI\CashController@update_cash_received_entry');

    // Route::post('/cheque_received_status', 'UI\ChequePaymentEntryController@cheque_received_status');

    Route::post('/delete_cash_received_entry', 'UI\CashController@delete_cash_received_entry');
});
// End


//Cash Deposited Entry
Route::group(['prefix' => '/admin/cash_deposited'], function () {

    Route::get('/list', 'UI\CashController@cash_deposited_entry_list');

    Route::get('/add_cash_deposited_entry', 'UI\CashController@add_cash_deposited_entry');

    Route::post('/store_cash_deposited_entry', 'UI\CashController@store_cash_deposited_entry');


    Route::get('/edit_cash_deposited_entry/{id}', 'UI\CashController@edit_cash_deposited_entry');

    Route::post('/update_cash_deposited_entry', 'UI\CashController@update_cash_deposited_entry');

    // Route::post('/cheque_received_status', 'UI\ChequePaymentEntryController@cheque_received_status');

    Route::post('/delete_cash_deposited_entry', 'UI\CashController@delete_cash_deposited_entry');
});
// End



//Cash Deposited Entry
Route::group(['prefix' => '/admin/ledger_adjustment'], function () {

    Route::get('/list', 'UI\LedgerController@ledger_adjustment_entry_list');

    Route::get('/add_adjustment_entry', 'UI\LedgerController@add_adjustment_entry');

    Route::post('/store_adjustment_entry', 'UI\LedgerController@store_adjustment_entry');


    Route::get('/edit_adjustment_entry/{id}', 'UI\LedgerController@edit_adjustment_entry');

    Route::post('/update_adjustment_entry', 'UI\LedgerController@update_adjustment_entry');

    // Route::post('/cheque_received_status', 'UI\ChequePaymentEntryController@cheque_received_status');

    Route::post('/delete_adjustment_entry', 'UI\LedgerController@delete_adjustment_entry');
});
// End


//Opening Balance Lists
Route::group(['prefix' => '/admin/opening_balance'], function () {

    Route::get('/list', 'UI\LedgerController@opening_balance_list');

    Route::get('/add_opening_balance', 'UI\LedgerController@add_opening_balance');

    Route::post('/store_opening_balance', 'UI\LedgerController@store_opening_balance');


    Route::get('/edit_opening_balance/{id}', 'UI\LedgerController@edit_opening_balance');

    Route::post('/update_opening_balance', 'UI\LedgerController@update_opening_balance');

    // Route::post('/cheque_received_status', 'UI\ChequePaymentEntryController@cheque_received_status');

    Route::post('/delete_opening_balance', 'UI\LedgerController@delete_opening_balance');
});
// End



//ReportsLists
Route::group(['prefix' => '/admin/reports'], function () {

    Route::get('/list', 'UI\ReportsController@ledger_reports_list');

    // Route::get('/add_opening_balance', 'UI\LedgerController@add_opening_balance');

    Route::post('/get_reports_by_ledger', 'UI\ReportsController@get_reports_by_ledger');


    // Route::get('/edit_opening_balance/{id}', 'UI\LedgerController@edit_opening_balance');

    // Route::post('/update_opening_balance', 'UI\LedgerController@update_opening_balance');

    // // Route::post('/cheque_received_status', 'UI\ChequePaymentEntryController@cheque_received_status');

    // Route::post('/delete_opening_balance', 'UI\LedgerController@delete_opening_balance');
});
// End


// MIS Report
Route::group(['prefix' => '/mis_reports'], function () {

    Route::get('/list', 'UI\ReportsController@mis_reports_list');

    Route::get('/add_mis_reports', 'UI\ReportsController@add_mis_reports');

    Route::get('/edit_mis_reports/{id}', 'UI\ReportsController@edit_mis_reports');

    Route::get('/view_mis_reports/{id}', 'UI\ReportsController@view_mis_reports');

    Route::post('/store_msi_report', 'UI\ReportsController@store_msi_report');

    Route::post('/update_msi_report', 'UI\ReportsController@update_msi_report');

    Route::get('/delete_mis_report/{id}', 'UI\ReportsController@delete_mis_report');
});
// End



Route::group(['prefix' => '/invoice'], function () {
    Route::get('/design', 'UI\HomeController@invoice_design');
});
