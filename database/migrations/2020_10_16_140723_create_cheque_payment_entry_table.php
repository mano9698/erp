<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChequePaymentEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheque_payment_entry', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('bank_id');
            $table->string('ledger_id');
            $table->string('cheque_number');
            $table->string('cheque_date');
            $table->string('cheque_amount');
            $table->longText('remarks');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheque_payment_entry');
    }
}
