<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('group_type_id');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('dob');
            $table->string('pan_no');
            $table->string('aadhar');
            $table->string('joining_date');
            $table->string('designation');
            $table->string('department');
            $table->string('reporting_manager');
            $table->string('gross_salary');
            $table->string('annual_ctc');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('pin_code');
            $table->string('address_1');
            $table->string('address_2');
            $table->string('blood_group');
            $table->string('phone');
            $table->string('email');
            $table->string('pan_copy');
            $table->string('aadhar_copy');
            $table->string('passport_photo');
            $table->string('offer_letter');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
