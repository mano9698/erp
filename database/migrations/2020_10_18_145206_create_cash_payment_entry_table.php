<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashPaymentEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_payment_entry', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('cash_in_hand_group');
            $table->string('ledger_id');
            $table->string('payment_date');
            $table->string('amount');
            $table->longText('remarks');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_payment_entry');
    }
}
