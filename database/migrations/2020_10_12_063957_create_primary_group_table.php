<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrimaryGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('primary_group', function (Blueprint $table) {
            $table->id();
            $table->string('user_id')->nullable();
            $table->string('primary_group_id')->nullable();
            $table->string('group_type')->nullable();
            $table->string('group_name');
            // $table->string('group_category');
            $table->boolean('status')->nullable();
            $table->boolean('type_of_group');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('primary_group');
    }
}
