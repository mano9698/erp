<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankStatementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_statement', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('bank_id')->nullable();
            $table->string('ledger_id')->nullable();
            $table->string('payment_mode')->nullable();
            $table->string('payment_reference_number')->nullable();
            $table->string('value_date')->nullable();
            $table->string('description')->nullable();
            $table->string('amount')->nullable();
            $table->boolean('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_statement');
    }
}
