<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLedgerAdjustmentEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ledger_adjustment_entry', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('date_of_adjustment');
            $table->string('from_ledger_id');
            $table->string('to_ledger_id');
            $table->string('amount_to_transfer');
            $table->longText('remarks');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ledger_adjustment_entry');
    }
}
