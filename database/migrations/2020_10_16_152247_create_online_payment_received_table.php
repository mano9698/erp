<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlinePaymentReceivedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_payment_received', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('bank_id');
            $table->string('ledger_id');
            $table->string('credit_date');
            $table->string('credited_in_bank');
            $table->string('ref_number');
            $table->longText('received_for');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheque_payment_received');
    }
}
