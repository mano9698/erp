<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashReceivedEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cash_received_entry', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('cash_in_hand_group');
            $table->string('ledger_id');
            $table->string('cash_received_date');
            $table->string('cash_amount_received');
            $table->longText('received_for');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cash_received_entry');
    }
}
