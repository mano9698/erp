<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOnlinePaymentEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_payment_entry', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('bank_id');
            $table->string('ledger_id');
            $table->string('payment_mode');
            $table->string('payment_date');
            $table->string('paid_amount');
            $table->string('reference_number');
            $table->longText('remarks');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_payment_entry');
    }
}
