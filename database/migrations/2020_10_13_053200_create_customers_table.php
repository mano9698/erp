<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('name');
            $table->string('group_type_id');
            $table->string('customer_type');
            $table->string('pan_no');
            $table->string('gst_no');
            $table->string('bussiness_category');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('pin_code');
            $table->string('address_1');
            $table->string('address_2');
            $table->string('contac_person_name');
            $table->string('contact_person_designation');
            $table->string('phone');
            $table->string('email');
            $table->string('pan_copy');
            $table->string('gst_copy');
            $table->string('company_incorporation');
            $table->string('aggreement_contract_copy');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
