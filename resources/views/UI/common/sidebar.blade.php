@if(Auth::guard('super_admin')->check())
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile d-flex no-block dropdown mt-3">
                        <div class="user-pic"><img src="{{URL::asset('UI/assets/images/users/1.jpg')}}" alt="users" class="rounded-circle" width="40" /></div>
                        <div class="user-content hide-menu ml-2">
                            <a href="javascript:void(0)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(Auth::guard('super_admin')->check())
                                <h5 class="mb-0 user-name font-medium">{{Session::get('AdminName')}} </h5>
                                <span class="op-5 user-email">{{Session::get('AdminEmail')}}</span>
                                @elseif(Auth::guard('user')->check())
                                <h5 class="mb-0 user-name font-medium">{{Session::get('Username')}} </h5>
                                <span class="op-5 user-email">{{Session::get('UserEmail')}}</span>
                                @endif

                            </a>
                        </div>
                    </div>
                    <!-- End User Profile-->
                </li>
                @if(Auth::guard('super_admin')->check())

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Companies</span></li> --}}
                <li class="sidebar-item"><a href="/admin/dashboard" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu main_menu-clr"> Dashboard </span></a></li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Companies </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <!-- <li class="sidebar-item"><a href="new_user.html" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Add New User </span></a></li> -->

                        <li class="sidebar-item"><a href="/admin/companies/list" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Companies List </span></a></li>
                    </ul>
                </li>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Cost Center </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <!-- <li class="sidebar-item"><a href="new_user.html" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Add New User </span></a></li> -->

                        <li class="sidebar-item"><a href="/admin/cost_center/list" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Cost Center List </span></a></li>
                    </ul>
                </li>
                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Users</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Users </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <!-- <li class="sidebar-item"><a href="new_user.html" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Add New User </span></a></li> -->

                        <li class="sidebar-item"><a href="/admin/users/list" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Users List </span></a></li>
                    </ul>
                </li>

                @endif
                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Bills</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Bills </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                         <li class="sidebar-item"><a href="/admin/bills/view_bills" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Bills </span></a></li>

                        <li class="sidebar-item"><a href="/admin/bills/add_bills" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> New Bills </span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Documents</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Documents </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                         <li class="sidebar-item"><a href="/admin/documents/view_documents" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Documents </span></a></li>

                        <li class="sidebar-item"><a href="/admin/documents/add_upload_document" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> New Documents </span></a></li>
                    </ul>
                </li>

                <!-- User Profile-->
                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Groups</span></li> --}}
                @if(Auth::guard('super_admin')->check())
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-content-copy"></i><span class="hide-menu main_menu-clr">Group Types</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        {{-- <li class="sidebar-item"><a href="new_group.html" class="sidebar-link"><i class="mdi mdi-format-align-left"></i><span class="hide-menu"> Add New Group Type</span></a></li> --}}

                        <li class="sidebar-item"><a href="/admin/group_type/list" class="sidebar-link"><i class="mdi mdi-format-align-left"></i><span class="hide-menu"> Group Type List</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-content-copy"></i><span class="hide-menu main_menu-clr">Primary Groups</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item"><a href="/admin/primary_group/list" class="sidebar-link"><i class="mdi mdi-format-align-left"></i><span class="hide-menu"> Group List</span></a></li>
                    </ul>
                </li>
                @endif
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-content-copy"></i><span class="hide-menu main_menu-clr">Sub Groups</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item"><a href="/admin/sub_group/list" class="sidebar-link"><i class="mdi mdi-format-align-left"></i><span class="hide-menu">Sub Group List</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Ledgers</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-inbox-arrow-down"></i><span class="hide-menu main_menu-clr">Ledgers </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item"><a href="/admin/ledger/list" class="sidebar-link"><i class="mdi mdi-email"></i><span class="hide-menu"> Ledger List</span></a></li>
                    </ul>
                </li>
                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Customers</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu main_menu-clr">Customers</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/customers/list" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> Customers List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/customer_invoice/list" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Customer Invoice Entry</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Vendors</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Vendors</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/vendors/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Vendors List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/vendor_invoice/list" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Vendors Invoice Entry</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Employee</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Employee</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/employees/list" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Employee Lists</span></a></li>

                        <li class="sidebar-item"><a href="/admin/emp_salary/list" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Employee Salary Lists</span></a></li>
                        {{-- <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Salary Entry</span></a></li> --}}
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Bank Accounts</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Bank Accounts</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/banks/add_bank" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Add Bank Account</span></a></li>
                        <li class="sidebar-item"><a href="/admin/banks/list" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Bank Accounts List</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Bank Statements</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/banks_statement/list" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Bank Statements List</span></a></li>

                        <li class="sidebar-item"><a href="/admin/banks_statement/confirm_banks_statement_list" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Confirm Bank Statements List</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Bank Entries</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Bank Online Payment Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/online_payment_entry/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Bank Online Payment Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/online_payment_entry/add_payment_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Bank Online Payment Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cheque Payment Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cheque_payment_entry/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cheque Payment Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cheque_payment_entry/add_cheque_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cheque Payment Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Online Payment Received Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/online_payment_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Online Payment Received Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/online_payment_received/add_payment_received" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Online Payment Received Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cheque Received Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cheque_payment_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cheque Received Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cheque_payment_received/add_cheque_received" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cheque Received Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cheque Deposited Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cheque_payment__deposite_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cheque Deposited Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cheque_payment__deposite_received/add_cheque_deposite" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cheque Deposited Entry</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Cash Accounts</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cash Payment Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cash_payment/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Payment Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cash_payment/add_cash_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cash Payment Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cash Received Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cash_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Received Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cash_received/add_cash_received_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cash Received Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cash Deposited in Bank</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cash_deposited/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Deposited in Bank List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cash_deposited/add_cash_deposited_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cash Deposited in Bank</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Journal Entries </span></li> --}}

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Ledger Adjustment Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/ledger_adjustment/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Ledger Adjustment Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/ledger_adjustment/add_adjustment_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Ledger Adjustment Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Opening Balance</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/opening_balance/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Opening Balance List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/opening_balance/add_opening_balance" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Opening Balance</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Reports </span></li> --}}

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Reports</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/reports/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Reports List</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">MIS Report</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/mis_reports/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> MIS Report List</span></a></li>

                        <li class="sidebar-item"><a href="/mis_reports/add_mis_reports" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu">Add MIS Report</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Invoice Design</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/invoice/design" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Invoice Design</span></a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
@elseif(Auth::guard('company')->check())
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile d-flex no-block dropdown mt-3">
                        <div class="user-pic"><img src="{{URL::asset('UI/assets/images/users/1.jpg')}}" alt="users" class="rounded-circle" width="40" /></div>
                        <div class="user-content hide-menu ml-2">
                            <a href="javascript:void(0)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(Auth::guard('super_admin')->check())
                                <h5 class="mb-0 user-name font-medium">{{Session::get('AdminName')}} </h5>
                                <span class="op-5 user-email">{{Session::get('AdminEmail')}}</span>
                                @elseif(Auth::guard('company')->check())
                                <h5 class="mb-0 user-name font-medium">{{Session::get('Companyname')}} </h5>
                                <span class="op-5 user-email">{{Session::get('UserEmail')}}</span>
                                @endif

                            </a>
                        </div>
                    </div>
                    <!-- End User Profile-->
                </li>

                <li class="sidebar-item"><a href="/admin/dashboard" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu main_menu-clr"> Dashboard </span></a></li>

                <li class="sidebar-item"><a href="/admin/dashboard" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu main_menu-clr"> Reports </span></a></li>

                <li class="sidebar-item"><a href="/admin/users/change_user_roles" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu main_menu-clr"> Users </span></a></li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Bills</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Bills </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item"><a href="/admin/bills/add_bills" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> New Bills </span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Documents</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Documents </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                         <li class="sidebar-item"><a href="/admin/documents/view_documents" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Documents </span></a></li>

                        <li class="sidebar-item"><a href="/admin/documents/add_upload_document" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> New Documents </span></a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>

@elseif(Auth::guard('manager')->check())
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile d-flex no-block dropdown mt-3">
                        <div class="user-pic"><img src="{{URL::asset('UI/assets/images/users/1.jpg')}}" alt="users" class="rounded-circle" width="40" /></div>
                        <div class="user-content hide-menu ml-2">
                            <a href="javascript:void(0)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(Auth::guard('super_admin')->check())
                                <h5 class="mb-0 user-name font-medium">{{Session::get('AdminName')}} </h5>
                                <span class="op-5 user-email">{{Session::get('AdminEmail')}}</span>
                                @elseif(Auth::guard('company')->check())
                                <h5 class="mb-0 user-name font-medium">{{Session::get('Companyname')}} </h5>
                                <span class="op-5 user-email">{{Session::get('UserEmail')}}</span>
                                @endif

                            </a>
                        </div>
                    </div>
                    <!-- End User Profile-->
                </li>

                <li class="sidebar-item"><a href="/admin/dashboard" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu main_menu-clr"> Dashboard </span></a></li>


                <li class="sidebar-item"><a href="/admin/users/change_user_roles" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu main_menu-clr"> Users </span></a></li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu clr-white">Master Data</span></li> --}}

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Master Data</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <!-- <li class="sidebar-item"><a href="new_user.html" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Add New User </span></a></li> -->

                        <li class="sidebar-item"><a href="/admin/cost_center/list" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> Cost Center List </span></a></li>

                        <li class="sidebar-item"><a href="/admin/sub_group/list" class="sidebar-link"><i class="mdi mdi-format-align-left"></i><span class="hide-menu">Sub Group List</span></a></li>

                        <li class="sidebar-item"><a href="/admin/ledger/list" class="sidebar-link"><i class="mdi mdi-email"></i><span class="hide-menu"> Ledger List</span></a></li>
                    </ul>
                </li>
                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-content-copy"></i><span class="hide-menu main_menu-clr">Sub Groups</span></a>
                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item"><a href="/admin/sub_group/list" class="sidebar-link"><i class="mdi mdi-format-align-left"></i><span class="hide-menu">Sub Group List</span></a></li>
                    </ul>
                </li> --}}

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Ledgers</span></li> --}}
                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-inbox-arrow-down"></i><span class="hide-menu main_menu-clr">Ledgers </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item"><a href="/admin/ledger/list" class="sidebar-link"><i class="mdi mdi-email"></i><span class="hide-menu"> Ledger List</span></a></li>
                    </ul>
                </li> --}}

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu clr-white">Uploads</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Uploads </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                         <li class="sidebar-item"><a href="/admin/bills/view_bills" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Bills </span></a></li>

                         <li class="sidebar-item"><a href="/admin/documents/view_documents" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Documents </span></a></li>
                        {{-- <li class="sidebar-item"><a href="/admin/bills/add_bills" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> New Bills </span></a></li> --}}
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Documents</span></li> --}}
                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Documents </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                         <li class="sidebar-item"><a href="/admin/documents/view_documents" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Documents </span></a></li>

                        <li class="sidebar-item"><a href="/admin/documents/add_upload_document" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> New Documents </span></a></li>
                    </ul>
                </li> --}}


                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu clr-white">Bank Entries</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu main_menu-clr">Customers</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/customers/list" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> Customers List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/customer_invoice/list" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Customer Invoice Entry</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Vendors</span></li> --}}

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Vendors</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/vendors/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Vendors List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/vendor_invoice/list" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Vendors Invoice Entry</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Employee</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Employee</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/employees/list" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Employee Lists</span></a></li>

                        <li class="sidebar-item"><a href="/admin/emp_salary/list" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Employee Salary Lists</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Bank Accounts</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Bank Entries</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        {{-- <li class="sidebar-item"><a href="/admin/banks/add_bank" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Add Bank Account</span></a></li> --}}
                        <li class="sidebar-item"><a href="/admin/banks/list" class="sidebar-link"><span class="hide-menu"> Bank Accounts List</span></a></li>

                        <li class="sidebar-item"><a href="/admin/online_payment_entry/list" class="sidebar-link"><span class="hide-menu"> Bank Online Payment Entry List</span></a></li>

                        <li class="sidebar-item"><a href="/admin/cheque_payment_entry/list" class="sidebar-link"><span class="hide-menu"> Cheque Payment Entry List</span></a></li>

                        <li class="sidebar-item"><a href="/admin/online_payment_received/list" class="sidebar-link"><span class="hide-menu"> Online Payment Received Entry List</span></a></li>

                        <li class="sidebar-item"><a href="/admin/cheque_payment_received/list" class="sidebar-link"><span class="hide-menu"> Cheque Received Entry List</span></a></li>

                        <li class="sidebar-item"><a href="/admin/cheque_payment__deposite_received/list" class="sidebar-link"><span class="hide-menu"> Cheque Deposited Entry List</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Bank Entries</span></li> --}}
                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Bank Online Payment Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/online_payment_entry/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Bank Online Payment Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/online_payment_entry/add_payment_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Bank Online Payment Entry</span></a></li>
                    </ul>
                </li> --}}

                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cheque Payment Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cheque_payment_entry/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cheque Payment Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cheque_payment_entry/add_cheque_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cheque Payment Entry</span></a></li>
                    </ul>
                </li> --}}

                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Online Payment Received Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/online_payment_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Online Payment Received Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/online_payment_received/add_payment_received" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Online Payment Received Entry</span></a></li>
                    </ul>
                </li> --}}

                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cheque Received Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cheque_payment_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cheque Received Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cheque_payment_received/add_cheque_received" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cheque Received Entry</span></a></li>
                    </ul>
                </li> --}}
{{--
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cheque Deposited Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cheque_payment__deposite_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cheque Deposited Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cheque_payment__deposite_received/add_cheque_deposite" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cheque Deposited Entry</span></a></li>
                    </ul>
                </li> --}}

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu clr-white">Cash Entries</span></li> --}}

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cash Entries</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cash_payment/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Payment Entry List</span></a></li>

                        <li class="sidebar-item"><a href="/admin/cash_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Received Entry List</span></a></li>

                        <li class="sidebar-item"><a href="/admin/cash_deposited/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Deposited in Bank List</span></a></li>
                        {{-- <li class="sidebar-item"><a href="/admin/cash_payment/add_cash_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cash Payment Entry</span></a></li> --}}
                    </ul>
                </li>

                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cash Received Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cash_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Received Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cash_received/add_cash_received_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cash Received Entry</span></a></li>
                    </ul>
                </li> --}}

                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cash Deposited in Bank</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cash_deposited/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Deposited in Bank List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cash_deposited/add_cash_deposited_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cash Deposited in Bank</span></a></li>
                    </ul>
                </li> --}}

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Journal Entries </span></li> --}}

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Ledger Adjustment Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/ledger_adjustment/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Ledger Adjustment Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/ledger_adjustment/add_adjustment_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Ledger Adjustment Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Opening Balance</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/opening_balance/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Opening Balance List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/opening_balance/add_opening_balance" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Opening Balance</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Reports </span></li> --}}

                {{-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Reports</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/reports/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Reports List</span></a></li>
                    </ul>
                </li> --}}
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
@elseif(Auth::guard('accountant')->check())
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile d-flex no-block dropdown mt-3">
                        <div class="user-pic"><img src="{{URL::asset('UI/assets/images/users/1.jpg')}}" alt="users" class="rounded-circle" width="40" /></div>
                        <div class="user-content hide-menu ml-2">
                            <a href="javascript:void(0)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(Auth::guard('super_admin')->check())
                                <h5 class="mb-0 user-name font-medium">{{Session::get('AdminName')}} </h5>
                                <span class="op-5 user-email">{{Session::get('AdminEmail')}}</span>
                                @elseif(Auth::guard('company')->check())
                                <h5 class="mb-0 user-name font-medium">{{Session::get('Companyname')}} </h5>
                                <span class="op-5 user-email">{{Session::get('UserEmail')}}</span>
                                @endif

                            </a>
                        </div>
                    </div>
                    <!-- End User Profile-->
                </li>

                <li class="sidebar-item"><a href="/admin/dashboard" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu main_menu-clr"> Dashboard </span></a></li>

                <li class="sidebar-item"><a href="/admin/dashboard" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu main_menu-clr"> Reports </span></a></li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Bills</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Bills </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                         <li class="sidebar-item"><a href="/admin/bills/view_bills" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> View Bills </span></a></li>

                        <li class="sidebar-item"><a href="/admin/bills/add_bills" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> New Bills </span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Documents</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Documents </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">

                        <li class="sidebar-item"><a href="/admin/documents/add_upload_document" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> New Documents </span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Customers</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-widgets"></i><span class="hide-menu main_menu-clr">Customers</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/customers/list" class="sidebar-link"><i class="mdi mdi-toggle-switch"></i><span class="hide-menu"> Customers List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/customer_invoice/list" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Customer Invoice Entry</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Vendors</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Vendors</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/vendors/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Vendors List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/vendor_invoice/list" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Vendors Invoice Entry</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Employee</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Employee</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/employees/list" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Employee Lists</span></a></li>

                        <li class="sidebar-item"><a href="/admin/emp_salary/list" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Employee Salary Lists</span></a></li>
                        {{-- <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Salary Entry</span></a></li> --}}
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Bank Accounts</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Bank Accounts</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/banks/add_bank" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Add Bank Account</span></a></li>
                        <li class="sidebar-item"><a href="/admin/banks/list" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Bank Accounts List</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Bank Entries</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Bank Online Payment Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/online_payment_entry/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Bank Online Payment Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/online_payment_entry/add_payment_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Bank Online Payment Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cheque Payment Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cheque_payment_entry/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cheque Payment Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cheque_payment_entry/add_cheque_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cheque Payment Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Online Payment Received Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/online_payment_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Online Payment Received Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/online_payment_received/add_payment_received" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Online Payment Received Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cheque Received Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cheque_payment_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cheque Received Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cheque_payment_received/add_cheque_received" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cheque Received Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cheque Deposited Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cheque_payment__deposite_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cheque Deposited Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cheque_payment__deposite_received/add_cheque_deposite" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cheque Deposited Entry</span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Cash Accounts</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cash Payment Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cash_payment/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Payment Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cash_payment/add_cash_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cash Payment Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cash Received Entry</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cash_received/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Received Entry List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cash_received/add_cash_received_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cash Received Entry</span></a></li>
                    </ul>
                </li>

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Cash Deposited in Bank</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/cash_deposited/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Cash Deposited in Bank List</span></a></li>
                        <li class="sidebar-item"><a href="/admin/cash_deposited/add_cash_deposited_entry" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Add Cash Deposited in Bank</span></a></li>
                    </ul>
                </li>


                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Reports </span></li> --}}

                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Reports</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/reports/list" class="sidebar-link"><i class="mdi mdi-rounded-corner"></i><span class="hide-menu"> Reports List</span></a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
@elseif(Auth::guard('hr')->check())
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile d-flex no-block dropdown mt-3">
                        <div class="user-pic"><img src="{{URL::asset('UI/assets/images/users/1.jpg')}}" alt="users" class="rounded-circle" width="40" /></div>
                        <div class="user-content hide-menu ml-2">
                            <a href="javascript:void(0)" class="" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(Auth::guard('super_admin')->check())
                                <h5 class="mb-0 user-name font-medium">{{Session::get('AdminName')}} </h5>
                                <span class="op-5 user-email">{{Session::get('AdminEmail')}}</span>
                                @elseif(Auth::guard('company')->check())
                                <h5 class="mb-0 user-name font-medium">{{Session::get('Companyname')}} </h5>
                                <span class="op-5 user-email">{{Session::get('UserEmail')}}</span>
                                @endif

                            </a>
                        </div>
                    </div>
                    <!-- End User Profile-->
                </li>

                <li class="sidebar-item"><a href="/admin/dashboard" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu main_menu-clr"> Dashboard </span></a></li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Bills</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Bills </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item"><a href="/admin/bills/add_bills" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> New Bills </span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Documents</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu main_menu-clr">Documents </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item"><a href="/admin/documents/add_upload_document" class="sidebar-link"><i class="mdi mdi-adjust"></i><span class="hide-menu"> New Documents </span></a></li>
                    </ul>
                </li>

                {{-- <li class="nav-small-cap"><i class="mdi mdi-dots-horizontal"></i> <span class="hide-menu">Employee</span></li> --}}
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-collage"></i><span class="hide-menu main_menu-clr">Employee</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item"><a href="/admin/employees/list" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Employee Lists</span></a></li>

                        <li class="sidebar-item"><a href="/admin/emp_salary/list" class="sidebar-link"><i class="mdi mdi-cart"></i> <span class="hide-menu">Employee Salary Lists</span></a></li>
                        {{-- <li class="sidebar-item"><a href="" class="sidebar-link"><i class="mdi mdi-tablet"></i><span class="hide-menu"> Salary Entry</span></a></li> --}}
                    </ul>
                </li>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
@endif
