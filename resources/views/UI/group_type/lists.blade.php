@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Group Type List</h4>
            </div>

            <div class="col-7 align-self-center">
                 <a href="javascript:void(0);" data-toggle="modal" data-target="#AddGroupType" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">New Group Type</a>
            </div>


        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @if(session('message'))
                            <div class="alert alert-success width100">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Name</th>
                                        {{-- <th scope="col">Status</th> --}}
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($GroupType))
                                    @foreach($GroupType as $Group)
                                    <tr>
                                    <td></td>
                                        <td>{{$Group->name}}</td>
                                        {{-- <td>
                                            <label class="switch">
                                                <input type="checkbox" data-id="{{$Group->id}}" class="group_type_status"
                                                @if($Group->status == 0)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider circle-btn"></span>
                                              </label>
                                        </td> --}}
                                        <td>
                                            <a href="javascript:void(0);" data-id="{{$Group->id}}" data-name="{{$Group->name}}" class="EditGroupType"><i class="mr-2 mdi mdi-grease-pencil"></i></a>  <a href="javascript:void(0);" data-id="{{$Group->id}}" id="delete_group_type"><i class="mr-2 mdi mdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

<div id="AddGroupType" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4> --}}
        </div>
        <div class="modal-body">
            <form action="/admin/group_type/add_group_type" method="post" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Group Type Name</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" value="">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <div id="EditGroupTypeModel" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4> --}}
        </div>
        <div class="modal-body">
            <form action="/admin/group_type/update_group_type" method="post" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Group Type Name</h4>
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" id="id" value="">
                                <input type="text" class="form-control" name="name" id="name" value="">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
@endsection
