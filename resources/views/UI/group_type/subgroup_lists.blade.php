@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
            <h4 class="page-title">{{$title}}</h4>
            </div>

            <div class="col-7 align-self-center">
                 <a href="javascript:void(0);" data-toggle="modal" data-target="#AddGroupType" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">New Sub Group</a>
            </div>


        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @if(session('message'))
                            <div class="alert alert-success width100">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        {{-- <th scope="col">Primary Group Name</th> --}}
                                        <th scope="col">Sub Group Name</th>
                                        {{-- <th scope="col">Status</th> --}}
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($SubGroup))
                                    @foreach($SubGroup as $Group)
                                    <tr>
                                    <td></td>
                                        {{-- <td>{{$Group->group_name}}</td> --}}
                                        <td>{{$Group->group_name}}</td>
                                        {{-- <td>
                                            <label class="switch">
                                                <input type="checkbox" data-id="{{$Group->id}}" class="sub_group_status"
                                                @if($Group->status == 0)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider circle-btn"></span>
                                              </label>
                                        </td> --}}
                                        <td>
                                            <a href="javascript:void(0);" data-id="{{$Group->id}}" data-name="{{$Group->subgroup_name}}" class="EditGroupType"><i class="mr-2 mdi mdi-grease-pencil"></i></a>  <a href="javascript:void(0);" data-id="{{$Group->id}}" id="delete_sub_group"><i class="mr-2 mdi mdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

<div id="AddGroupType" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4> --}}
        </div>
        <div class="modal-body">
            <form action="/admin/sub_group/add_sub_group" method="post" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Choose Primary Group</h4>
                        <select class="custom-select col-12 search_dropdown" name="primary_group" id="example-month-input2">
                            @foreach($PrimaryGroup as $Group)
                                <option value="{{$Group->id}}">{{$Group->group_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Sub Group Name</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="sub_group" value="">
                            </div>
                    </div>
                </div>

                {{-- <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Group Category</h4>
                        <select class="custom-select col-12" id="example-month-input2">
                            <option selected="">Choose...</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div> --}}

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <div id="EditGroupTypeModel" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4> --}}
        </div>
        <div class="modal-body">
            <form action="/admin/sub_group/update_sub_group" method="post" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Choose Primary Group</h4>
                        <input type="hidden" class="form-control" name="id" id="id" value="">
                        <select class="custom-select col-12 search_dropdown" name="primary_group" id="example-month-input2">
                            @foreach($PrimaryGroup as $Group)
                                <option value="{{$Group->id}}">{{$Group->group_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Primary Group Name</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" id="name" name="sub_group" value="">
                            </div>
                    </div>
                </div>

                {{-- <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Group Category</h4>
                        <select class="custom-select col-12" id="example-month-input2">
                            <option selected="">Choose...</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div> --}}

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
@endsection
