@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">New Document</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/admin/documents/store_documents" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Document Type</h4>
                        <select class="custom-select col-12" id="example-month-input2" name="document_type">
                            <option selected disabled>Choose Document Type</option>
                            <option value="Bank Statement">Bank Statement</option>
                            <option value="Card Statement">Card Statement</option>
                            <option value="HR Document">HR Document</option>
                            <option value="Board Resolution">Board Resolution</option>
                            <option value="ROC Document">ROC Document</option>
                            <option value="Financials">Financials </option>
                            <option value="Others">Others</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Document</h4>
                            <div class="form-group">
                                <input type="file" name="document" class="form-control">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Upload Date</h4>
                            <div class="form-group">
                                <input type="date" name="upload_date" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Document Name</h4>
                            <div class="form-group">
                                <input type="text" name="document_name" class="form-control">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Short Description </h4>
                            <div class="form-group">
                                <textarea class="form-control" name="short_description" id="" cols="30" rows="10"></textarea>
                            </div>
                    </div>
                </div>


                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
