@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">New Customer</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/admin/customers/store_customer" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Customer Name</h4>
                        <div class="form-group">
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Group</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="group_type_id">
                            <option selected disabled class="dropdown-head-font-size">Choose Primary Group</option>
                            @foreach($PrimaryGroup as $Primary)
                            <option value="{{$Primary->id}}">{{$Primary->group_name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Sub Group</option>
                            @foreach($SubGroup as $Sub)
                                <option value="{{$Sub->id}}">{{$Sub->group_name}}</option>
                            @endforeach

                            {{-- <option disabled class="dropdown-head-font-size">Choose Sub Group</option>
                            @foreach($SubGroup as $Sub)
                                <option value="{{$Sub->subgroup_name}}">{{$Sub->subgroup_name}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Customer Type</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="customer_type">
                            <option selected disabled>Choose Customer Type</option>
                            <option value="Consultant">Consultant</option>
                            <option value="Proprietorship">Proprietorship</option>
                            <option value="Partnership Firm">Partnership Firm</option>
                            <option value="LLP">LLP</option>
                            <option value="Company ">Company </option>
                            <option value="Trust">Trust</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Customer PAN</h4>
                            <div class="form-group">
                                <input type="text" name="pan_no" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Customer GST</h4>
                            <div class="form-group">
                                <input type="text" name="gst_no" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Business Category</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="bussiness_category">
                            <option selected disabled>Choose Bussiness Category</option>
                            <option value="Providing Services">Providing Services</option>
                            <option value="Contract Staffing">Contract Staffing</option>
                            <option value="Supplying Materilas">Supplying Materilas</option>
                            <option value="Work Contract">Work Contract</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Country</h4>
                            <div class="form-group">
                                <input type="text" name="country" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">State</h4>
                            <div class="form-group">
                                <input type="text" name="state" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">City</h4>
                            <div class="form-group">
                                <input type="text" name="city" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Pincode</h4>
                            <div class="form-group">
                                <input type="text" name="pin_code" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Address Line 1</h4>
                            <div class="form-group">
                                <input type="text" name="address_1" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Address Line 2</h4>
                            <div class="form-group">
                                <input type="text" name="address_2" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Contact Person Name</h4>
                            <div class="form-group">
                                <input type="text" name="contac_person_name" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Contact Person Designation</h4>
                            <div class="form-group">
                                <input type="text" name="contact_person_designation" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Phone Number</h4>
                            <div class="form-group">
                                <input type="text" name="phone" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Email ID</h4>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control">
                            </div>
                    </div>
                </div>

                <table class="table">
                    <thead>
                      <tr>
                        <th>Choose Image</th>
                        <th>Preview</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                            <h4 class="card-title">PAN Copy</h4>
                                <div class="form-group">
                                    <input type="file" name="pan_copy" id="CheangeImg" class="form-control">
                                </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                                <br>
                                <img id="img_preview" src="#" alt="your image" style="
                                width: 250px;
                            ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">GST Copy</h4>
                            <div class="form-group">
                                <input type="file" name="gst_copy" id="Cheangegst" class="form-control">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                            <br>
                            <img id="gst_preview" src="#" alt="your image" style="
                            width: 250px;
                        ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">Company Incorporation </h4>
                            <div class="form-group">
                                <input type="file" name="company_incorporation" class="form-control" id="Cheangecmpny">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                                <br>
                                <img id="cmpny_preview" src="#" alt="your image" style="
                                width: 250px;
                            ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">Agreement / Contract copy</h4>
                            <div class="form-group">
                                <input type="file" name="aggreement_contract_copy" class="form-control" id="Cheangeagree">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                            <br>
                            <img id="agree_preview" src="#" alt="your image" style="
                            width: 250px;
                            ">
                        </td>
                      </tr>
                    </tbody>
                  </table>


                    {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="card-body">
                            <h4 class="card-title">PAN Copy</h4>
                                <div class="form-group">
                                    <input type="file" name="pan_copy" id="CheangeImg" class="form-control">
                                </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <label for="">Preview</label>
                        <br>
                        <img id="img_preview" src="#" alt="your image" style="
                        width: 250px;
                    ">
                    </div> --}}

                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">GST Copy</h4>
                            <div class="form-group">
                                <input type="file" name="gst_copy" id="Cheangegst" class="form-control">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <label for="">Preview</label>
                    <br>
                    <img id="gst_preview" src="#" alt="your image" style="
                    width: 250px;
                ">
                </div> --}}

                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Company Incorporation </h4>
                            <div class="form-group">
                                <input type="file" name="company_incorporation" class="form-control" id="Cheangecmpny">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <label for="">Preview</label>
                    <br>
                    <img id="cmpny_preview" src="#" alt="your image" style="
                    width: 250px;
                ">
                </div> --}}


                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Agreement / Contract copy</h4>
                            <div class="form-group">
                                <input type="file" name="aggreement_contract_copy" class="form-control" id="Cheangeagree">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <label for="">Preview</label>
                    <br>
                    <img id="agree_preview" src="#" alt="your image" style="
                    width: 250px;
                ">
                </div> --}}


                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
