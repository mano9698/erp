
@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>

        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/admin/customer_invoice/update_customer_invoice" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Customer Name</h4>
                        <input type="hidden" class="form-control" name="id" value="{{$CustomerInvoice->id}}">

                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="customer_id">
                            @foreach($Customers as $Customer)
                                <option value="{{$Customer->id}}" @if($CustomerInvoice->customer_id = $Customer->id) selected @endif)>{{$Customer->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Invoice Number</h4>
                            <div class="form-group">
                            <input type="text" class="form-control" name="invoice_number" value="{{$CustomerInvoice->invoice_number}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Invoice Date</h4>
                            <div class="form-group">
                                <input type="date" class="form-control" name="invoice_date" value="{{$CustomerInvoice->invoice_date}}">
                            </div>
                    </div>
                </div>

                <?php
                        $customer_ledger_amounts = DB::table('customer_ledger_amounts')
                                                ->where('customer_ledger_amounts.customer_invoice_id', '=', $CustomerInvoice->id)
                                                ->get();

                                                // echo json_encode($customer_ledger_amounts);
                                                // exit;

                ?>

                @foreach($customer_ledger_amounts as $invoice)

                <div class="fieldGroup row form-width">
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="card-body">
                            <label for="">Select Ledger Name</label>
                            <select class="custom-select col-12" id="example-month-input2" name="">
                                @foreach($Ledger as $Ledgers)
                                    <option value="{{$Ledgers->id}}" @if($invoice->ledger_id == $Ledgers->id) selected @endif)>{{$Ledgers->ledger_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="card-body">
                            <label for="">Select Cost Center Name</label>
                            <select class="custom-select col-12" id="example-month-input2" name="">
                                <option value="0">Not Applicable</option>
                                @foreach($CostCenter as $Cost)
                                    <option value="{{$Cost->id}}" @if($invoice->cost_id == $Cost->id) selected @endif>{{$Cost->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="card-body">
                            <label for="">Enter Amount</label>
                        <input type="number" placeholder="Enter Amount" class="form-control Total" id="cost" name="" value="{{$invoice->amount}}">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <a href="javascript:void(0);" data-id="{{$invoice->id}}" class="delete_customer_ledger_amount"><i class="mr-2 mdi mdi-delete"></i></a>
                    </div>
                </div>
                @endforeach

                <div class="col-md-10" style="margin-top:26px;text-align: right;">
                    <div class="card-body">
                        <a href="javascript:void(0)" class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add More</a>
                    </div>
                </div>


                <div class="fieldGroupCopy " style="display: none;">
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="card-body">
                            <label for="">Select Ledger Name</label>
                            <select class="custom-select col-12" id="example-month-input2" name="ledger[]">
                                @foreach($Ledger as $Ledgers)
                                    <option value="{{$Ledgers->id}}">{{$Ledgers->ledger_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="card-body">
                            <label for="">Select Cost Center Name</label>
                            <select class="custom-select col-12" id="example-month-input2" name="cost[]">
                                <option value="0">Not Applicable</option>
                                @foreach($CostCenter as $Cost)
                                    <option value="{{$Cost->id}}">{{$Cost->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-3">
                        <div class="card-body">
                            <label for="">Enter Amount</label>
                            <input type="number" placeholder="Enter Amount" class="form-control Total" onkeyup="Total()" name="amount[]">
                        </div>
                    </div>
                    <div class="col-md-2" style="margin-top:26px;">
                        <div class="card-body">
                            <a href="javascript:void(0)" class="btn btn-danger remove"><span class="glyphicon glyphicon glyphicon-remove" aria-hidden="true"></span> Remove</a>
                        </div>
                    </div>
                </div>

                {{-- <br>
                <br>
                <table class="table css-serial" id="dynamicTable" style="
                margin-top: 2%;
            ">
                    <thead>
                      <tr>
                        <th>Selecte Ledger</th>
                        <th>Amount</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="DynamicTableData">


                    </tbody>
                  </table>--}}
                  <br>
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-1">
                        <h4>Grand Total: </h4>
                    </div>
                    <div class="col-md-4">
                        <input type="text" name="total_amount" id="TotalGrandTotal" placeholder="" class="form-control TotalAmount" value="{{$CustomerInvoice->total_amount}}">
                      </div>
                    <div class="col-md-3">

                    </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>

                        {{-- <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">View</button> --}}

                        {{-- <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Confirm & Submit</button> --}}
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection

@section('JSScript')
<script type="text/javascript">
   $(document).ready(function(){
    //group add limit
    var maxGroup = 10;

    //add more fields group
    $(".addMore").click(function(){
        if($('body').find('.fieldGroup').length < maxGroup){
            var fieldHTML = '<div class="fieldGroup row form-width">'+$(".fieldGroupCopy").html()+'</div>';
            $('body').find('.fieldGroup:last').after(fieldHTML);
        }else{
            alert('Maximum '+maxGroup+' groups are allowed.');
        }
    });

    //remove fields group
    $("body").on("click",".remove",function(){
        $(this).parents(".fieldGroup").remove();
    });
});



$(document).ready(function(){
    calculateSum();

//iterate through each textboxes and add keyup
//handler to trigger sum event
$(".Total").each(function() {

    $(this).keyup(function(){
        calculateSum();

    });
});

});


function Total(){

    $(this).keyup(function(){
        calculateSum();

    });
}


function calculateSum() {

var sum = 0;
//iterate through each textboxes and add the values
$(".Total").each(function() {

    //add only if the value is number
    if(!isNaN(this.value) && this.value.length!=0) {
        sum += parseFloat(this.value);
    }

});
//.toFixed() method will roundoff the final sum to 2 decimal places
$(".TotalAmount").val(sum.toFixed(2));
}
</script>


@endsection


