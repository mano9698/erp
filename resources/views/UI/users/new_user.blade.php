@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">New Customer</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/add_users" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Name</h4>
                        <div class="form-group">
                            <input type="text" class="form-control" name="name">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Email</h4>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Mobile</h4>
                            <div class="form-group">
                                <input type="text" name="mobile" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Country</h4>
                            <div class="form-group">
                                <input type="text" name="country" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">State</h4>
                            <div class="form-group">
                                <input type="text" name="state" class="form-control">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">City</h4>
                            <div class="form-group">
                                <input type="text" name="city" class="form-control">
                            </div>
                    </div>
                </div>


                    {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="card-body">
                            <h4 class="card-title">PAN Copy</h4>
                                <div class="form-group">
                                    <input type="file" name="pan_copy" id="CheangeImg" class="form-control">
                                </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <label for="">Preview</label>
                        <br>
                        <img id="img_preview" src="#" alt="your image" style="
                        width: 250px;
                    ">
                    </div> --}}

                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">GST Copy</h4>
                            <div class="form-group">
                                <input type="file" name="gst_copy" id="Cheangegst" class="form-control">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <label for="">Preview</label>
                    <br>
                    <img id="gst_preview" src="#" alt="your image" style="
                    width: 250px;
                ">
                </div> --}}

                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Company Incorporation </h4>
                            <div class="form-group">
                                <input type="file" name="company_incorporation" class="form-control" id="Cheangecmpny">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <label for="">Preview</label>
                    <br>
                    <img id="cmpny_preview" src="#" alt="your image" style="
                    width: 250px;
                ">
                </div> --}}


                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Agreement / Contract copy</h4>
                            <div class="form-group">
                                <input type="file" name="aggreement_contract_copy" class="form-control" id="Cheangeagree">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <label for="">Preview</label>
                    <br>
                    <img id="agree_preview" src="#" alt="your image" style="
                    width: 250px;
                ">
                </div> --}}


                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
