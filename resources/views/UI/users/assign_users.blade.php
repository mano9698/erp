@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{ $title }}</h4>
            </div>

            <div class="col-7 align-self-center">
                <a href="javascript:void(0);" data-toggle="modal" data-target="#AddUsers" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">Assign User</a>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @if(session('message'))
                            <div class="alert alert-success width100">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Mobile</th>
                                        <th scope="col">Country</th>
                                        <th scope="col">State</th>
                                        <th scope="col">City</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($Users))
                                    @foreach($Users as $User)
                                    <tr>
                                    <th scope="row">{{$User->AssignId}}</th>
                                        <td>{{$User->name}}</td>
                                        <td>{{$User->email}}</td>
                                        <td>{{$User->mobile}}</td>
                                        <td>{{$User->country}}</td>
                                        <td>{{$User->state}}</td>
                                        <td>{{$User->city}}</td>
                                        <td>
                                            <a href="javascript:void(0);" data-id="{{$User->AssignId}}" data-name="{{$User->name}}" class="EditUsers"><i class="mr-2 mdi mdi-grease-pencil"></i></a>  <a href="/admin/users/delete_assign_users/{{$User->AssignId}}"  onclick="return confirm(' Are you sure. You want to delete?');"><i class="mr-2 mdi mdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

<div id="AddUsers" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4> --}}
        </div>
        <div class="modal-body">
            <form action="/admin/users/add_assign_users" method="post" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Choose Users</h4>
                        <input type="hidden" name="company_id" value="{{ Request::segment(4) }}">

                        <select class="custom-select col-12" name="user_id" id="example-month-input2">
                            @foreach($UsersList as $Users)
                                <option value="{{$Users->id}}">{{$Users->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <div id="EditUsers" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          {{-- <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4> --}}
        </div>
        <div class="modal-body">
            <form action="/admin/users/update_assign_users" method="post" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Group Type Name</h4>
                            <div class="form-group">
                            </div>
                            <div class="col-sm-12 col-md-6 col-lg-12">
                                <div class="card-body">
                                    <h4 class="card-title">Choose Group Type</h4>
                                    <input type="hidden" name="company_id" value="{{ Request::segment(4) }}">

                                    <input type="hidden" name="id" id="id">

                                    <select class="custom-select col-12" name="user_id" id="example-month-input2">
                                        @foreach($UsersList as $Users)
                                            <option value="{{$Users->id}}">{{$Users->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

@endsection
