@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{ $title }}</h4>
            </div>

            <div class="col-7 align-self-center">
                <a href="/admin/users/add_user" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">New User</a>
            </div>


        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Mobile</th>
                                        <th scope="col">Country</th>
                                        <th scope="col">State</th>
                                        <th scope="col">City</th>
                                        <th scope="col">Roles</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($Users))
                                    @foreach($Users as $User)
                                    <tr>
                                    <td></td>
                                        <td>{{$User->name}}</td>
                                        <td>{{$User->email}}</td>
                                        <td>{{$User->mobile}}</td>
                                        <td>{{$User->country}}</td>
                                        <td>{{$User->state}}</td>
                                        <td>{{$User->city}}</td>
                                        <td>
                                            @if($User->user_type == 3)
                                                New User
                                            @elseif($User->user_type == 4)
                                                Manager
                                            @elseif($User->user_type == 5)
                                            Accountant
                                            @elseif($User->user_type == 6)
                                            HR Manager
                                            @endif
                                        </td>
                                        <td>
                                            <label class="switch">
                                                <input type="checkbox" data-id="{{$User->id}}" id="user_status"
                                                @if($User->status == 1)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider circle-btn"></span>
                                              </label>
                                        </td>
                                        <td>
                                            <a href="/admin/users/edit_user/{{$User->id}}" target="_blank"><i class="mr-2 mdi mdi-grease-pencil"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
