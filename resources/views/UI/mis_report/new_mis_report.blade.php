@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/mis_reports/store_msi_report" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Company</h4>
                        <select class="custom-select col-12" id="example-month-input2" name="company_id">
                            <option selected disabled>Choose Company</option>
                            @foreach($Users as $User)
                                <option value="{{$User->id}}">{{$User->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Title</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="title">
                            </div>
                    </div>
                </div>
                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Year</h4>
                        <select class="custom-select col-12" id="example-month-input2" name="month">
                            <option selected selected disabled>Choose Year</option>
                            <option value="2018-19">2018-19</option>
                            <option value="2019-20">2019-20</option>
                            <option value="2020-21">2020-21</option>
                            <option value="2021-22">2021-22</option>
                            <option value="2022-23">2022-23</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Month</h4>
                        <select class="custom-select col-12" id="example-month-input2" name="year">
                            <option selected selected disabled>Choose Month</option>
                            <option value='Janaury'>Janaury</option>
                            <option value='February'>February</option>
                            <option value='March'>March</option>
                            <option value='April'>April</option>
                            <option value='May'>May</option>
                            <option value='June'>June</option>
                            <option value='July'>July</option>
                            <option value='August'>August</option>
                            <option value='September'>September</option>
                            <option value='October'>October</option>
                            <option value='November'>November</option>
                            <option value='December'>December</option>
                        </select>
                    </div>
                </div> --}}
                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Description </h4>
                            <div class="form-group">
                                <textarea name="description" class="form-control ckeditor" id="" cols="30" rows="10"></textarea>
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
