@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/mis_reports/update_msi_report" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Company</h4>
                        <select class="custom-select col-12" id="example-month-input2" name="company_id">
                            <option selected disabled>Choose Company</option>
                            @foreach($Users as $User)
                                <option value="{{$User->id}}" @if($User->id == $MISReport->company_id) selected @endif>{{$User->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Title</h4>
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" value="{{ $MISReport->id }}">
                                <input type="text" class="form-control" name="title" value="{{ $MISReport->title }}">
                            </div>
                    </div>
                </div>
                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Year</h4>
                        <select class="custom-select col-12" id="example-month-input2" name="year">
                            <option selected selected disabled>Choose Year</option>
                            <option value="2018-19" @if($MISReport->year == "2018-19") selected @endif>2018-19</option>
                            <option value="2019-20" @if($MISReport->year == "2019-20") selected @endif>2019-20</option>
                            <option value="2020-21" @if($MISReport->year == "2020-21") selected @endif>2020-21</option>
                            <option value="2021-22" @if($MISReport->year == "2021-22") selected @endif>2021-22</option>
                            <option value="2022-23" @if($MISReport->year == "2022-23") selected @endif>2022-23</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Month</h4>
                        <select class="custom-select col-12" id="example-month-input2" name="month">
                            <option selected selected disabled>Choose Month</option>
                            <option value='Janaury' @if($MISReport->month == "Janaury") selected @endif>Janaury</option>
                            <option value='February' @if($MISReport->month == "February") selected @endif>February</option>
                            <option value='March' @if($MISReport->month == "March") selected @endif>March</option>
                            <option value='April' @if($MISReport->month == "April") selected @endif>April</option>
                            <option value='May' @if($MISReport->month == "May") selected @endif>May</option>
                            <option value='June' @if($MISReport->month == "June") selected @endif>June</option>
                            <option value='July' @if($MISReport->month == "July") selected @endif>July</option>
                            <option value='August' @if($MISReport->month == "August") selected @endif>August</option>
                            <option value='September' @if($MISReport->month == "September") selected @endif>September</option>
                            <option value='October' @if($MISReport->month == "October") selected @endif>October</option>
                            <option value='November' @if($MISReport->month == "November") selected @endif>November</option>
                            <option value='December' @if($MISReport->month == "December") selected @endif>December</option>
                        </select>
                    </div>
                </div> --}}
                <div class="col-sm-12 col-md-6 col-lg-12">
                    <div class="card-body">
                        <h4 class="card-title">Description </h4>
                            <div class="form-group">
                                <textarea name="description" class="form-control ckeditor" id="" cols="30" rows="10">{{ $MISReport->description }}</textarea>
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
