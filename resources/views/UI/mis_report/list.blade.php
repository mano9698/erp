@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>

            {{-- <div class="col-7 align-self-center">
                <a href="/mis_report/list" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">New MSI Report</a>
            </div> --}}
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        @if(session('message'))
                            <div class="alert alert-success width100">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Company Name</th>
                                        <th scope="col">Title</th>
                                        {{-- <th scope="col">Month</th>
                                        <th scope="col">Year</th> --}}
                                        {{-- <th scope="col">Status</th> --}}
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($MISReport as $Report)
                                    <tr>
                                        <td></td>
                                        <td>{{$Report->name}}</td>
                                        <td>{{$Report->title}}</td>
                                        {{-- <td>{{$Report->month}}</td>
                                        <td>{{$Report->year}}</td> --}}
                                        {{-- <td>
                                            <label class="switch">
                                                <input type="checkbox" data-id="{{$Payment->id}}" class="payment_entry_status"
                                                @if($Payment->status == 0)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider circle-btn"></span>
                                              </label>
                                        </td> --}}
                                        <td>
                                            <a href="/mis_reports/edit_mis_reports/{{$Report->id}}"><i class="mr-2 mdi mdi-grease-pencil"></i></a>  <a href="/mis_reports/delete_mis_report/{{$Report->id}}" onclick="return confirm(' Are you sure. You want to delete?');"><i class="mr-2 mdi mdi-delete"></i></a>
                                            <a href="/mis_reports/view_mis_reports/{{$Report->id}}" ><i class="mr-2 mdi mdi-account"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection
