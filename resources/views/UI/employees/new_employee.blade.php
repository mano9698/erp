@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">New Employee</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif
            <form action="/admin/employees/store_employee" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Group</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="group_type_id">
                            <option selected disabled class="dropdown-head-font-size">Choose Primary Group</option>
                            @foreach($PrimaryGroup as $Primary)
                            <option value="{{$Primary->id}}">{{$Primary->group_name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Sub Group</option>
                            @foreach($SubGroup as $Sub)
                                <option value="{{$Sub->id}}">{{$Sub->group_name}}</option>
                            @endforeach

                            {{-- <option disabled class="dropdown-head-font-size">Choose Sub Group</option>
                            @foreach($SubGroup as $Sub)
                                <option value="{{$Sub->subgroup_name}}">{{$Sub->subgroup_name}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">First Name</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="first_name">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Middle Name</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="middle_name">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Last Name</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="last_name">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Date of Birth</h4>
                            <div class="form-group">
                                <input type="date" class="form-control" name="dob">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">PAN Number</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="pan_no">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Aadhar Number</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="aadhar">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Joining Date</h4>
                            <div class="form-group">
                                <input type="date" class="form-control" name="joining_date">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Designation</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="designation">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Department</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="department">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Reporting Manager</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="reporting_manager">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Monthly Gross Salary</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="gross_salary">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Annual CTC</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="annual_ctc">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Country</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="country">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">State</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="state">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">City</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="city">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Pincode</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="pin_code">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Address Line 1</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="address_1">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Address Line 2</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="address_2">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Blood Group</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="blood_group">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Personal Phone Number</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Personal Email ID</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email">
                            </div>
                    </div>
                </div>

                <table class="table">
                    <thead>
                      <tr>
                        <th>Choose Image</th>
                        <th>Preview</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                            <h4 class="card-title">PAN Copy</h4>
                                <div class="form-group">
                                    <input type="file" name="pan_copy" id="CheangeImg" class="form-control">
                                </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                                <br>
                                <img id="img_preview" src="#" alt="your image" style="
                                width: 250px;
                            ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">Aadhar Copy</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="aadhar_copy">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                            <br>
                            <img id="gst_preview" src="#" alt="your image" style="
                            width: 250px;
                        ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">Passport Photo</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="passport_photo">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                                <br>
                                <img id="cmpny_preview" src="#" alt="your image" style="
                                width: 250px;
                            ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">Offer Letter </h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="offer_letter">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                            <br>
                            <img id="agree_preview" src="#" alt="your image" style="
                            width: 250px;
                            ">
                        </td>
                      </tr>
                    </tbody>
                  </table>

                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">PAN Copy</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="pan_copy">
                            </div>
                    </div>
                </div> --}}
                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Aadhar Copy</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="aadhar_copy">
                            </div>
                    </div>
                </div> --}}
                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Passport Photo</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="passport_photo">
                            </div>
                    </div>
                </div> --}}
                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Offer Letter </h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="offer_letter">
                            </div>
                    </div>
                </div> --}}
                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection
