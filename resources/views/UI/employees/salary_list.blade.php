@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>

            <div class="col-7 align-self-center">
                <a href="/admin/emp_salary/add_employee_salary" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">New Salary Entry</a>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Employee Name</th>
                                        <th scope="col">Salary Date</th>
                                        <th scope="col">Salary Due Date</th>
                                        <th scope="col">Total Amount </th>
                                        {{-- <th scope="col">Status</th> --}}
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($EmployeeSalary as $Salary)
                                    <tr>
                                    <td></td>
                                        <td>{{$Salary->first_name}}</td>
                                        <td>{{$Salary->salary_month}}</td>
                                        <td>{{$Salary->due_date_date}}</td>
                                        <td>{{$Salary->total_amount}}</td>
                                        {{-- <td>
                                            <label class="switch">
                                                <input type="checkbox" data-id="{{$Salary->id}}" class="customer_invoice_status"
                                                @if($Salary->status == 0)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider circle-btn"></span>
                                              </label>
                                        </td> --}}
                                        <td>
                                            <a href="/admin/emp_salary/edit_employee_salary/{{$Salary->id}}"><i class="mr-2 mdi mdi-grease-pencil"></i></a>  <a href="javascript:void(0);" data-id="{{$Salary->id}}" class="delete_employee_salary"><i class="mr-2 mdi mdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection
