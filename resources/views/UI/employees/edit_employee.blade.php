@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">New Employee</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif
            <form action="/admin/employees/update_employee" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Group</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="group_type_id">
                            <option selected disabled class="dropdown-head-font-size">Choose Primary Group</option>
                            @foreach($PrimaryGroup as $Primary)
                            <option value="{{$Primary->id}}"  @if($Primary->id == $Employees->group_type_id) selected @endif>{{$Primary->group_name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Sub Group</option>
                            @foreach($SubGroup as $Sub)
                                <option value="{{$Sub->id}}"  @if($Sub->id == $Employees->group_type_id) selected @endif>{{$Sub->group_name}}</option>
                            @endforeach


                            {{-- <option disabled class="dropdown-head-font-size">Choose Sub Group</option>
                            @foreach($SubGroup as $Sub)

                                <option value="{{$Sub->subgroup_name}}"  @if($Sub->subgroup_name == $Vendors->group_type_id) selected @endif>{{$Sub->subgroup_name}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">First Name</h4>
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" value="{{$Employees->id}}">

                            <input type="text" class="form-control" name="first_name" value="{{$Employees->first_name}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Middle Name</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="middle_name" value="{{$Employees->middle_name}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Last Name</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="last_name" value="{{$Employees->last_name}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Date of Birth</h4>
                            <div class="form-group">
                                <input type="date" class="form-control" name="dob" value="{{$Employees->dob}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">PAN Number</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="pan_no" value="{{$Employees->pan_no}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Aadhar Number</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="aadhar" value="{{$Employees->aadhar}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Joining Date</h4>
                            <div class="form-group">
                                <input type="date" class="form-control" name="joining_date" value="{{$Employees->joining_date}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Designation</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="designation" value="{{$Employees->designation}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Department</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="department" value="{{$Employees->department}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Reporting Manager</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="reporting_manager" value="{{$Employees->reporting_manager}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Monthly Gross Salary</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="gross_salary" value="{{$Employees->gross_salary}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Annual CTC</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="annual_ctc" value="{{$Employees->annual_ctc}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Country</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="country" value="{{$Employees->country}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">State</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="state" value="{{$Employees->state}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">City</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="city" value="{{$Employees->city}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Pincode</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="pin_code" value="{{$Employees->pin_code}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Address Line 1</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="address_1" value="{{$Employees->address_1}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Address Line 2</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="address_2" value="{{$Employees->address_2}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Blood Group</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="blood_group" value="{{$Employees->blood_group}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Personal Phone Number</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="phone" value="{{$Employees->phone}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Personal Email ID</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="email" value="{{$Employees->email}}">
                            </div>
                    </div>
                </div>

                <table class="table">
                    <thead>
                      <tr>
                        <th>Choose Image</th>
                        <th>Preview</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                            <h4 class="card-title">PAN Copy</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" id="CheangeImg" name="pan_copy" value="{{$Employees->pan_copy}}">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                                <br>
                                <img id="img_preview" src="/UI/employees/pan_copy/{{$Employees->pan_copy}}" alt="your image" style="
                                width: 250px;
                            ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">Aadhar Copy</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" id="Cheangeaadhar" name="aadhar_copy" value="{{$Employees->aadhar_copy}}">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                            <br>
                            <img id="aadhar_preview" src="/UI/employees/aadhar_copy/{{$Employees->aadhar_copy}}" alt="your image" style="
                            width: 250px;
                        ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">Passport Photo</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" id="Cheangepassport" name="passport_photo" value="{{$Employees->passport_photo}}">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                                <br>
                                <img id="passport_preview" src="/UI/employees/passport_photo/{{$Employees->passport_photo}}" alt="your image" style="
                                width: 250px;
                            ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">Offer Letter </h4>
                            <div class="form-group">
                                <input type="file" class="form-control" id="Changeoffer" name="offer_letter" value="{{$Employees->offer_letter}}">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                            <br>
                            <img id="offer_preview" src="/UI/employees/offer_letter/{{$Employees->offer_letter}}" alt="your image" style="
                            width: 250px;
                            ">
                        </td>
                      </tr>
                    </tbody>
                  </table>

                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">PAN Copy</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="pan_copy" value="{{$Employees->pan_copy}}">
                            </div>
                    </div>
                </div> --}}
                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Aadhar Copy</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="aadhar_copy" value="{{$Employees->aadhar_copy}}">
                            </div>
                    </div>
                </div> --}}
                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Passport Photo</h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="passport_photo" value="{{$Employees->passport_photo}}">
                            </div>
                    </div>
                </div> --}}
                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Offer Letter </h4>
                            <div class="form-group">
                                <input type="file" class="form-control" name="offer_letter" value="{{$Employees->offer_letter}}">
                            </div>
                    </div>
                </div> --}}
                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection
