@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
            <h4 class="page-title">{{$title}}</h4>
            </div>

            <div class="col-7 align-self-center">
                <a href="/admin/employees/add_employee" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">New Employee</a>
            </div>


        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Employee Name</th>
                                        <th scope="col">Date of Birth</th>
                                        <th scope="col">PAN Number</th>
                                        <th scope="col">Aadhar Number </th>
                                        <th scope="col">Joining Date</th>
                                        <th scope="col">Designation</th>
                                        <th scope="col">Department</th>
                                        {{-- <th scope="col">Status</th> --}}
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($Employees as $Employee)
                                    <tr>
                                    <td></td>
                                        <td>{{$Employee->first_name}} {{$Employee->middle_name}} {{$Employee->last_name}}</td>
                                        <td>{{$Employee->dob}}</td>
                                        <td>{{$Employee->pan_no}}</td>
                                        <td>{{$Employee->aadhar}}</td>
                                        <td>{{$Employee->joining_date}}</td>
                                        <td>{{$Employee->designation}}</td>
                                        <td>{{$Employee->department}}</td>
                                        {{-- <td>
                                            <label class="switch">
                                                <input type="checkbox" data-id="{{$Employee->id}}" class="employee_status"
                                                @if($Employee->status == 0)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider circle-btn"></span>
                                              </label>
                                        </td> --}}
                                        <td>
                                            <a href="/admin/employees/edit_employee/{{$Employee->id}}"><i class="mr-2 mdi mdi-grease-pencil"></i></a>  <a href="javascript:void(0);" data-id="{{$Employee->id}}" class="delete_employee"><i class="mr-2 mdi mdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection
