@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Dashboard</h4>
            </div>
            <div class="col-7 align-self-center">
                {{-- <div class="d-flex no-block justify-content-end align-items-center">
                    <div class="mr-2">
                        <div class="lastmonth"></div>
                    </div>
                    <div class=""><small>LAST MONTH</small>
                        <h4 class="text-info mb-0 font-medium">$58,256</h4></div>
                </div>
            </div> --}}
        </div>
    </div>
    <br>
    <div class="card gredient-info-bg mt-0 mb-0">
        <div class="card-body">
            <h4 class="card-title text-white">Welcome
                @if(Auth::guard('super_admin')->check())
                    {{ Session::get('AdminName') }}
                @elseif(Auth::guard('company')->check())
                {{ Session::get('Companyname') }}
                @elseif(Auth::guard('manager')->check())
                {{ Session::get('Managername') }}
                @elseif(Auth::guard('accountant')->check())
                {{ Session::get('Accountantname') }}
                @elseif(Auth::guard('hr')->check())
                {{ Session::get('Hrname') }}
                @endif
            </h4>
            <div class="row mt-4 mb-3">
                <!-- col -->
                {{-- <div class="col-sm-12 col-lg-4">
                    <div class="temp d-flex align-items-center flex-row">
                        <div class="display-5 text-white"><i class="wi wi-day-showers"></i> <span>73<sup>°</sup></span></div>
                        <div class="ml-2">
                            <h3 class="mb-0 text-white">Saturday</h3><small class="text-white op-5">Ahmedabad, India</small>
                        </div>
                    </div>
                </div> --}}
                <!-- col -->
                <div class="col-sm-12 col-lg-8">
                    <div class="row">
                        <!-- col -->
                        <div class="col-sm-12 col-md-4">
                            <div class="info d-flex align-items-center">
                                <div class="mr-2">
                                    <i class="mdi mdi-wallet text-white display-5 op-5"></i>
                                </div>
                                <div>
                                    <h3 class="text-white mb-0">{{ $Employees }}</h3>
                                    <span class="text-white op-5">Total Employees</span>
                                </div>
                            </div>
                        </div>
                        <!-- col -->
                        <!-- col -->
                        {{-- <div class="col-sm-12 col-md-4">
                            <div class="info d-flex align-items-center">
                                <div class="mr-2">
                                    <i class="mdi mdi-star-circle text-white display-5 op-5"></i>
                                </div>
                                <div>
                                    <h3 class="text-white mb-0">769.08</h3>
                                    <span class="text-white op-5">Total Ledgers</span>
                                </div>
                            </div>
                        </div> --}}
                        <!-- col -->
                        <!-- col -->
                        {{-- <div class="col-sm-12 col-md-4">
                            <div class="info d-flex align-items-center">
                                <div class="mr-2">
                                    <i class="mdi mdi-basket text-white display-5 op-5"></i>
                                </div>
                                <div>
                                    <h3 class="text-white mb-0">5489</h3>
                                    <span class="text-white op-5">Total Vendors</span>
                                </div>
                            </div>
                        </div> --}}
                        <!-- col -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Sales chart -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-md-flex align-items-center">
                            <div>
                                <h4 class="card-title">Balances</h4>
                                <h5 class="card-subtitle">Overview of Latest Month</h5>
                            </div>
                            <div class="ml-auto d-flex no-block align-items-center">
                                <!-- <ul class="list-inline font-12 dl mr-3 mb-0">
                                    <li class="list-inline-item text-info"><i class="fa fa-circle"></i> Iphone</li>
                                    <li class="list-inline-item text-primary"><i class="fa fa-circle"></i> Ipad</li>
                                </ul> -->
                                <div class="dl">
                                    <select class="custom-select">
                                        <option value="0" selected>Monthly</option>
                                        <option value="1">Daily</option>
                                        <option value="2">Weekly</option>
                                        <option value="3">Yearly</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <!-- column -->
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-9">

                                        </div>
                                        <div class="col-md-3">
                                            <select name="year" id="capital_year" class="form-control">
                                                <option value="">Select Year</option>
                                                @foreach($year_list as $row)
                                                    <option value="{{$row->year}}">{{$row->year}}</option>
                                                @endforeach
                                                {{-- <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option> --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                            <div id="chart_div" style="width: 100%; height: 600px;"></div>
                                    </div>

                                </div>
                            </div>
                            <!-- column -->
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-9">

                                        </div>
                                        <div class="col-md-3">
                                            <select name="year" id="loans_year" class="form-control">
                                                <option value="">Select Year</option>
                                                @foreach($year_list as $row)
                                                    <option value="{{$row->year}}">{{$row->year}}</option>
                                                @endforeach
                                                {{-- <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option> --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                            <div id="loans_chart_div" style="width: 100%; height: 600px;"></div>
                                    </div>

                                </div>
                            </div>
                            <!-- column -->
                        </div>

                        <div class="row">
                            <!-- column -->
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-9">

                                        </div>
                                        <div class="col-md-3">
                                            <select name="year" id="liabilities_year" class="form-control">
                                                <option value="">Select Year</option>
                                                @foreach($year_list as $row)
                                                    <option value="{{$row->year}}">{{$row->year}}</option>
                                                @endforeach
                                                {{-- <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option> --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                            <div id="liabilities_chart_div" style="width: 100%; height: 600px;"></div>
                                    </div>

                                </div>
                            </div>
                            <!-- column -->
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-9">

                                        </div>
                                        <div class="col-md-3">
                                            <select name="year" id="Fixed_assets_year" class="form-control">
                                                <option value="">Select Year</option>
                                                @foreach($year_list as $row)
                                                    <option value="{{$row->year}}">{{$row->year}}</option>
                                                @endforeach
                                                {{-- <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option> --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                            <div id="Fixed_assets_chart_div" style="width: 100%; height: 600px;"></div>
                                    </div>

                                </div>
                            </div>
                            <!-- column -->
                        </div>


                        <div class="row">
                            <!-- column -->
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-9">

                                        </div>
                                        <div class="col-md-3">
                                            <select name="year" id="Current_assets_year" class="form-control">
                                                <option value="">Select Year</option>
                                                @foreach($year_list as $row)
                                                    <option value="{{$row->year}}">{{$row->year}}</option>
                                                @endforeach
                                                {{-- <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option> --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                            <div id="Current_assets_chart_div" style="width: 100%; height: 600px;"></div>
                                    </div>

                                </div>
                            </div>
                            <!-- column -->
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-9">

                                        </div>
                                        <div class="col-md-3">
                                            <select name="year" id="Income_year" class="form-control">
                                                <option value="">Select Year</option>
                                                @foreach($year_list as $row)
                                                    <option value="{{$row->year}}">{{$row->year}}</option>
                                                @endforeach
                                                {{-- <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option> --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                            <div id="Income_chart_div" style="width: 100%; height: 600px;"></div>
                                    </div>

                                </div>
                            </div>
                            <!-- column -->
                        </div>

                        <div class="row">
                            <!-- column -->
                            <div class="col-lg-6">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-9">

                                        </div>
                                        <div class="col-md-3">
                                            <select name="year" id="Expenses_year" class="form-control">
                                                <option value="">Select Year</option>
                                                @foreach($year_list as $row)
                                                    <option value="{{$row->year}}">{{$row->year}}</option>
                                                @endforeach
                                                {{-- <option value="2020">2020</option>
                                                <option value="2019">2019</option>
                                                <option value="2018">2018</option> --}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                            <div id="Expenses_chart_div" style="width: 100%; height: 600px;"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- Info Box -->
                    <!-- ============================================================== -->

                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Sales chart -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Email campaign chart -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Ravenue - page-view-bounce rate -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Table -->
        <!-- ============================================================== -->


        <!-- ============================================================== -->
        <!-- Table -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Recent comment and chats -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <!-- Recent comment and chats -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection


@section('JSScript')
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
    // Load the Visualization API and the corechart package.
    google.charts.load('current', {
        'packages': ['corechart', 'bar']
    });
    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawMonthlyChart);
    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawMonthlyChart(chart_data, chart_main_title) {
        let jsonData = chart_data;
        // Create the data table.
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Capitals');
        $.each(jsonData, (i, jsonData) => {
            let month = jsonData.month;
            let profit = parseFloat($.trim(jsonData.profit));
            data.addRows([
                [month, profit]
            ]);
        });
        data.addRows([
            // ['Mushrooms', 140],
            // ['Onions', 50],
            // ['Olives', 30],
            // ['Zucchini', 20],
            // ['Pepperoni', 12]
        ]);
        // Set chart options
        var options = {
            // 'title': 'Check Monthly Profit',
            title: chart_main_title,
            hAxis: {
                title: "Months"
            },
            vAxis: {
                title: "Capitals"
            },
            format: 'MMM',
            colors: ['blue'],

        chartArea: {
            width: '50%',
            height: '80%'
        }
        }
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
    function load_monthly_data(year, title) {
        const temp_title = title + ' ' + year;
        $.ajax({
            url: '/chart/fetch_data/3',
            method:"POST",
            data: {
                "_token": "{{ csrf_token() }}",
                year:year
            },
            dataType: "JSON",
            success:function(data) {
                drawMonthlyChart(data, temp_title);
            }
        });
        console.log(`Year: ${year}`);
    }

    // Loans
    google.charts.setOnLoadCallback(LoansdrawMonthlyChart);
    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function LoansdrawMonthlyChart(chart_data, chart_main_title) {
        let jsonData = chart_data;
        // Create the data table.
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Loans');
        $.each(jsonData, (i, jsonData) => {
            let month = jsonData.month;
            let profit = parseFloat($.trim(jsonData.profit));
            data.addRows([
                [month, profit]
            ]);
        });
        data.addRows([
            // ['Mushrooms', 140],
            // ['Onions', 50],
            // ['Olives', 30],
            // ['Zucchini', 20],
            // ['Pepperoni', 12]
        ]);
        // Set chart options
        var options = {
            // 'title': 'Check Monthly Profit',
            title: chart_main_title,
            hAxis: {
                title: "Months"
            },
            vAxis: {
                title: "Loans"
            },
            format: 'MMM',
            colors: ['blue'],

        chartArea: {
            width: '50%',
            height: '80%'
        }
        }
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('loans_chart_div'));
        chart.draw(data, options);
    }
    function loans_monthly_data(year, title) {
        const temp_title = title + ' ' + year;
        $.ajax({
            url: '/chart/fetch_data/3',
            method:"POST",
            data: {
                "_token": "{{ csrf_token() }}",
                year:year
            },
            dataType: "JSON",
            success:function(data) {
                LoansdrawMonthlyChart(data, temp_title);
            }
        });
        console.log(`Year: ${year}`);
    }

    // Liabilities
    google.charts.setOnLoadCallback(LiabilitiesdrawMonthlyChart);
    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function LiabilitiesdrawMonthlyChart(chart_data, chart_main_title) {
        let jsonData = chart_data;
        // Create the data table.
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Liabilities');
        $.each(jsonData, (i, jsonData) => {
            let month = jsonData.month;
            let profit = parseFloat($.trim(jsonData.profit));
            data.addRows([
                [month, profit]
            ]);
        });
        data.addRows([
            // ['Mushrooms', 140],
            // ['Onions', 50],
            // ['Olives', 30],
            // ['Zucchini', 20],
            // ['Pepperoni', 12]
        ]);
        // Set chart options
        var options = {
            // 'title': 'Check Monthly Profit',
            title: chart_main_title,
            hAxis: {
                title: "Months"
            },
            vAxis: {
                title: "Liabilities"
            },
            format: 'MMM',
            colors: ['blue'],

        chartArea: {
            width: '50%',
            height: '80%'
        }
        }
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('liabilities_chart_div'));
        chart.draw(data, options);
    }
    function liabilities_monthly_data(year, title) {
        const temp_title = title + ' ' + year;
        $.ajax({
            url: '/chart/fetch_data/3',
            method:"POST",
            data: {
                "_token": "{{ csrf_token() }}",
                year:year
            },
            dataType: "JSON",
            success:function(data) {
                LiabilitiesdrawMonthlyChart(data, temp_title);
            }
        });
        console.log(`Year: ${year}`);
    }

    // Fixed Assets
    google.charts.setOnLoadCallback(Fixed_assetsdrawMonthlyChart);
    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function Fixed_assetsdrawMonthlyChart(chart_data, chart_main_title) {
        let jsonData = chart_data;
        // Create the data table.
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Fixed Assets');
        $.each(jsonData, (i, jsonData) => {
            let month = jsonData.month;
            let profit = parseFloat($.trim(jsonData.profit));
            data.addRows([
                [month, profit]
            ]);
        });
        data.addRows([
            // ['Mushrooms', 140],
            // ['Onions', 50],
            // ['Olives', 30],
            // ['Zucchini', 20],
            // ['Pepperoni', 12]
        ]);
        // Set chart options
        var options = {
            // 'title': 'Check Monthly Profit',
            title: chart_main_title,
            hAxis: {
                title: "Months"
            },
            vAxis: {
                title: "Fixed Assets"
            },
            format: 'MMM',
            colors: ['blue'],

        chartArea: {
            width: '50%',
            height: '80%'
        }
        }
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('Fixed_assets_chart_div'));
        chart.draw(data, options);
    }
    function Fixed_assets_monthly_data(year, title) {
        const temp_title = title + ' ' + year;
        $.ajax({
            url: '/chart/fetch_data/3',
            method:"POST",
            data: {
                "_token": "{{ csrf_token() }}",
                year:year
            },
            dataType: "JSON",
            success:function(data) {
                Fixed_assetsdrawMonthlyChart(data, temp_title);
            }
        });
        console.log(`Year: ${year}`);
    }

    // Current Assets
    google.charts.setOnLoadCallback(Current_assetsdrawMonthlyChart);
    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function Current_assetsdrawMonthlyChart(chart_data, chart_main_title) {
        let jsonData = chart_data;
        // Create the data table.
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Current Assets');
        $.each(jsonData, (i, jsonData) => {
            let month = jsonData.month;
            let profit = parseFloat($.trim(jsonData.profit));
            data.addRows([
                [month, profit]
            ]);
        });
        data.addRows([
            // ['Mushrooms', 140],
            // ['Onions', 50],
            // ['Olives', 30],
            // ['Zucchini', 20],
            // ['Pepperoni', 12]
        ]);
        // Set chart options
        var options = {
            // 'title': 'Check Monthly Profit',
            title: chart_main_title,
            hAxis: {
                title: "Months"
            },
            vAxis: {
                title: "Current Assets"
            },
            format: 'MMM',
            colors: ['blue'],

        chartArea: {
            width: '50%',
            height: '80%'
        }
        }
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('Current_assets_chart_div'));
        chart.draw(data, options);
    }
    function Current_assets_monthly_data(year, title) {
        const temp_title = title + ' ' + year;
        $.ajax({
            url: '/chart/fetch_data/3',
            method:"POST",
            data: {
                "_token": "{{ csrf_token() }}",
                year:year
            },
            dataType: "JSON",
            success:function(data) {
                Current_assetsdrawMonthlyChart(data, temp_title);
            }
        });
        console.log(`Year: ${year}`);
    }


    // Income
    google.charts.setOnLoadCallback(IncomedrawMonthlyChart);
    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function IncomedrawMonthlyChart(chart_data, chart_main_title) {
        let jsonData = chart_data;
        // Create the data table.
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Income');
        $.each(jsonData, (i, jsonData) => {
            let month = jsonData.month;
            let profit = parseFloat($.trim(jsonData.profit));
            data.addRows([
                [month, profit]
            ]);
        });
        data.addRows([
            // ['Mushrooms', 140],
            // ['Onions', 50],
            // ['Olives', 30],
            // ['Zucchini', 20],
            // ['Pepperoni', 12]
        ]);
        // Set chart options
        var options = {
            // 'title': 'Check Monthly Profit',
            title: chart_main_title,
            hAxis: {
                title: "Months"
            },
            vAxis: {
                title: "Income"
            },
            format: 'MMM',
            colors: ['blue'],

        chartArea: {
            width: '50%',
            height: '80%'
        }
        }
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('Income_chart_div'));
        chart.draw(data, options);
    }
    function Income_monthly_data(year, title) {
        const temp_title = title + ' ' + year;
        $.ajax({
            url: '/chart/fetch_data/3',
            method:"POST",
            data: {
                "_token": "{{ csrf_token() }}",
                year:year
            },
            dataType: "JSON",
            success:function(data) {
                IncomedrawMonthlyChart(data, temp_title);
            }
        });
        console.log(`Year: ${year}`);
    }


    // Expenses
    google.charts.setOnLoadCallback(ExpensesdrawMonthlyChart);
    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function ExpensesdrawMonthlyChart(chart_data, chart_main_title) {
        let jsonData = chart_data;
        // Create the data table.
        let data = new google.visualization.DataTable();
        data.addColumn('string', 'Month');
        data.addColumn('number', 'Expenses');
        $.each(jsonData, (i, jsonData) => {
            let month = jsonData.month;
            let profit = parseFloat($.trim(jsonData.profit));
            data.addRows([
                [month, profit]
            ]);
        });
        data.addRows([
            // ['Mushrooms', 140],
            // ['Onions', 50],
            // ['Olives', 30],
            // ['Zucchini', 20],
            // ['Pepperoni', 12]
        ]);
        // Set chart options
        var options = {
            // 'title': 'Check Monthly Profit',
            title: chart_main_title,
            hAxis: {
                title: "Months"
            },
            vAxis: {
                title: "Expenses"
            },
            format: 'MMM',
            colors: ['blue'],

        chartArea: {
            width: '50%',
            height: '80%'
        }
        }
        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.ColumnChart(document.getElementById('Expenses_chart_div'));
        chart.draw(data, options);
    }
    function Expenses_monthly_data(year, title) {
        const temp_title = title + ' ' + year;
        $.ajax({
            url: '/chart/fetch_data/3',
            method:"POST",
            data: {
                "_token": "{{ csrf_token() }}",
                year:year
            },
            dataType: "JSON",
            success:function(data) {
                ExpensesdrawMonthlyChart(data, temp_title);
            }
        });
        console.log(`Year: ${year}`);
    }
</script>

<script>

    $(document).ready(function() {
        $('#capital_year').change(function() {
            var year = $(this).val();
            if(year != '') {
                load_monthly_data(year, 'Monthly Data for:');
            }
        });
    });

    $(document).ready(function() {
        $('#loans_year').change(function() {
            var year = $(this).val();
            if(year != '') {
                loans_monthly_data(year, 'Monthly Data for:');
            }
        });
    });

    $(document).ready(function() {
        $('#liabilities_year').change(function() {
            var year = $(this).val();
            if(year != '') {
                liabilities_monthly_data(year, 'Monthly Data for:');
            }
        });
    });

    $(document).ready(function() {
        $('#Fixed_assets_year').change(function() {
            var year = $(this).val();
            if(year != '') {
                Fixed_assets_monthly_data(year, 'Monthly Data for:');
            }
        });

    });

    $(document).ready(function() {
        $('#Current_assets_year').change(function() {
            var year = $(this).val();
            if(year != '') {
                Current_assets_monthly_data(year, 'Monthly Data for:');
            }
        });

    });

    $(document).ready(function() {
        $('#Income_year').change(function() {
            var year = $(this).val();
            if(year != '') {
                Income_monthly_data(year, 'Monthly Data for:');
            }
        });

    });

    $(document).ready(function() {
        $('#Expenses_year').change(function() {
            var year = $(this).val();
            if(year != '') {
                Expenses_monthly_data(year, 'Monthly Data for:');
            }
        });

    });
</script>
@endsection
