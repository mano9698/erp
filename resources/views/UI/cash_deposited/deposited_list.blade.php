@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>

            <div class="col-7 align-self-center">
                <a href="/admin/cash_deposited/add_cash_deposited_entry" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">New Cash Deposited Entry</a>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Cash-in-Hand Group</th>
                                        <th scope="col">Bank Name</th>
                                        <th scope="col">Cash Deposited Date</th>
                                        <th scope="col">Cash Deposited Amount</th>
                                        <th scope="col">Cash Deposited By</th>
                                        {{-- <th scope="col">Status</th> --}}
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($CashDepositedBank as $Payment)
                                    <tr>
                                        <td></td>
                                        <td>{{$Payment->cash_in_hand_group}}</td>
                                        <td>{{$Payment->bank_name}}</td>
                                        <td>{{$Payment->cash_deposited_date}}</td>
                                        <td>{{$Payment->cash_deposited_amount}}</td>
                                        <td>{{$Payment->cash_deposited_by}}</td>
                                        {{-- <td>
                                            <label class="switch">
                                                <input type="checkbox" data-id="{{$Payment->id}}" class="cheque_entry_status"
                                                @if($Payment->status == 0)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider circle-btn"></span>
                                              </label>
                                        </td> --}}
                                        <td>
                                            <a href="/admin/cash_deposited/edit_cash_deposited_entry/{{$Payment->id}}"><i class="mr-2 mdi mdi-grease-pencil"></i></a>  <a href="javascript:void(0);" data-id="{{$Payment->id}}" class="delete_cash_deposited_entry"><i class="mr-2 mdi mdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection
