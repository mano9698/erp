@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/admin/cash_deposited/update_cash_deposited_entry" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Cash-in-Hand Group</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="cash_in_hand_group">
                            <option selected disabled>Select Cash-in-Hand Group</option>
                            @foreach($Ledger as $Ledgers)
                                <option value="{{$Ledgers->id}}" @if($CashDepositedBank->cash_in_hand_group == $Ledgers->id) selected @endif>{{$Ledgers->ledger_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Bank</h4>
                        <select class="custom-select col-12" id="example-month-input2" name="bank_id">
                            <option selected disabled>Choose Bank</option>
                            @foreach($Banks as $Bank)
                                <option value="{{$Bank->id}}" @if($CashDepositedBank->bank_id == $Bank->id) selected @endif>{{$Bank->bank_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Cash Deposited Date</h4>
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" value="{{$CashDepositedBank->id}}">

                                <input type="date" class="form-control" name="cash_deposited_date" value="{{$CashDepositedBank->cash_deposited_date}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Cash Deposited Amount
                        </h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="cash_deposited_amount" value="{{$CashDepositedBank->cash_deposited_amount}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Cash Deposited By
                        </h4>
                            <div class="form-group">
                                <textarea name="cash_deposited_by" id="" cols="30" rows="10" class="form-control">{{$CashDepositedBank->cash_deposited_by}}</textarea>
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
