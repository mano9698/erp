@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-2 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>

            <div class="col-3 align-self-center">
                <div class="card-body">
                    <label for="">Select Ledger Name</label>
                    <select class="custom-select col-12" id="ledger_name" name="ledger">
                        <option selected disabled>Type Ledger Name</option>
                        @foreach($Ledger as $Ledgers)
                            <option value="{{$Ledgers->id}}">{{$Ledgers->ledger_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-2 align-self-center">
                <div class="card-body">
                    <label for="">Select From Date</label>
                    <input type="date">
                </div>
            </div>
            <div class="col-2 align-self-center">
                <div class="card-body">
                    <label for="">Select To Date</label>
                    <input type="date">
                </div>
            </div>

            <div class="col-2 align-self-center">
                <a href="javascript:void(0);" id="SearchLedgers" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">Search</a>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        {{-- <th scope="col">#</th> --}}
                                        <th scope="col">Date</th>
                                        <th scope="col">Particulars</th>
                                        <th scope="col">Debit</th>
                                        <th scope="col">Credit</th>
                                        <th scope="col">Balance</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($Vendors as $Vendor)
                                    <tr>
                                        {{-- <th scope="row">{{$Vendor->id}}</th> --}}
                                        <td>{{$Vendor->name}}</td>
                                        <td>{{$Vendor->group_type_id}}</td>
                                        <td>{{$Vendor->vendor_type}}</td>
                                        <td>{{$Vendor->pan_no}}</td>
                                        <td>{{$Vendor->gst_no}}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection