@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>

        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"><b>Liabilities</b></th>
                                        <th scope="col">Amount</th>
                                        <th scope="col"><b>Assets</b></th>
                                        <th scope="col">Amount </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><b>Capital Accounts ( From Primary Groups )</b></td>
                                        <td></td>
                                        <td><b>Fixed Assets ( From Primary Groups )</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td><b>Capital Accounts ( From Primary Groups )</b></td>
                                        <td></td>
                                        <td><b>Fixed Assets ( From Primary Groups )</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td><b>Capital Accounts ( From Primary Groups )</b></td>
                                        <td></td>
                                        <td><b>Fixed Assets ( From Primary Groups )</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td><b>Capital Accounts ( From Primary Groups )</b></td>
                                        <td></td>
                                        <td><b>Fixed Assets ( From Primary Groups )</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"><b>Expenses</b></th>
                                        <th scope="col">Amount</th>
                                        <th scope="col"><b>Income</b></th>
                                        <th scope="col">Amount </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><b>Capital Accounts ( From Primary Groups )</b></td>
                                        <td></td>
                                        <td><b>Fixed Assets ( From Primary Groups )</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td><b>Capital Accounts ( From Primary Groups )</b></td>
                                        <td></td>
                                        <td><b>Fixed Assets ( From Primary Groups )</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td><b>Capital Accounts ( From Primary Groups )</b></td>
                                        <td></td>
                                        <td><b>Fixed Assets ( From Primary Groups )</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td><b>Capital Accounts ( From Primary Groups )</b></td>
                                        <td></td>
                                        <td><b>Fixed Assets ( From Primary Groups )</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                        <td>- Ledgers under Capital Account Group</td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection
