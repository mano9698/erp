@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/admin/ledger_adjustment/update_adjustment_entry" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Date of Adjustment</h4>
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" value="{{$LedgerAdjustmentEntry->id}}">

                                <input type="date" class="form-control" name="date_of_adjustment" value="{{$LedgerAdjustmentEntry->date_of_adjustment}}">
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select From Ledger</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="from_ledger_id">
                            <option selected disabled>Select From Ledger</option>
                            <option selected disabled>Select Customer Ledger</option>
                            @foreach($Customers as $Customer)
                            <option value="{{$Customer->id}}">{{$Customer->name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Vendor Ledger</option>
                            @foreach($Vendors as $Vendor)
                                <option value="{{$Vendor->id}}">{{$Vendor->name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Employees Ledger</option>
                            @foreach($Employees as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->first_name}} {{$Employee->middle_name}} {{$Employee->last_name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Bank Ledger</option>
                            @foreach($Banks as $Bank)
                                <option value="{{$Bank->id}}">{{$Bank->bank_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select To Ledger</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="to_ledger_id">
                            <option selected disabled>Select To Ledger</option>
                            <option selected disabled>Select Customer Ledger</option>
                            @foreach($Customers as $Customer)
                            <option value="{{$Customer->id}}">{{$Customer->name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Vendor Ledger</option>
                            @foreach($Vendors as $Vendor)
                                <option value="{{$Vendor->id}}">{{$Vendor->name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Employees Ledger</option>
                            @foreach($Employees as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->first_name}} {{$Employee->middle_name}} {{$Employee->last_name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Bank Ledger</option>
                            @foreach($Banks as $Bank)
                                <option value="{{$Bank->id}}">{{$Bank->bank_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Amount to transfer
                        </h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="amount_to_transfer" value="{{$LedgerAdjustmentEntry->amount_to_transfer}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Remarks for Adjustment
                        </h4>
                            <div class="form-group">
                                <textarea name="remarks" id="" cols="30" rows="10" class="form-control">{{$LedgerAdjustmentEntry->remarks}}</textarea>
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
