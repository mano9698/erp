@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>

            <div class="col-7 align-self-center">
                <a href="/admin/ledger_adjustment/add_adjustment_entry" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">New Ledger Adjustment Entry</a>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Date of Adjustment</th>
                                        <th scope="col">From Ledger</th>
                                        <th scope="col">To Ledger</th>
                                        <th scope="col">Amount to transfer</th>
                                        {{-- <th scope="col">Cash Deposited By</th> --}}
                                        {{-- <th scope="col">Status</th> --}}
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($LedgerAdjustmentEntry as $Payment)
                                    <?php
                                        $FromLedger = DB::table('ledger')
                                                        ->where('id', $Payment->from_ledger_id)
                                                        ->select('ledger.ledger_name as FromLedger')
                                                        ->first();
                                        // echo json_encode($FromLedger);
                                        // exit;

                                        $ToLedger = DB::table('ledger')
                                                        ->where('id', $Payment->to_ledger_id)
                                                        ->select('ledger.ledger_name as ToLedger')
                                                        ->first();
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td>{{$Payment->date_of_adjustment}}</td>
                                        <td>{{$FromLedger->FromLedger}}</td>
                                        <td>{{$ToLedger->ToLedger}}</td>
                                        <td>{{$Payment->amount_to_transfer}}</td>
                                        {{-- <td>
                                            <label class="switch">
                                                <input type="checkbox" data-id="{{$Payment->id}}" class="cheque_entry_status"
                                                @if($Payment->status == 0)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider circle-btn"></span>
                                              </label>
                                        </td> --}}
                                        <td>
                                            <a href="/admin/ledger_adjustment/edit_adjustment_entry/{{$Payment->id}}"><i class="mr-2 mdi mdi-grease-pencil"></i></a>  <a href="javascript:void(0);" data-id="{{$Payment->id}}" class="delete_adjustment_entry"><i class="mr-2 mdi mdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection
