@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{ $title }}</h4>
            </div>



        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Mobile</th>
                                        <th scope="col">Country</th>
                                        <th scope="col">State</th>
                                        <th scope="col">City</th>
                                        <th scope="col">Assign Role</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($Users))
                                    @foreach($Users as $User)
                                    <tr>
                                    <th scope="row">{{$User->id}}</th>
                                        <td>{{$User->name}}</td>
                                        <td>{{$User->email}}</td>
                                        <td>{{$User->mobile}}</td>
                                        <td>{{$User->country}}</td>
                                        <td>{{$User->state}}</td>
                                        <td>{{$User->city}}</td>
                                        <td>
                                            <select onchange="CheckRoles({{$User->id}})" class="custom-select col-12" id="roles_user_id" name="user_id">
                                                <option selected disabled>Select Roles</option>
                                                    <option value="4" @if($User->user_type == 4) selected @endif>Manager</option>
                                                    <option value="5" @if($User->user_type == 5) selected @endif>Accountant</option>
                                                    <option value="6" @if($User->user_type == 6) selected @endif>HR Manager</option>
                                            </select>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection

@section('JSScript')
    <script>
    function CheckRoles(id){
        var user_id = $("#roles_user_id").val();
        $.ajax({
            type: "POST",
            url: "/admin/users/change_roles",
            data: {"id": id, "user_id": user_id},
            dataType: "JSON",
            success: function (data) {
                alert(data.success);
            }
        });
    }
    </script>
@endsection
