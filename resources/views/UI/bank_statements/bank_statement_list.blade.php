@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>

            {{-- <div class="col-7 align-self-center">
                <a href="/admin/banks/add_bank" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">New Bank Statement</a>
            </div> --}}
        </div>


    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <form  action="/admin/banks_statement/importExcel" id="myForm" class="form-horizontal" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" />
                            <button type="submit" class="btn btn-primary">Import File</button>
                        </form>
                        <br>
                        @if(session('message'))
                            <div class="alert alert-success width100">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <button style="margin-bottom: 10px" class="btn btn-primary delete_all" data-url="/admin/banks_statement/delete_all_bank_statement">Delete All Selected</button>
                        <div class="table-responsive">
                            <table class="table" id="bank_statement">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th width="50px"><input type="checkbox" id="master"></th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Remakrs </th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Bank Name</th>
                                        <th scope="col">Ledger Name</th>
                                        <th scope="col">Payment Mode</th>
                                        <th scope="col">Payment Reference Number</th>
                                        {{-- <th scope="col">Status</th> --}}
                                        <th scope="col">Action</th>
                                        <th scope="col">Remove Data</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr>
                                        @foreach($BankStatement as $Statement)
                                    <form method="POST" action="/admin/banks_statement/update_bank_statement" id="payoutRequest">
                                        @csrf

                                        <input type="hidden" name="id" value="{{ $Statement->id }}">
                                        <td></td>
                                        <td><input type="checkbox" class="sub_chk" data-id="{{$Statement->id}}"></td>
                                        <td>{{ $Statement->value_date }}</td>
                                        <td>{{ $Statement->description }}</td>
                                        <td>Rs. {{ $Statement->amount }}</td>
                                        <td>
                                            <select class="custom-select col-12 search_dropdown" id="" name="bank_id">
                                                @foreach($Banks as $Bank)
                                                    <option value="{{$Bank->id}}">{{$Bank->bank_name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select col-12 search_dropdown" id="" name="customer_id">
                                                <option >Select Customer Ledger</option>
                                                @foreach($Customers as $Customer)
                                                <option value="{{$Customer->id}}">{{$Customer->name}}</option>
                                                @endforeach

                                                <option  class="dropdown-head-font-size">Choose Vendor Ledger</option>
                                                @foreach($Vendors as $Vendor)
                                                    <option value="{{$Vendor->id}}">{{$Vendor->name}}</option>
                                                @endforeach

                                                <option  class="dropdown-head-font-size">Choose Employees Ledger</option>
                                                @foreach($Employees as $Employee)
                                                    <option value="{{$Employee->id}}">{{$Employee->first_name}} {{$Employee->middle_name}} {{$Employee->last_name}}</option>
                                                @endforeach

                                                <option  class="dropdown-head-font-size">Choose Bank Ledger</option>
                                                @foreach($Banks as $Bank)
                                                    <option value="{{$Bank->id}}">{{$Bank->bank_name}}</option>
                                                @endforeach
                                            </select>
                                        </td>
                                        <td>
                                            <select class="custom-select col-12 search_dropdown" id="" name="payment_mode_id">
                                                <option>Choose Payment Mode</option>
                                                    <option value="NEFT">NEFT</option>
                                                    <option value="RTGS">RTGS</option>
                                                    <option value="IMPS">IMPS</option>
                                                    <option value="CARDS">CARDS </option>
                                            </select>
                                        </td>
                                        <td><input type="text" class="form-control" id="" name="payment_reference_number"></td>
                                        <td>
                                            <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg" >Confirm</button>
                                        </td>
                                        <td><a href="/admin/banks_statement/delete_bank_statement/{{$Statement->id}}" onclick="return confirm(' Are you sure. You want to delete?');"><i class="mr-2 mdi mdi-delete"></i></a></td>
                                    </form>
                                    </tr>

                                    @endforeach
                                    {{-- @foreach($BankStatement as $Bank)
                                    <tr>
                                    <td></td>
                                        <td>{{$Bank->bank_name}}</td>
                                        <td>{{$Bank->account_number}}</td>
                                        <td>{{$Bank->ifsc_code}}</td>
                                        <td>{{$Bank->bank_branch}}</td>
                                        <td>
                                            <a href="/admin/banks/edit_bank/{{$Bank->id}}"><i class="mr-2 mdi mdi-grease-pencil"></i></a>  <a href="javascript:void(0);" data-id="{{$Bank->id}}" class="delete_bank"><i class="mr-2 mdi mdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection


@section('JSScript')
<script type="text/javascript">
    $(document).ready(function () {

        $('#bank_statement').find('tbody tr:eq(0)').hide();

        $('#master').on('click', function(e) {
         if($(this).is(':checked',true))
         {
            $(".sub_chk").prop('checked', true);
         } else {
            $(".sub_chk").prop('checked',false);
         }
        });


        $('.delete_all').on('click', function(e) {


            var allVals = [];
            $(".sub_chk:checked").each(function() {
                allVals.push($(this).attr('data-id'));
            });


            if(allVals.length <=0)
            {
                alert("Please select row.");
            }  else {


                var check = confirm("Are you sure you want to delete this row?");
                if(check == true){


                    var join_selected_values = allVals.join(",");


                    $.ajax({
                        url: $(this).data('url'),
                        type: 'DELETE',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".sub_chk:checked").each(function() {
                                    $(this).parents("tr").remove();
                                });
                                alert(data['success']);
                            } else if (data['error']) {
                                alert(data['error']);
                            } else {
                                alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });

                    location.reload();
                //   $.each(allVals, function( index, value ) {
                //       $('table tr').filter("[data-row-id='" + value + "']").remove();
                //   });
                }
            }
        });



    });
</script>
@endsection
