@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/admin/online_payment_entry/update_payment_entry" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Bank</h4>
                        <input type="hidden" class="form-control" name="id" value="{{$OnlinePaymentEntry->id}}">


                        <select class="custom-select col-12" id="example-month-input2" name="bank_id">
                            <option selected disabled>Choose Bank</option>
                            @foreach($Banks as $Bank)
                                <option value="{{$Bank->id}}" @if($OnlinePaymentEntry->bank_id == $Bank->id) Selected @endif>{{$Bank->bank_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Party Ledger</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="ledger_id">
                            <option selected disabled>Select From Ledger</option>
                            <option selected disabled>Select Customer Ledger</option>
                            @foreach($Customers as $Customer)
                            <option value="{{$Customer->id}}">{{$Customer->name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Vendor Ledger</option>
                            @foreach($Vendors as $Vendor)
                                <option value="{{$Vendor->id}}">{{$Vendor->name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Employees Ledger</option>
                            @foreach($Employees as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->first_name}} {{$Employee->middle_name}} {{$Employee->last_name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Bank Ledger</option>
                            @foreach($Banks as $Bank)
                                <option value="{{$Bank->id}}">{{$Bank->bank_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Payment Mode</h4>
                        <select class="custom-select col-12" id="example-month-input2" name="payment_mode">
                            <option selected selected disabled>Choose Payment Mode</option>
                            <option value="NEFT" @if($OnlinePaymentEntry->payment_mode == "NEFT") Selected @endif>NEFT</option>
                            <option value="RTGS" @if($OnlinePaymentEntry->payment_mode == "RTGS") Selected @endif>RTGS</option>
                            <option value="IMPS" @if($OnlinePaymentEntry->payment_mode == "IMPS") Selected @endif>IMPS</option>
                            <option value="CARDS" @if($OnlinePaymentEntry->payment_mode == "CARDS") Selected @endif>CARDS </option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Payment Date</h4>
                            <div class="form-group">
                            <input type="date" class="form-control" name="payment_date" value="{{$OnlinePaymentEntry->payment_date}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Paid Amount </h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="paid_amount" value="{{$OnlinePaymentEntry->paid_amount}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Payment Reference Number</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="ref_number" value="{{$OnlinePaymentEntry->reference_number}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Payment Remarks</h4>
                            <div class="form-group">
                                <textarea name="remarks" id="" cols="30" rows="10" class="form-control">{{$OnlinePaymentEntry->remarks}}</textarea>
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
