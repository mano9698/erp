@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/admin/cheque_payment_entry/update_cheque_entry" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Bank</h4>
                        <input type="hidden" class="form-control" name="id" value="{{$ChequePaymentEntry->id}}">


                        <select class="custom-select col-12" id="example-month-input2" name="bank_id">
                            <option selected disabled>Select From Ledger</option>
                            <option selected disabled>Select Customer Ledger</option>
                            @foreach($Customers as $Customer)
                            <option value="{{$Customer->id}}">{{$Customer->name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Vendor Ledger</option>
                            @foreach($Vendors as $Vendor)
                                <option value="{{$Vendor->id}}">{{$Vendor->name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Employees Ledger</option>
                            @foreach($Employees as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->first_name}} {{$Employee->middle_name}} {{$Employee->last_name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Bank Ledger</option>
                            @foreach($Banks as $Bank)
                                <option value="{{$Bank->id}}">{{$Bank->bank_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Party Ledger</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="ledger_id">
                            <option selected disabled>Choose Party Ledger</option>
                            @foreach($Ledger as $Ledgers)
                                <option value="{{$Ledgers->id}}" @if($ChequePaymentEntry->ledger_id == $Ledgers->id) Selected @endif>{{$Ledgers->ledger_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Cheque Number
                        </h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="cheque_number" value="{{$ChequePaymentEntry->cheque_number}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Cheque Date </h4>
                            <div class="form-group">
                                <input type="date" class="form-control" name="cheque_date" value="{{$ChequePaymentEntry->cheque_date}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Cheque Amount</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="cheque_amount" value="{{$ChequePaymentEntry->cheque_amount}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Payment Remarks</h4>
                            <div class="form-group">
                                <textarea name="remarks" id="" cols="30" rows="10" class="form-control">{{$ChequePaymentEntry->remarks}}</textarea>
                            </div>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
