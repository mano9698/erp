<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicon.png">
    <title>{{$title}}</title>
    <!-- Custom CSS -->
    <link href="{{URL::asset('UI/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('UI/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{URL::asset('UI/dist/css/style.min.css')}}" rel="stylesheet">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        @include('UI.common.header')
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        @include('UI.common.sidebar')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        @yield('Content')
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- customizer Panel -->
    <!-- ============================================================== -->

    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="{{URL::asset('UI/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{URL::asset('UI/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{URL::asset('UI/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- apps -->
    <script src="{{URL::asset('UI/dist/js/app.min.js')}}"></script>
    <script src="{{URL::asset('UI/dist/js/app.init.js')}}"></script>
    <script src="{{URL::asset('UI/dist/js/app-style-switcher.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{URL::asset('UI/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{URL::asset('UI/assets/extra-libs/sparkline/sparkline.js')}}"></script>
    <!--Wave Effects -->
    <script src="{{URL::asset('UI/dist/js/waves.js')}}"></script>
    <!--Menu sidebar -->
    <script src="{{URL::asset('UI/dist/js/sidebarmenu.js')}}"></script>
    <!--Custom JavaScript -->
    <script src="{{URL::asset('UI/dist/js/custom.min.js')}}"></script>
    <!--This page JavaScript -->
    <!--chartis chart-->
    <script src="{{URL::asset('UI/assets/libs/chartist/dist/chartist.min.js')}}"></script>
    <script src="{{URL::asset('UI/assets/libs/chartist-plugin-tooltips/dist/chartist-plugin-tooltip.min.js')}}"></script>
    <!--c3 charts -->
    <script src="{{URL::asset('UI/assets/extra-libs/c3/d3.min.js')}}"></script>
    <script src="{{URL::asset('UI/assets/extra-libs/c3/c3.min.js')}}"></script>
    <!--chartjs -->
    <script src="{{URL::asset('UI/assets/libs/chart.js/dist/Chart.min.js')}}"></script>
    <script src="{{URL::asset('UI/dist/js/pages/dashboards/dashboard1.js')}}"></script>

    <script src="{{URL::asset('UI/dist/js/custom/users.js')}}"></script>
    <script src="{{URL::asset('UI/dist/js/custom/banks.js')}}"></script>
    <script src="{{URL::asset('UI/dist/js/custom/cash.js')}}"></script>

    <script src="{{URL::asset('UI/assets/libs/sweetalert2/dist/sweetalert2.all.min.js')}}"></script>
    <script src="{{URL::asset('UI/assets/libs/sweetalert2/sweet-alert.init.js')}}"></script>

    <script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>

        <script src="http://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

        <script>
            $(document).ready(function(){

// write code here
$('.ckeditor').ckeditor();

});

            $(document).ready(function(){
                $(".search_dropdown").select2();
            });

            $(document).ready( function () {
                $('table').DataTable();
            } );
        </script>

<script>
    function addRowCount(tableAttr) {
$(tableAttr).each(function(){
$('th:first-child, thead td:first-child', this).each(function(){
  var tag = $(this).prop('tagName');
  $(this).before('<'+tag+'>#</'+tag+'>');
});
$('td:first-child', this).each(function(i){
  $(this).before('<td>'+(i+1)+'</td>');
});
});
}

// Call the function with table attr on which you want automatic serial number
addRowCount('.table');

$('#printInvoice').click(function(){
            Popup($('.invoice')[0].outerHTML);
            function Popup(data)
            {
                window.print();
                return true;
            }
        });
</script>

    @yield('JSScript')
</body>

</html>
