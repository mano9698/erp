@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Edit Vendor</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/admin/vendors/update_vendor" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Vendor Name</h4>
                        <div class="form-group">
                            <input type="hidden" class="form-control" name="id" value="{{$Vendors->id}}">
                            <input type="text" class="form-control" name="name" value="{{$Vendors->name}}">
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Group</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="group_type_id">
                            <option selected disabled class="dropdown-head-font-size">Choose Primary Group</option>
                            @foreach($PrimaryGroup as $Primary)
                            <option value="{{$Primary->id}}"  @if($Primary->id == $Vendors->group_type_id) selected @endif>{{$Primary->group_name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Sub Group</option>
                            @foreach($SubGroup as $Sub)
                                <option value="{{$Sub->id}}"  @if($Sub->id == $Vendors->group_type_id) selected @endif>{{$Sub->group_name}}</option>
                            @endforeach


                            {{-- <option disabled class="dropdown-head-font-size">Choose Sub Group</option>
                            @foreach($SubGroup as $Sub)

                                <option value="{{$Sub->subgroup_name}}"  @if($Sub->subgroup_name == $Vendors->group_type_id) selected @endif>{{$Sub->subgroup_name}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Vendor Type</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="vendor_type">
                            {{-- <option selected disabled>Choose Customer Type</option> --}}
                            <option value="Proprietorship" @if($Vendors->vendor_type == "Proprietorship") selected @endif>Proprietorship</option>
                            <option value="Partnership Firm" @if($Vendors->vendor_type == "Partnership Firm") selected @endif>Partnership Firm</option>
                            <option value="LLP" @if($Vendors->vendor_type == "LLP") selected @endif>LLP</option>
                            <option value="Company" @if($Vendors->vendor_type == "Company") selected @endif>Company </option>
                            <option value="Trust" @if($Vendors->vendor_type == "Trust") selected @endif>Trust</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Vendor PAN</h4>
                            <div class="form-group">
                                <input type="text" name="pan_no" class="form-control" value="{{$Vendors->pan_no}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Vendor GST</h4>
                            <div class="form-group">
                                <input type="text" name="gst_no" class="form-control" value="{{$Vendors->gst_no}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Business Category</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="bussiness_category">
                            {{-- <option selected disabled>Choose Bussiness Category</option> --}}
                            <option value="Service Provider" @if($Vendors->bussiness_category == "Service Provider") selected @endif>Service Provider</option>
                            <option value="Contractor" @if($Vendors->bussiness_category == "Contractor") selected @endif>Contractor</option>
                            <option value="Supplier" @if($Vendors->bussiness_category == "Supplier") selected @endif>Supplier</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Country</h4>
                            <div class="form-group">
                                <input type="text" name="country" class="form-control" value="{{$Vendors->country}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">State</h4>
                            <div class="form-group">
                                <input type="text" name="state" class="form-control" value="{{$Vendors->state}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">City</h4>
                            <div class="form-group">
                                <input type="text" name="city" class="form-control" value="{{$Vendors->city}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Pincode</h4>
                            <div class="form-group">
                                <input type="text" name="pin_code" class="form-control" value="{{$Vendors->pin_code}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Address Line 1</h4>
                            <div class="form-group">
                                <input type="text" name="address_1" class="form-control" value="{{$Vendors->address_1}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Address Line 2</h4>
                            <div class="form-group">
                                <input type="text" name="address_2" class="form-control" value="{{$Vendors->address_2}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Contact Person Name</h4>
                            <div class="form-group">
                                <input type="text" name="contac_person_name" class="form-control" value="{{$Vendors->contac_person_name}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Contact Person Designation</h4>
                            <div class="form-group">
                                <input type="text" name="contact_person_designation" class="form-control" value="{{$Vendors->contact_person_designation}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Phone Number</h4>
                            <div class="form-group">
                                <input type="text" name="phone" class="form-control" value="{{$Vendors->phone}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Email ID</h4>
                            <div class="form-group">
                                <input type="text" name="email" class="form-control" value="{{$Vendors->email}}">
                            </div>
                    </div>
                </div>

                <table class="table">
                    <thead>
                      <tr>
                        <th>Choose Image</th>
                        <th>Preview</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                            <h4 class="card-title">PAN Copy</h4>
                                <div class="form-group">
                                    <input type="file" name="pan_copy" id="CheangeImg" class="form-control" value="{{$Vendors->pan_copy}}">
                                </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                                <br>
                                <img id="img_preview" src="/UI/vendors/pan/{{$Vendors->pan_copy}}" alt="your image" style="
                                width: 250px;
                            ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">GST Copy</h4>
                            <div class="form-group">
                                <input type="file" name="gst_copy" id="Cheangegst" class="form-control" value="{{$Vendors->gst_copy}}">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                            <br>
                            <img id="gst_preview" src="/UI/vendors/gst/{{$Vendors->gst_copy}}" alt="your image" style="
                            width: 250px;
                        ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">Company Incorporation </h4>
                            <div class="form-group">
                                <input type="file" name="company_incorporation" class="form-control" id="Cheangecmpny" value="{{$Vendors->company_incorporation}}">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                                <br>
                                <img id="cmpny_preview" src="/UI/vendors/company_incorporation/{{$Vendors->company_incorporation}}" alt="your image" style="
                                width: 250px;
                            ">
                        </td>
                      </tr>

                      <tr>
                        <td>
                            <h4 class="card-title">Agreement / Contract copy</h4>
                            <div class="form-group">
                                <input type="file" name="aggreement_contract_copy" class="form-control" id="Cheangeagree" value="{{$Vendors->aggreement_contract_copy}}">
                            </div>
                        </td>
                        <td>
                            <label for="">Preview</label>
                            <br>
                            <img id="agree_preview" src="/UI/vendors/agreement _contract/{{$Vendors->aggreement_contract_copy}}" alt="your image" style="
                            width: 250px;
                            ">
                        </td>
                      </tr>
                    </tbody>
                  </table>


                {{-- <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">PAN Copy</h4>
                            <div class="form-group">
                                <input type="file" name="pan_copy" class="form-control" value="{{$Vendors->pan_copy}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">GST Copy</h4>
                            <div class="form-group">
                                <input type="file" name="gst_copy" class="form-control" value="{{$Vendors->gst_copy}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Company Incorporation </h4>
                            <div class="form-group">
                                <input type="file" name="company_incorporation" class="form-control" value="{{$Vendors->company_incorporation}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Agreement / Contract copy</h4>
                            <div class="form-group">
                                <input type="file" name="aggreement_contract_copy" class="form-control" value="{{$Vendors->aggreement_contract_copy}}">
                            </div>
                    </div>
                </div> --}}
                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
