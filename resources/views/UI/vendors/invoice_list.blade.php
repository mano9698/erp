@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>

            <div class="col-7 align-self-center">
                <a href="/admin/vendor_invoice/add_vendor_invoice" class="btn waves-effect waves-light btn-rounded btn-outline-primary pull-right btn-lg">New Vendor Invoice</a>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <!-- <h6 class="card-title mt-5">Table With Outside Padding</h6> -->
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Vendor Name</th>
                                        <th scope="col">Invoice Number</th>
                                        <th scope="col">Invoice Date</th>
                                        <th scope="col">Total Invoice Amount </th>
                                        {{-- <th scope="col">Status</th> --}}
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($VendorInvoice as $Invoice)
                                    <tr>
                                    <td></td>
                                        <td>{{$Invoice->name}}</td>
                                        <td>{{$Invoice->invoice_number}}</td>
                                        <td>{{$Invoice->invoice_date}}</td>
                                        <td>{{$Invoice->total_amount}}</td>
                                        {{-- <td>
                                            <label class="switch">
                                                <input type="checkbox" data-id="{{$Invoice->id}}" class="vendor_invoice_status"
                                                @if($Invoice->status == 0)
                                                    checked
                                                @endif
                                                >
                                                <span class="slider circle-btn"></span>
                                              </label>
                                        </td> --}}
                                        <td>
                                            <a href="/admin/vendor_invoice/edit_invoice/{{$Invoice->id}}"><i class="mr-2 mdi mdi-grease-pencil"></i></a>  <a href="javascript:void(0);" data-id="{{$Invoice->id}}" class="delete_vendor_invoice"><i class="mr-2 mdi mdi-delete"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection
