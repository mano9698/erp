@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">New Bank</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif
            
            <form action="/admin/banks/store_banks" method="post" enctype="multipart/form-data" class="form-width">
                @csrf

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Bank Name</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="bank_name">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Bank Account Number </h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="account_number">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">IFSC Code</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="ifsc_code">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Bank Branch</h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="bank_branch">
                            </div>
                    </div>
                </div>
                
                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>
                
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        All Rights Reserved by  xtream admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>

@endsection