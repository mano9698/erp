@extends('UI.base')

@section('Content')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">{{$title}}</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row bg-white">
            @if(session('message'))
                <div class="alert alert-success width100">
                    <ul>
                        <li>{!! session('message') !!}</li>
                    </ul>
                </div>
            @endif

            <form action="/admin/cash_received/update_cash_received_entry" method="post" enctype="multipart/form-data" class="form-width">
                @csrf
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Cash-in-Hand Group</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="cash_in_hand_group">
                            <option selected disabled>Choose Cash-in-Hand Group</option>
                            @foreach($Ledger as $Ledgers)
                                <option value="{{$Ledgers->id}}" @if($CashReceivedEntry->cash_in_hand_group == $Ledgers->id) Selected @endif>{{$Ledgers->ledger_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Select Party Ledger</h4>
                        <select class="custom-select col-12 search_dropdown" id="example-month-input2" name="ledger_id">
                            <option selected disabled>Select Customer Ledger</option>
                            @foreach($Customers as $Customer)
                            <option value="{{$Customer->id}}">{{$Customer->name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Vendor Ledger</option>
                            @foreach($Vendors as $Vendor)
                                <option value="{{$Vendor->id}}">{{$Vendor->name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Employees Ledger</option>
                            @foreach($Employees as $Employee)
                                <option value="{{$Employee->id}}">{{$Employee->first_name}} {{$Employee->middle_name}} {{$Employee->last_name}}</option>
                            @endforeach

                            <option disabled class="dropdown-head-font-size">Choose Bank Ledger</option>
                            @foreach($Banks as $Bank)
                                <option value="{{$Bank->id}}">{{$Bank->bank_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Cash Received Date</h4>
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" value="{{$CashReceivedEntry->id}}">

                                <input type="date" class="form-control" name="cash_received_date" value="{{$CashReceivedEntry->cash_received_date}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Cash Amount Received
                        </h4>
                            <div class="form-group">
                                <input type="text" class="form-control" name="cash_amount_received" value="{{$CashReceivedEntry->cash_amount_received}}">
                            </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <div class="card-body">
                        <h4 class="card-title">Received For
                        </h4>
                            <div class="form-group">
                                <textarea name="received_for" id="" cols="30" rows="10" class="form-control">{{$CashReceivedEntry->received_for}}</textarea>
                            </div>
                    </div>
                </div>


                <div class="col-sm-12 col-md-6 col-lg-5">
                    <div class="card-body">
                        <br>
                        <button type="submit" class="btn waves-effect waves-light btn-rounded btn-outline-primary btn-lg">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    @include('UI.common.footer')
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection
