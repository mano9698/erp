$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



// Online Payment Entry

$('.delete_cash_payment_entry').click(function(){
    // swal({   
    //     title: "Are you sure?",   
    //     text: "You will not be able to recover this imaginary file!",   
    //     type: "warning",   
    //     showCancelButton: true,   
    //     confirmButtonColor: "#DD6B55",   
    //     confirmButtonText: "Yes, delete it!",   
    //     cancelButtonText: "No, cancel plx!",   
    //     // closeOnConfirm: false,   
    //     // closeOnCancel: false 
    // }, function(){     
    //     var id = $(this).data('id'); 
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id'); 
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cash_payment/delete_cash_payment_entry',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End


// Cheque Payment Entry

$(".cheque_entry_status").click(function() {
    if(this.checked){
        var status = 0; 
        var id = $(this).data('id'); 
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment_entry/cheque_entry_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1; 
        var id = $(this).data('id'); 
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment_entry/cheque_entry_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_cash_received_entry').click(function(){
    // swal({   
    //     title: "Are you sure?",   
    //     text: "You will not be able to recover this imaginary file!",   
    //     type: "warning",   
    //     showCancelButton: true,   
    //     confirmButtonColor: "#DD6B55",   
    //     confirmButtonText: "Yes, delete it!",   
    //     cancelButtonText: "No, cancel plx!",   
    //     // closeOnConfirm: false,   
    //     // closeOnCancel: false 
    // }, function(){     
    //     var id = $(this).data('id'); 
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id'); 
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cash_received/delete_cash_received_entry',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End


// Cash Deposited entry

$(".cheque_received_status").click(function() {
    if(this.checked){
        var status = 0; 
        var id = $(this).data('id'); 
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment_received/cheque_received_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1; 
        var id = $(this).data('id'); 
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment_received/cheque_received_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_cash_deposited_entry').click(function(){
    // swal({   
    //     title: "Are you sure?",   
    //     text: "You will not be able to recover this imaginary file!",   
    //     type: "warning",   
    //     showCancelButton: true,   
    //     confirmButtonColor: "#DD6B55",   
    //     confirmButtonText: "Yes, delete it!",   
    //     cancelButtonText: "No, cancel plx!",   
    //     // closeOnConfirm: false,   
    //     // closeOnCancel: false 
    // }, function(){     
    //     var id = $(this).data('id'); 
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id'); 
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cash_deposited/delete_cash_deposited_entry',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End


// Cheque Payment Deposited Entry

// $(".cheque_received_status").click(function() {
//     if(this.checked){
//         var status = 0; 
//         var id = $(this).data('id'); 
//         console.log(status);
//         $.ajax({
//             type: "POST",
//             dataType: "json",
//             url: '/admin/cheque_payment_received/cheque_received_status',
//             data: {'status': status, 'id': id},
//             success: function(data){
//             console.log(data.success)
//                 alert(data.success);
//             }
//         });
//     }else if(!this.checked){
//         var status = 1; 
//         var id = $(this).data('id'); 
//         console.log(status);
//         $.ajax({
//             type: "POST",
//             dataType: "json",
//             url: '/admin/cheque_payment_received/cheque_received_status',
//             data: {'status': status, 'id': id},
//             success: function(data){
//             console.log(data.success)
//                 alert(data.success);
//             }
//         });
//     }
// });


$('.delete_cheque_deposite').click(function(){
    // swal({   
    //     title: "Are you sure?",   
    //     text: "You will not be able to recover this imaginary file!",   
    //     type: "warning",   
    //     showCancelButton: true,   
    //     confirmButtonColor: "#DD6B55",   
    //     confirmButtonText: "Yes, delete it!",   
    //     cancelButtonText: "No, cancel plx!",   
    //     // closeOnConfirm: false,   
    //     // closeOnCancel: false 
    // }, function(){     
    //     var id = $(this).data('id'); 
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id'); 
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment__deposite_received/delete_cheque_deposite',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End





// Online Payment Received

$(".payment_received_status").click(function() {
    if(this.checked){
        var status = 0; 
        var id = $(this).data('id'); 
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/online_payment_received/payment_received_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1; 
        var id = $(this).data('id'); 
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/online_payment_received/payment_received_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_payment_received').click(function(){
    // swal({   
    //     title: "Are you sure?",   
    //     text: "You will not be able to recover this imaginary file!",   
    //     type: "warning",   
    //     showCancelButton: true,   
    //     confirmButtonColor: "#DD6B55",   
    //     confirmButtonText: "Yes, delete it!",   
    //     cancelButtonText: "No, cancel plx!",   
    //     // closeOnConfirm: false,   
    //     // closeOnCancel: false 
    // }, function(){     
    //     var id = $(this).data('id'); 
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id'); 
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/online_payment_received/delete_payment_received',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End


// Ledger Adjustment 

$('.delete_adjustment_entry').click(function(){
    // swal({   
    //     title: "Are you sure?",   
    //     text: "You will not be able to recover this imaginary file!",   
    //     type: "warning",   
    //     showCancelButton: true,   
    //     confirmButtonColor: "#DD6B55",   
    //     confirmButtonText: "Yes, delete it!",   
    //     cancelButtonText: "No, cancel plx!",   
    //     // closeOnConfirm: false,   
    //     // closeOnCancel: false 
    // }, function(){     
    //     var id = $(this).data('id'); 
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id'); 
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/ledger_adjustment/delete_adjustment_entry',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End



// Opening balance

$('.delete_opening_balance').click(function(){
    // swal({   
    //     title: "Are you sure?",   
    //     text: "You will not be able to recover this imaginary file!",   
    //     type: "warning",   
    //     showCancelButton: true,   
    //     confirmButtonColor: "#DD6B55",   
    //     confirmButtonText: "Yes, delete it!",   
    //     cancelButtonText: "No, cancel plx!",   
    //     // closeOnConfirm: false,   
    //     // closeOnCancel: false 
    // }, function(){     
    //     var id = $(this).data('id'); 
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id'); 
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/opening_balance/delete_opening_balance',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End













// Reports

$("#SearchLedgers").click(function() {
    var ledger_name = $("#ledger_name").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/admin/reports/get_reports_by_ledger',
        data: {'ledger_name': ledger_name},
        success: function(data){
        console.log(data)
            // alert(data.success);
        }
    });
});
