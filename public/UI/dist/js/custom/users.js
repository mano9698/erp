$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



 $("#user_status").click(function() {
    if(this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/users/change_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/users/change_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});




// Group Type
$(".EditGroupType").click(function() {
    // console.log($(this).data("value"));
    $("#id").val($(this).data("id"));
    $("#name").val($(this).data("name"));
    $("#EditGroupTypeModel").modal('show');
    return false;
});

$(".EditCostCenter").click(function() {
    // console.log($(this).data("value"));
    $("#id").val($(this).data("id"));
    $("#name").val($(this).data("name"));
    $("#EditCostCenterModel").modal('show');
    return false;
});


$(".EditUsers").click(function() {
    // console.log($(this).data("value"));
    $("#id").val($(this).data("id"));
    $("#EditUsers").modal('show');
    return false;
});



$(".group_type_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/group_type/group_type_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/group_type/group_type_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});

$('#delete_group_type').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/group_type/delete_group_type',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

$('#delete_cost_center').click(function(){

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cost_center/delete_cost_center',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

$(".group_cost_center").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cost_center/group_cost_center',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cost_center/group_cost_center',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);

            }
        });
    }
});



// Primary Group

$(".primary_group_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/primary_group/primary_group_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/primary_group/primary_group_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);

            }
        });
    }
});


$('#delete_primary_group').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/primary_group/delete_primary_group',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End


// Sub Group

$(".sub_group_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/sub_group/sub_group_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/sub_group/sub_group_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('#delete_sub_group').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/sub_group/delete_sub_group',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End




// Ledger

$(".ledger_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/ledger/ledger_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/ledger/ledger_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_ledger').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/ledger/delete_ledger',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End




// Customers

$(".customer_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/customers/customer_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/customers/customer_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_customers').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/customers/delete_customers',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End



// Customer Invoice

$(".customer_invoice_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/customer_invoice/customer_invoice_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/customer_invoice/customer_invoice_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_customer_invoice').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/customer_invoice/delete_customer_invoice',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});



$('.delete_customer_ledger_amount').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/customer_invoice/delete_customers_ledger_amounts',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});
// End

$('.delete_cash_ledger_amount').click(function(){

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cash_payment/delete_cash_ledger_amount',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});





// Vendors

$(".vendor_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/vendors/vendor_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/vendors/vendor_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_vendors').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/vendors/delete_vendors',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End




// Vendor Invoice

$(".vendor_invoice_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/vendor_invoice/vendor_invoice_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/vendor_invoice/vendor_invoice_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_vendor_invoice').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/vendor_invoice/delete_vendor_invoice',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});


$('.delete_vendor_ledger_amount').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/vendor_invoice/delete_vendors_ledger_amounts',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End




// Employees

$(".employee_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/employees/employee_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/employees/employee_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_employee').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/employees/delete_employee',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End










// Banks

$(".bank_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/banks/bank_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/banks/bank_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_bank').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/banks/delete_bank',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End





// Images


function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#img_preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#CheangeImg").change(function() {
    readURL(this);
  });



  function readURL1(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#gst_preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#Cheangegst").change(function() {
    readURL1(this);
  });



  function readURL2(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#cmpny_preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#Cheangecmpny").change(function() {
    readURL2(this);
  });


  function readURL3(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#agree_preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#Cheangeagree").change(function() {
    readURL3(this);
  });



//   Employees

function readURL4(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#aadhar_preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#Cheangeaadhar").change(function() {
    readURL4(this);
  });


  function readURL5(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#passport_preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#Cheangepassport").change(function() {
    readURL5(this);
  });



  function readURL6(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $('#offer_preview').attr('src', e.target.result);
      }

      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#Changeoffer").change(function() {
    readURL6(this);
  });





  $('.delete_employee_salary_amount').click(function(){


    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/emp_salary/delete_employee_salary_amount',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});


$('.delete_employee_salary').click(function(){


    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/emp_salary/delete_employee_salary',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});
