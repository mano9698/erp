$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});



// Online Payment Entry

$(".payment_entry_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/online_payment_entry/payment_entry_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/online_payment_entry/payment_entry_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_payment_entry').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/online_payment_entry/delete_payment_entry',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End


// Cheque Payment Entry

$(".cheque_entry_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment_entry/cheque_entry_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment_entry/cheque_entry_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_cheque_entry').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment_entry/delete_cheque_entry',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End


// Cheque Payment Entry

$(".cheque_received_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment_received/cheque_received_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment_received/cheque_received_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_cheque_received').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment_received/delete_cheque_received',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End


// Cheque Payment Deposited Entry

// $(".cheque_received_status").click(function() {
//     if(this.checked){
//         var status = 0;
//         var id = $(this).data('id');
//         console.log(status);
//         $.ajax({
//             type: "POST",
//             dataType: "json",
//             url: '/admin/cheque_payment_received/cheque_received_status',
//             data: {'status': status, 'id': id},
//             success: function(data){
//             console.log(data.success)
//                 alert(data.success);
//             }
//         });
//     }else if(!this.checked){
//         var status = 1;
//         var id = $(this).data('id');
//         console.log(status);
//         $.ajax({
//             type: "POST",
//             dataType: "json",
//             url: '/admin/cheque_payment_received/cheque_received_status',
//             data: {'status': status, 'id': id},
//             success: function(data){
//             console.log(data.success)
//                 alert(data.success);
//             }
//         });
//     }
// });


$('.delete_cheque_deposite').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/cheque_payment__deposite_received/delete_cheque_deposite',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End





// Online Payment Received

$(".payment_received_status").click(function() {
    if(this.checked){
        var status = 0;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/online_payment_received/payment_received_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }else if(!this.checked){
        var status = 1;
        var id = $(this).data('id');
        console.log(status);
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/online_payment_received/payment_received_status',
            data: {'status': status, 'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
            }
        });
    }
});


$('.delete_payment_received').click(function(){
    // swal({
    //     title: "Are you sure?",
    //     text: "You will not be able to recover this imaginary file!",
    //     type: "warning",
    //     showCancelButton: true,
    //     confirmButtonColor: "#DD6B55",
    //     confirmButtonText: "Yes, delete it!",
    //     cancelButtonText: "No, cancel plx!",
    //     // closeOnConfirm: false,
    //     // closeOnCancel: false
    // }, function(){
    //     var id = $(this).data('id');
    //     $.ajax({
    //         type: "POST",
    //         dataType: "json",
    //         url: '/admin/group_type/delete_group_type',
    //         data: {'id': id},
    //         success: function(data){
    //         console.log(data.success)
    //             alert(data.success);
    //         }
    //     });
    // });

    var answer = confirm ("Do you want to delete?");
    if (answer)
    {
        var id = $(this).data('id');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/online_payment_received/delete_payment_received',
            data: {'id': id},
            success: function(data){
            console.log(data.success)
                alert(data.success);
                location.reload();
            }
        });
    }
});

// End








// $('.confirm_entry').click(function(){

//     var id = $(this).data('id');
//     var bank_id = $("#bank_id").val();
//     var customer_id = $("#customer_id").val();
//     var payment_mode_id = $("#payment_mode_id").val();
//     var payemnt_reference_no = $("#payemnt_reference_no").val();
//     alert(payment_mode_id);
//     return false;

//     var FormData = {
//         id: id,
//         bank_id: bank_id,
//         customer_id: customer_id,
//         payment_mode_id: payment_mode_id,
//         payemnt_reference_no: payemnt_reference_no
//     }
//     $.ajax({
//         type: "POST",
//         dataType: "json",
//         url: '/admin/banks_statement/update_bank_statement',
//         data: FormData,
//         success: function(data){
//         console.log(data.success)
//             alert(data.success);
//             location.reload();
//         }
//     });
// });
